package cz.muni.fi.pa165.purple.reportingmanager.repository;

import cz.muni.fi.pa165.purple.reportingmanager.model.ReportCashFlowRequest;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportCashResponse;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportRequest;
import cz.muni.fi.pa165.purple.reportingmanager.model.projection.AverageGroupCountProjection;
import cz.muni.fi.pa165.purple.reportingmanager.model.projection.TotalGroupCountProjection;
import cz.muni.fi.pa165.purple.reportingmanager.repository.aggregation.AggregationFactory;
import cz.muni.fi.pa165.purple.reportingmanager.repository.criteria.CriteriaFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class MongoRepoServiceTest {

    @InjectMocks
    private MongoRepoService mongoRepoService;

    @Mock
    private MongoRepo mongoRepo;

    @Mock
    private MongoTemplate mongoTemplate;

    @Mock
    private CriteriaFactory criteriaFactory;

    @Mock
    private AggregationFactory aggregationFactory;

    @Test
    void getTotalTransactionCountPerType() {
        ReportRequest reportRequest = new ReportRequest();
        Criteria criteriaMock = mock(Criteria.class);
        Aggregation aggregationMock = mock(Aggregation.class);
        AggregationResults aggregationResultsMock = mock(AggregationResults.class);

        doReturn(criteriaMock).when(criteriaFactory).createCountCriteria(reportRequest);
        doReturn(aggregationMock).when(aggregationFactory).createCountAggregation(criteriaMock);
        doReturn(aggregationResultsMock).when(mongoTemplate).aggregate(eq(aggregationMock), nullable(String.class), eq(TotalGroupCountProjection.class));

        mongoRepoService.getTotalTransactionCountPerType(reportRequest);

        verify(criteriaFactory).createCountCriteria(reportRequest);
        verify(aggregationFactory).createCountAggregation(criteriaMock);
        verify(mongoTemplate).aggregate(eq(aggregationMock), nullable(String.class), eq(TotalGroupCountProjection.class));
    }

    @Test
    void getAverageTransactionCountPerType_usesCorrectAggregationAndCriteria() {
        ReportRequest reportRequest = new ReportRequest();
        Criteria criteriaMock = mock(Criteria.class);
        Aggregation aggregationMock = mock(Aggregation.class);
        AggregationResults aggregationResultsMock = mock(AggregationResults.class);

        doReturn(criteriaMock).when(criteriaFactory).createCountCriteria(reportRequest);
        doReturn(aggregationMock).when(aggregationFactory).createAverageAggregation(criteriaMock);
        doReturn(aggregationResultsMock).when(mongoTemplate).aggregate(eq(aggregationMock), nullable(String.class), eq(AverageGroupCountProjection.class));

        mongoRepoService.getAverageTransactionCountPerType(reportRequest);

        verify(criteriaFactory).createCountCriteria(reportRequest);
        verify(aggregationFactory).createAverageAggregation(criteriaMock);
        verify(mongoTemplate).aggregate(eq(aggregationMock), nullable(String.class), eq(AverageGroupCountProjection.class));
    }

    @Test
    void getCashFlow_usesCorrectAggregationAndCriteria() {
        ReportCashFlowRequest reportRequest = new ReportCashFlowRequest();
        Criteria criteriaMock = mock(Criteria.class);
        Aggregation aggregationMock = mock(Aggregation.class);
        AggregationResults aggregationResultsMock = mock(AggregationResults.class);

        doReturn(criteriaMock).when(criteriaFactory).createCashCriteria(reportRequest);
        doReturn(aggregationMock).when(aggregationFactory).createCashFlowAggregation(criteriaMock);
        doReturn(aggregationResultsMock).when(mongoTemplate).aggregate(eq(aggregationMock), nullable(String.class), eq(ReportCashResponse.class));

        mongoRepoService.getCashFlow(reportRequest);

        verify(criteriaFactory).createCashCriteria(reportRequest);
        verify(aggregationFactory).createCashFlowAggregation(criteriaMock);
        verify(mongoTemplate).aggregate(eq(aggregationMock), nullable(String.class), eq(ReportCashResponse.class));
    }
}