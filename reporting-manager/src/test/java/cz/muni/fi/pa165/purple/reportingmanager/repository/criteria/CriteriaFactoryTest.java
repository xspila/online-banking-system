package cz.muni.fi.pa165.purple.reportingmanager.repository.criteria;

import cz.muni.fi.pa165.purple.reportingmanager.model.CurrencyEnum;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportCashFlowRequest;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportRequest;
import cz.muni.fi.pa165.purple.reportingmanager.model.TransactionTypeEnum;
import cz.muni.fi.pa165.purple.reportingmanager.utils.FieldConstants;
import org.bson.Document;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.data.mongodb.core.query.Criteria;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

class CriteriaFactoryTest {

    private final CriteriaFactory instance = new CriteriaFactory();

    @Nested
    class CreateCountCriteria {

        @Test
        void createCountCriteria_withAccountNumberReportRequest_returnsExpectedCriteria() {
            ReportRequest reportRequest = createTimeFilterReportRequest();
            reportRequest.setAccountNumbers(Arrays.asList("123", "456"));

            Criteria criteria = instance.createCountCriteria(reportRequest);

            assertThat(criteria).isNotNull();
            Document criteriaDocument = criteria.getCriteriaObject();
            assertThat(criteriaDocument).hasSize(2);
            assertTimeCriteria(criteriaDocument, reportRequest.getFrom(), reportRequest.getTo());
            assertAccountNumbersCriteria(criteriaDocument, reportRequest.getAccountNumbers());
        }

        @Test
        void createCountCriteria_withTransactionTypeFilterReportRequest_returnsExpectedCriteria() {
            ReportRequest reportRequest = createTimeFilterReportRequest();
            reportRequest.setTransactionTypeFilter(Arrays.asList(TransactionTypeEnum.INCOMING_PAYMENT, TransactionTypeEnum.OUTGOING_PAYMENT));

            Criteria criteria = instance.createCountCriteria(reportRequest);

            assertThat(criteria).isNotNull();
            Document criteriaDocument = criteria.getCriteriaObject();
            assertThat(criteriaDocument).hasSize(2);
            assertTimeCriteria(criteriaDocument, reportRequest.getFrom(), reportRequest.getTo());
            assertTransactionTypeFilterCriteria(criteriaDocument, reportRequest.getTransactionTypeFilter());
        }

        @Test
        void createCountCriteria_timeFilterOnly_returnsExpectedCriteria() {
            ReportRequest reportRequest = createTimeFilterReportRequest();

            Criteria criteria = instance.createCountCriteria(reportRequest);

            assertThat(criteria).isNotNull();
            Document criteriaDocument = criteria.getCriteriaObject();
            assertThat(criteriaDocument).hasSize(1);
            assertTimeCriteria(criteriaDocument, reportRequest.getFrom(), reportRequest.getTo());
        }

        private ReportRequest createTimeFilterReportRequest() {
            ReportRequest reportRequest = new ReportRequest();
            reportRequest.setFrom(OffsetDateTime.now().minusDays(1));
            reportRequest.setTo(OffsetDateTime.now());
            return reportRequest;
        }
    }

    @Nested
    class CreateCashCriteria {

        @Test
        void createCashCriteria_timeFilterAndCurrencyReportRequest_returnsExpectedCriteria() {
            ReportCashFlowRequest reportCashFlowRequest = new ReportCashFlowRequest();
            reportCashFlowRequest.setFrom(OffsetDateTime.now().minusDays(1));
            reportCashFlowRequest.setTo(OffsetDateTime.now());
            reportCashFlowRequest.setCurrency(CurrencyEnum.CZK);

            Criteria criteria = instance.createCashCriteria(reportCashFlowRequest);

            assertThat(criteria).isNotNull();
            Document criteriaDocument = criteria.getCriteriaObject();
            assertThat(criteriaDocument).hasSize(2);
            assertTimeCriteria(criteriaDocument, reportCashFlowRequest.getFrom(), reportCashFlowRequest.getTo());
            assertCurrencyCriteria(criteriaDocument, reportCashFlowRequest.getCurrency());
        }

        @Test
        void createCashCriteria_withAccountNumberReportRequest_returnsExpectedCriteria() {
            ReportCashFlowRequest reportCashFlowRequest = new ReportCashFlowRequest();
            reportCashFlowRequest.setFrom(OffsetDateTime.now().minusDays(1));
            reportCashFlowRequest.setTo(OffsetDateTime.now());
            reportCashFlowRequest.setAccountNumbers(Arrays.asList("123", "456"));
            reportCashFlowRequest.setCurrency(CurrencyEnum.EUR);

            Criteria criteria = instance.createCashCriteria(reportCashFlowRequest);

            assertThat(criteria).isNotNull();
            Document criteriaDocument = criteria.getCriteriaObject();
            assertThat(criteriaDocument).hasSize(3);
            assertAll(() -> {
                assertTimeCriteria(criteriaDocument, reportCashFlowRequest.getFrom(), reportCashFlowRequest.getTo());
                assertAccountNumbersCriteria(criteriaDocument, reportCashFlowRequest.getAccountNumbers());
                assertCurrencyCriteria(criteriaDocument, reportCashFlowRequest.getCurrency());
            });
        }


        private void assertCurrencyCriteria(Document criteriaDocument, CurrencyEnum expectedCurrency) {
            CurrencyEnum currencyCriteria = (CurrencyEnum) criteriaDocument.get(FieldConstants.CURRENCY);
            assertThat(currencyCriteria).isEqualTo(expectedCurrency);
        }

    }

    private void assertTransactionTypeFilterCriteria(Document criteriaDocument, List<TransactionTypeEnum> expectedTypes) {
        Document transactionTypeFilterCriteria = (Document) criteriaDocument.get(FieldConstants.TRANSACTION_TYPE);
        assertThat(transactionTypeFilterCriteria).hasSize(1);
        assertThat(transactionTypeFilterCriteria.get("$in")).isEqualTo(expectedTypes);
    }

    private void assertTimeCriteria(Document criteriaDocument, OffsetDateTime expectedFrom, OffsetDateTime expectedTo) {
        Document timeCriteria = (Document) criteriaDocument.get(FieldConstants.TIMESTAMP);
        assertThat(timeCriteria).hasSize(2);
        assertThat(timeCriteria.get("$gte")).isEqualTo(expectedFrom.toLocalDateTime().plusHours(2));
        assertThat(timeCriteria.get("$lte")).isEqualTo(expectedTo.toLocalDateTime().plusHours(2));
    }

    private void assertAccountNumbersCriteria(Document criteriaDocument, List<String> expectedAccountNumbers) {
        Document accountNumbersCriteria = (Document) criteriaDocument.get(FieldConstants.ACCOUNT_NUMBER);
        assertThat(accountNumbersCriteria).hasSize(1);
        assertThat(accountNumbersCriteria.get("$in")).isEqualTo(expectedAccountNumbers);
    }

}