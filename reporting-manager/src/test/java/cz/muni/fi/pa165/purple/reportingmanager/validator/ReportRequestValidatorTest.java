package cz.muni.fi.pa165.purple.reportingmanager.validator;

import cz.muni.fi.pa165.purple.reportingmanager.error.exception.FromAfterToTimeException;
import cz.muni.fi.pa165.purple.reportingmanager.error.exception.InvalidAccountNumberException;
import org.junit.jupiter.api.Test;

import java.time.OffsetDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;

class ReportRequestValidatorTest {

    private final ReportRequestValidator instance = new ReportRequestValidator();

    @Test
    void validateTimePeriod_validTime_noErrors() {
        OffsetDateTime fromTime = OffsetDateTime.now();
        OffsetDateTime toTime = OffsetDateTime.now().plusDays(1);

        instance.validateTimePeriod(fromTime, toTime);
    }

    @Test
    void validateTimePeriod_invalidTime_throwsException() {
        OffsetDateTime fromTime = OffsetDateTime.now();
        OffsetDateTime toTime = OffsetDateTime.now().minusDays(1);

        assertThrows(FromAfterToTimeException.class, () -> instance.validateTimePeriod(fromTime, toTime));
    }

    @Test
    void validateAccountNumbers_validNumber_noErrors() {
        List<String> accountNumbers = List.of("CZ5508000000001234567899");

        instance.validateAccountNumbers(accountNumbers);
    }

    @Test
    void validateAccountNumbers_invalidNumber_throwsException() {
        List<String> accountNumbers = List.of("CZ5508000000001234567890");

        assertThrows(InvalidAccountNumberException.class, () -> instance.validateAccountNumbers(accountNumbers));
    }

}