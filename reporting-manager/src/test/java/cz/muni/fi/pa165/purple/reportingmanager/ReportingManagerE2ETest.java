package cz.muni.fi.pa165.purple.reportingmanager;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.purple.reportingmanager.model.CurrencyEnum;
import cz.muni.fi.pa165.purple.reportingmanager.model.GeneralTransactionStatisticsBo;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportAverageResponse;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportCashResponse;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportTotalResponse;
import cz.muni.fi.pa165.purple.reportingmanager.model.TransactionTypeEnum;
import cz.muni.fi.pa165.purple.reportingmanager.repository.MongoRepo;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.openapitools.OpenApiGeneratorApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@SpringBootTest(webEnvironment = DEFINED_PORT, classes = {MongoRepo.class, OpenApiGeneratorApplication.class})
@AutoConfigureMockMvc
@Testcontainers
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@WithMockUser(username = "admin", authorities = {"SCOPE_EMPLOYEE"})
public class ReportingManagerE2ETest {

    public static final LocalDateTime BASE_TIME = LocalDateTime.of(2024, 2, 1, 1, 0);

    @Container
    @ServiceConnection
    static MongoDBContainer mongoDBContainer = new MongoDBContainer(DockerImageName.parse("mongo:latest"));

    @DynamicPropertySource
    static void datasourceProperties(DynamicPropertyRegistry registry) {
        registry.add("mongodb.url", mongoDBContainer::getReplicaSetUrl);
    }

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private MongoRepo mongoRepo;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @Order(0)
    void testSaveTransaction() {
        createTestTransactions();
    }

    @Test
    @Order(1)
    void testTotalEndpoint() throws Exception {
        String contentAsString = mockMvc.perform(MockMvcRequestBuilders.post("/reporting/total").content(
                                "{\"from\":\"2024-01-01T00:00:00Z\"," +
                                        "\"to\":\"2024-03-01T00:02:00Z\"}")
                        .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().getResponse().getContentAsString();
        ReportTotalResponse reportTotalResponse = objectMapper.readValue(contentAsString, ReportTotalResponse.class);

        assertAll(
                () -> assertThat(reportTotalResponse.getTotalAll().isPresent()).isTrue(),
                () -> assertThat(reportTotalResponse.getTotalAll().get()).isEqualTo(12L),
                () -> assertThat(reportTotalResponse.getDepositsTotal().isPresent()).isTrue(),
                () -> assertThat(reportTotalResponse.getDepositsTotal().get()).isEqualTo(3L),
                () -> assertThat(reportTotalResponse.getWithdrawalsTotal().isPresent()).isTrue(),
                () -> assertThat(reportTotalResponse.getWithdrawalsTotal().get()).isEqualTo(3L),
                () -> assertThat(reportTotalResponse.getOutgoingPaymentsTotal().isPresent()).isTrue(),
                () -> assertThat(reportTotalResponse.getOutgoingPaymentsTotal().get()).isEqualTo(3L),
                () -> assertThat(reportTotalResponse.getIncomingPaymentsTotal().isPresent()).isTrue(),
                () -> assertThat(reportTotalResponse.getIncomingPaymentsTotal().get()).isEqualTo(3L)
        );
    }

    @Test
    @Order(2)
    void testAverageEndpoint() throws Exception {
        String contentAsString = mockMvc.perform(MockMvcRequestBuilders.post("/reporting/average").content(
                                "{\"from\":\"2024-01-01T00:00:00Z\"," +
                                        "\"to\":\"2024-03-01T00:02:00Z\"}")
                        .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().getResponse().getContentAsString();
        ReportAverageResponse reportAverageResponse = objectMapper.readValue(contentAsString, ReportAverageResponse.class);

        assertAll(
                () -> assertThat(reportAverageResponse.getAverageAll().isPresent()).isTrue(),
                () -> assertThat(reportAverageResponse.getAverageAll().get()).isEqualTo(12.0f),
                () -> assertThat(reportAverageResponse.getDepositsAverage().isPresent()).isTrue(),
                () -> assertThat(reportAverageResponse.getDepositsAverage().get()).isEqualTo(3.0f),
                () -> assertThat(reportAverageResponse.getWithdrawalsAverage().isPresent()).isTrue(),
                () -> assertThat(reportAverageResponse.getWithdrawalsAverage().get()).isEqualTo(3.0f),
                () -> assertThat(reportAverageResponse.getOutgoingPaymentsAverage().isPresent()).isTrue(),
                () -> assertThat(reportAverageResponse.getOutgoingPaymentsAverage().get()).isEqualTo(3.0f),
                () -> assertThat(reportAverageResponse.getIncomingPaymentsAverage().isPresent()).isTrue(),
                () -> assertThat(reportAverageResponse.getIncomingPaymentsAverage().get()).isEqualTo(3.0f)
        );
    }

    @Test
    @Order(3)
    void testCashFlowEndpoint() throws Exception {
        String contentAsString = mockMvc.perform(MockMvcRequestBuilders.post("/reporting/cash").content(
                                "{\"from\":\"2024-01-01T00:00:00Z\"," +
                                        "\"to\":\"2024-03-01T00:02:00Z\"}")
                        .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().getResponse().getContentAsString();
        ReportCashResponse reportCashResponse = objectMapper.readValue(contentAsString, ReportCashResponse.class);

        assertAll(
                () -> assertThat(reportCashResponse.getBalance()).isEqualTo(BigDecimal.valueOf(1200)),
                () -> assertThat(reportCashResponse.getDebit()).isEqualTo(BigDecimal.valueOf(1200)),
                () -> assertThat(reportCashResponse.getCredit()).isEqualTo(BigDecimal.valueOf(0)),
                () -> assertThat(reportCashResponse.getCurrency()).isEqualTo(CurrencyEnum.CZK)
        );
    }


    private void createTestTransactions() {
        // create 3 transactions for each type
        for (TransactionTypeEnum type : TransactionTypeEnum.values()) {
            for (int i = 0; i < 3; i++) {
                GeneralTransactionStatisticsBo transaction = new GeneralTransactionStatisticsBo();
                transaction.setAccountNumber("CZ6508000000192000145399");
                transaction.setMoneyTransferred(100);
                transaction.setTransactionType(type);
                transaction.setCurrency(CurrencyEnum.CZK);
                transaction.setTimestamp(BASE_TIME.minusMinutes(i));
                mongoRepo.save(transaction);
            }
        }
    }
}
