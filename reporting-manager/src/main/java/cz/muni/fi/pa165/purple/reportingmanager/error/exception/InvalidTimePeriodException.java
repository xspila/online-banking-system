package cz.muni.fi.pa165.purple.reportingmanager.error.exception;

public class InvalidTimePeriodException extends BaseBadRequestException {

    public InvalidTimePeriodException(String... args) {
        super(args);
    }

    @Override
    public String getCode() {
            return this.getCode() + ".invalidTimePeriod";
        }
}
