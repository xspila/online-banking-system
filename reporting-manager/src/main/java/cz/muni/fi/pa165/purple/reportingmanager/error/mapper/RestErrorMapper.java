package cz.muni.fi.pa165.purple.reportingmanager.error.mapper;


import cz.muni.fi.pa165.purple.reportingmanager.error.exception.BaseRestException;
import cz.muni.fi.pa165.purple.reportingmanager.error.model.RestError;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;

import java.util.Locale;

@Mapper(componentModel = "spring", uses = RestErrorMessageMapper.class)
public abstract class RestErrorMapper {

    @Mapping(target = "error", source = "ex")
    @Mapping(target = "timestamp", expression = "java(OffsetDateTime.now())")
    @Mapping(target = "uuid", expression = "java(UUID.randomUUID())")
    public abstract RestError map(BaseRestException ex, @Context Locale locale);

    @Mapping(target = "error", source = "ex")
    @Mapping(target = "timestamp", expression = "java(OffsetDateTime.now())")
    @Mapping(target = "uuid", expression = "java(UUID.randomUUID())")
    public abstract RestError map(HttpRequestMethodNotSupportedException ex, @Context Locale locale);

    @Mapping(target = "error", source = "ex")
    @Mapping(target = "timestamp", expression = "java(OffsetDateTime.now())")
    @Mapping(target = "uuid", expression = "java(UUID.randomUUID())")
    public abstract RestError map(HttpMediaTypeNotSupportedException ex, @Context Locale locale);

}