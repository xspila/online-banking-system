package cz.muni.fi.pa165.purple.reportingmanager.error.mapper;

import cz.muni.fi.pa165.purple.reportingmanager.error.exception.BaseRestException;
import cz.muni.fi.pa165.purple.reportingmanager.error.model.RestErrorMessage;
import lombok.NoArgsConstructor;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;

import java.util.Locale;


@Mapper(componentModel = "spring")
@NoArgsConstructor
public abstract class RestErrorMessageMapper {

    @Autowired
    @Qualifier("errorMessageSource")
    protected MessageSource errorMessageSource;

    @Mapping(target = "message", source = "ex")
    public abstract RestErrorMessage map(BaseRestException ex, @Context Locale locale);

    protected String translateBaseRestException(BaseRestException ex, @Context Locale locale) {
        return errorMessageSource.getMessage(ex.getCode(), ex.getArgs(), locale);
    }

    @Mapping(target = "message", source = "ex")
    public abstract RestErrorMessage map(HttpRequestMethodNotSupportedException ex, @Context Locale locale);

    protected String translateHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException ex, @Context Locale locale) {
        return errorMessageSource.getMessage("error.http.method.not.supported", new Object[]{ex.getMethod()}, locale);
    }

    @Mapping(target = "message", source = "ex")
    public abstract RestErrorMessage map(HttpMediaTypeNotSupportedException ex, @Context Locale locale);

    protected String translateHttpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException ex, @Context Locale locale) {
        return errorMessageSource.getMessage("error.http.media.type.not.supported", new Object[]{ex.getContentType()}, locale);
    }

}
