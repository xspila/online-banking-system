package cz.muni.fi.pa165.purple.reportingmanager.error.exception;

import java.util.List;

public class InvalidAccountNumberException extends BaseBadRequestException {

        public InvalidAccountNumberException(List<String> invalidAccountNumbers) {
            super(invalidAccountNumbers.toArray());
        }

        @Override
        public String getCode() {
            return super.getCode() + ".invalidAccountNumber";
        }
}
