package cz.muni.fi.pa165.purple.reportingmanager.repository.aggregation;

import cz.muni.fi.pa165.purple.reportingmanager.utils.FieldConstants;
import lombok.NonNull;
import org.springframework.data.mongodb.core.aggregation.AccumulatorOperators;
import org.springframework.data.mongodb.core.aggregation.AddFieldsOperation;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationExpression;
import org.springframework.data.mongodb.core.aggregation.ConditionalOperators;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;

@Component
public class AggregationFactory {

    /**
     * Creates aggregation for counting transactions filtered according to provided criteria.
     */
    public Aggregation createCountAggregation(@NonNull Criteria criteria) {
        MatchOperation matchStage = Aggregation.match(criteria);
        GroupOperation groupStage = Aggregation.group(FieldConstants.TRANSACTION_TYPE).count().as("count");
        AddFieldsOperation addFieldsOperation = Aggregation.addFields().addFieldWithValue("type", "$_id").build();
        return Aggregation.newAggregation(matchStage, groupStage, addFieldsOperation);
    }

    /**
     * Creates aggregation for calculating average number of transactions per account for each transaction type.
     *
     * @param criteria criteria for filtering transactions
     */
    public Aggregation createAverageAggregation(@NonNull Criteria criteria) {
        MatchOperation matchStage = Aggregation.match(criteria);
        GroupOperation groupByTypeAndAccountStage = Aggregation.group(FieldConstants.TRANSACTION_TYPE, FieldConstants.ACCOUNT_NUMBER).
                count().
                as("accountCount");
        GroupOperation groupByTypeStage = Aggregation.group(FieldConstants.TRANSACTION_TYPE)
                .count().as("uniqueAccounts")
                .sum("accountCount").as("totalTransactions");
        ProjectionOperation calculateAverageStage = Aggregation.project("totalTransactions", "uniqueAccounts")
                .andExpression("totalTransactions / uniqueAccounts").as("average");
        AddFieldsOperation addFieldsOperation = Aggregation.addFields().addFieldWithValue("type", "$_id").build();
        return Aggregation.newAggregation(matchStage, groupByTypeAndAccountStage, groupByTypeStage, calculateAverageStage, addFieldsOperation);
    }

    /**
     * Creates aggregation for calculating cash flow statistics filtered according to provided criteria.
     */
    public Aggregation createCashFlowAggregation(@NonNull Criteria criteria) {
        MatchOperation matchStage = Aggregation.match(criteria);
        AggregationExpression balanceExpression = AccumulatorOperators.Sum.sumOf(FieldConstants.MONEY_TRANSFERRED);
        AggregationExpression debitExpression = ConditionalOperators.when(Criteria.where(FieldConstants.MONEY_TRANSFERRED).gt(0))
                .thenValueOf(FieldConstants.MONEY_TRANSFERRED)
                .otherwise(0);
        AggregationExpression creditExpression = ConditionalOperators.when(Criteria.where(FieldConstants.MONEY_TRANSFERRED).lt(0))
                .thenValueOf(FieldConstants.MONEY_TRANSFERRED)
                .otherwise(0);
        GroupOperation groupStage = Aggregation.group(FieldConstants.CURRENCY)
                .sum(balanceExpression).as("balance")
                .sum(debitExpression).as("debit")
                .sum(creditExpression).as("credit");
        return Aggregation.newAggregation(matchStage, groupStage);
    }
}