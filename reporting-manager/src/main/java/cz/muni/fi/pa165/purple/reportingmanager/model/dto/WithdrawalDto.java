package cz.muni.fi.pa165.purple.reportingmanager.model.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class WithdrawalDto extends TransactionDto {
    private String fromIban;
}
