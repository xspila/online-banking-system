package cz.muni.fi.pa165.purple.reportingmanager.repository.criteria;

import cz.muni.fi.pa165.purple.reportingmanager.model.ReportCashFlowRequest;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportRequest;
import cz.muni.fi.pa165.purple.reportingmanager.utils.FieldConstants;
import lombok.NonNull;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;

@Component
public class CriteriaFactory {

    /**
     * Creates criteria for counting transactions. Can filter by time, account numbers and transaction types.
     *
     * @param reportRequest report request
     * @return Criteria for counting transactions
     */
    public Criteria createCountCriteria(@NonNull ReportRequest reportRequest) {
        LocalDateTime fromTime = transformFromTime(reportRequest.getFrom());
        LocalDateTime toTime = transformToTime(reportRequest.getTo());
        Criteria criteria = Criteria.where(FieldConstants.TIMESTAMP).gte(fromTime).lte(toTime);

        if (!CollectionUtils.isEmpty(reportRequest.getAccountNumbers())) {
            criteria.and(FieldConstants.ACCOUNT_NUMBER).in(reportRequest.getAccountNumbers());
        }
        if (!CollectionUtils.isEmpty(reportRequest.getTransactionTypeFilter())) {
            criteria.and(FieldConstants.TRANSACTION_TYPE).in(reportRequest.getTransactionTypeFilter());
        }
        return criteria;
    }

    /**
     * Creates criteria for cash flow report. Can filter by time, account numbers and currency.
     *
     * @param reportRequest report request
     * @return Criteria for cash flow report
     */
    public Criteria createCashCriteria(ReportCashFlowRequest reportRequest) {
        LocalDateTime fromTime = transformFromTime(reportRequest.getFrom());
        LocalDateTime toTime = transformToTime(reportRequest.getTo());
        Criteria criteria = Criteria.where(FieldConstants.TIMESTAMP).gte(fromTime).lte(toTime).and(FieldConstants.CURRENCY).is(reportRequest.getCurrency());

        if (!CollectionUtils.isEmpty(reportRequest.getAccountNumbers())) {
            criteria.and(FieldConstants.ACCOUNT_NUMBER).in(reportRequest.getAccountNumbers());
        }

        return criteria;
    }

    /**
     * Transforms OffsetDateTime to LocalDateTime and adds 2 hours to accomodate for the fact that mongoDb transforms
     * times in aggregations to UTC, that means it substracts 2 hours, so its necessary to add 2 hours for the filtering
     * to be as expected.
     *
     * @Default if fromTime is null, set to (now - 24hours)
     */
    private LocalDateTime transformFromTime(OffsetDateTime fromTime) {
        return fromTime == null ? LocalDateTime.now().minusDays(1).plusHours(2) : fromTime.toLocalDateTime().plusHours(2);
    }

    /**
     * Transforms OffsetDateTime to LocalDateTime and adds 2 hours to accomodate for the fact that mongoDb transforms
     * times in aggregations to UTC, that means it substracts 2 hours, so its neccesary to add 2 hours for the filtering
     * to be as expected.
     *
     * @Default If toTime is null, set to now
     */
    private LocalDateTime transformToTime(OffsetDateTime toTime) {
        return toTime == null ? LocalDateTime.now().plusHours(2) : toTime.toLocalDateTime().plusHours(2);
    }
}