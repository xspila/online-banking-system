package cz.muni.fi.pa165.purple.reportingmanager.model;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.TimeSeries;
import org.springframework.data.mongodb.core.timeseries.Granularity;

import java.time.LocalDateTime;

@Data
@TimeSeries(collection = "TransactionCollection",
        timeField = "timestamp",
        granularity = Granularity.SECONDS)
public class GeneralTransactionStatisticsBo {

    @Id
    private String id;
    private String accountNumber;
    private TransactionTypeEnum transactionType;
    private long moneyTransferred;
    private CurrencyEnum currency;
    @Indexed
    private LocalDateTime timestamp;
}
