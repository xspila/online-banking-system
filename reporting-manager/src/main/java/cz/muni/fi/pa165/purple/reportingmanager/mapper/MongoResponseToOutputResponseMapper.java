package cz.muni.fi.pa165.purple.reportingmanager.mapper;

import cz.muni.fi.pa165.purple.reportingmanager.model.ReportAverageResponse;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportTotalResponse;
import cz.muni.fi.pa165.purple.reportingmanager.model.projection.AverageGroupCountProjection;
import cz.muni.fi.pa165.purple.reportingmanager.model.projection.TotalGroupCountProjection;
import org.mapstruct.Mapper;
import org.openapitools.jackson.nullable.JsonNullable;

import java.util.List;


@Mapper(componentModel = "spring")
public abstract class MongoResponseToOutputResponseMapper {

    public ReportTotalResponse mapToOutputTotalResponse(List<TotalGroupCountProjection> totalGroupCountProjections) {
        ReportTotalResponse reportTotalResponse = new ReportTotalResponse();
        long total = 0;
        for (TotalGroupCountProjection projection : totalGroupCountProjections) {
            switch (projection.getType()) {
                case DEPOSIT:
                    reportTotalResponse.setDepositsTotal(JsonNullable.of(projection.getCount()));
                    break;
                case WITHDRAWAL:
                    reportTotalResponse.setWithdrawalsTotal(JsonNullable.of(projection.getCount()));
                    break;
                case OUTGOING_PAYMENT:
                    reportTotalResponse.setOutgoingPaymentsTotal(JsonNullable.of(projection.getCount()));
                    break;
                case INCOMING_PAYMENT:
                    reportTotalResponse.setIncomingPaymentsTotal(JsonNullable.of(projection.getCount()));
                    break;
            }
            total += projection.getCount();
        }
        if (totalGroupCountProjections.size() != 1) {
            reportTotalResponse.setTotalAll(JsonNullable.of(total));
        }
        return reportTotalResponse;
    }

    public ReportAverageResponse mapToOutputAverageResponse(List<AverageGroupCountProjection> averageGroupCountProjections) {
        ReportAverageResponse reportAverageResponse = new ReportAverageResponse();
        float totalAverage = 0;
        for (AverageGroupCountProjection projection : averageGroupCountProjections) {
            switch (projection.getType()) {
                case DEPOSIT:
                    reportAverageResponse.setDepositsAverage(JsonNullable.of(projection.getAverage()));
                    break;
                case WITHDRAWAL:
                    reportAverageResponse.setWithdrawalsAverage(JsonNullable.of(projection.getAverage()));
                    break;
                case OUTGOING_PAYMENT:
                    reportAverageResponse.setOutgoingPaymentsAverage(JsonNullable.of(projection.getAverage()));
                    break;
                case INCOMING_PAYMENT:
                    reportAverageResponse.setIncomingPaymentsAverage(JsonNullable.of(projection.getAverage()));
                    break;
            }
            totalAverage += projection.getAverage();
        }
        if (averageGroupCountProjections.size() != 1) {
            reportAverageResponse.setAverageAll(JsonNullable.of(totalAverage));
        }
        return reportAverageResponse;
    }
}
