package cz.muni.fi.pa165.purple.reportingmanager.mongo;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.ServerApi;
import com.mongodb.ServerApiVersion;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import cz.muni.fi.pa165.purple.reportingmanager.model.GeneralTransactionStatisticsBo;
import cz.muni.fi.pa165.purple.reportingmanager.repository.MongoRepo;
import cz.muni.fi.pa165.purple.reportingmanager.utils.FieldConstants;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackageClasses = MongoRepo.class)
public class MongoConfig extends AbstractMongoClientConfiguration {

    @Value("${mongodb.url}")
    private String mongoDbUrl;

    @Value("${mongodb.database.name}")
    private String databaseName;

    @Override
    protected String getDatabaseName() {
        return databaseName;
    }

    @Override
    public MongoClient mongoClient() {
        ServerApi serverApi = ServerApi.builder()
                .version(ServerApiVersion.V1)
                .build();
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(new ConnectionString(mongoDbUrl))
                .serverApi(serverApi)
                .build();
        MongoClient mongoClient = MongoClients.create(settings);
        MongoTemplate mongoTemplate = new MongoTemplate(mongoClient, databaseName);
        if (!mongoTemplate.collectionExists(GeneralTransactionStatisticsBo.class)) {
            MongoCollection<Document> collection = mongoTemplate.createCollection(GeneralTransactionStatisticsBo.class);
            collection.createIndex(new Document(FieldConstants.TIMESTAMP, 1));
        }
        return mongoClient;
    }


    @Bean
    public MongoTemplate mongoTemplate() {
        return new MongoTemplate(mongoClient(), databaseName);
    }
}