package cz.muni.fi.pa165.purple.reportingmanager.model.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ImmediateDto extends TransactionDto {
    private String senderIban;
    private String receiverIban;
}