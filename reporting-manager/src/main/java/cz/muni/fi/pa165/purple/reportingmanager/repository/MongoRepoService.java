package cz.muni.fi.pa165.purple.reportingmanager.repository;

import cz.muni.fi.pa165.purple.reportingmanager.model.GeneralTransactionStatisticsBo;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportCashFlowRequest;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportCashResponse;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportRequest;
import cz.muni.fi.pa165.purple.reportingmanager.model.projection.AverageGroupCountProjection;
import cz.muni.fi.pa165.purple.reportingmanager.model.projection.TotalGroupCountProjection;
import cz.muni.fi.pa165.purple.reportingmanager.repository.aggregation.AggregationFactory;
import cz.muni.fi.pa165.purple.reportingmanager.repository.criteria.CriteriaFactory;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;


@Service
@RequiredArgsConstructor
@Slf4j
public class MongoRepoService {

    private final MongoRepo mongoRepo;
    private final MongoTemplate mongoTemplate;
    private final CriteriaFactory criteriaFactory;
    private final AggregationFactory aggregationFactory;

    @Value("${mongodb.timeSeries.transactionCollectionName:TransactionCollection}")
    private String transactionCollectionName;

    @Transactional
    public void saveTransaction(GeneralTransactionStatisticsBo transaction) {
        mongoRepo.save(transaction);
    }

    @Transactional
    public void saveAllTransactions(List<GeneralTransactionStatisticsBo> transactions) {
        log.info("Saving {}", transactions);
        mongoRepo.saveAll(transactions);
    }

    @Transactional(readOnly = true)
    public List<TotalGroupCountProjection> getTotalTransactionCountPerType(@NonNull ReportRequest reportRequest) {
        Criteria criteria = criteriaFactory.createCountCriteria(reportRequest);
        Aggregation aggregation = aggregationFactory.createCountAggregation(criteria);

        return mongoTemplate.aggregate(aggregation, transactionCollectionName, TotalGroupCountProjection.class).getMappedResults();
    }

    @Transactional(readOnly = true)
    public List<AverageGroupCountProjection> getAverageTransactionCountPerType(@NonNull ReportRequest reportRequest) {
        Criteria criteria = criteriaFactory.createCountCriteria(reportRequest);
        Aggregation aggregation = aggregationFactory.createAverageAggregation(criteria);

        return mongoTemplate.aggregate(aggregation, transactionCollectionName, AverageGroupCountProjection.class).getMappedResults();
    }

    @Transactional(readOnly = true)
    public ReportCashResponse getCashFlow(@NonNull ReportCashFlowRequest reportRequest) {
        Criteria criteria = criteriaFactory.createCashCriteria(reportRequest);
        Aggregation aggregation = aggregationFactory.createCashFlowAggregation(criteria);

        ReportCashResponse result = mongoTemplate.aggregate(aggregation, transactionCollectionName, ReportCashResponse.class).getUniqueMappedResult();
        if (result != null) {
            result.setCurrency(reportRequest.getCurrency());
        } else {
            result = new ReportCashResponse();
            result.balance(BigDecimal.ZERO);
        }
        return result;
    }

}
