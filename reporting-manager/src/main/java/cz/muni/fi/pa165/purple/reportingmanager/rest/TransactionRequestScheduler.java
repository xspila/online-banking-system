package cz.muni.fi.pa165.purple.reportingmanager.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.purple.reportingmanager.model.GeneralTransactionStatisticsBo;
import cz.muni.fi.pa165.purple.reportingmanager.model.TransactionTypeEnum;
import cz.muni.fi.pa165.purple.reportingmanager.model.dto.DepositDto;
import cz.muni.fi.pa165.purple.reportingmanager.model.dto.ImmediateDto;
import cz.muni.fi.pa165.purple.reportingmanager.model.dto.WithdrawalDto;
import cz.muni.fi.pa165.purple.reportingmanager.repository.MongoRepoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Component
@RequiredArgsConstructor
@Slf4j
public class TransactionRequestScheduler {

    private final WebClient transactionManagerWebClient;
    private final MongoRepoService mongoRepoService;
    private final ObjectMapper objectMapper;

    @Scheduled(fixedRate = 30000) // Adjust the rate as needed
    public void sendTransactionRequest() {
        int page = 0; // start from first page
        int size = 100; // size of a page
        Pageable pageable = PageRequest.of(page, size, Sort.by("timestamp").descending());
        retrieveAndProcessPage(pageable);
    }

    private void retrieveAndProcessPage(Pageable pageable) {
        LocalDateTime since = LocalDateTime.now().minusSeconds(30);
        log.info("Retrieving transactions since {}", since);
        transactionManagerWebClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path("/transactions")
                        .queryParam("page", pageable.getPageNumber())
                        .queryParam("size", pageable.getPageSize())
                        .queryParam("sort", "timestamp,desc")
                        .queryParam("since", since.toString())
                        .build())
                .header("Authorization", "Bearer " + jwtToken)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<Map<String, Object>>>() {
                })
                .doOnError(throwable -> log.error("Error occurred: {}", throwable.getMessage()))
                .subscribe(response -> {
                    if (response == null) {
                        return;
                    }

                    List<GeneralTransactionStatisticsBo> statisticsBoList = new ArrayList<>();
                    log.info("Received {} transactions", response.size());
                    response.forEach(transactionDto -> {
                        switch (transactionDto.get("type").toString()) {
                            case "DEPOSIT" -> {
                                DepositDto depositDto = objectMapper.convertValue(transactionDto, DepositDto.class);
                                GeneralTransactionStatisticsBo statisticsBo = new GeneralTransactionStatisticsBo();
                                statisticsBo.setMoneyTransferred(Math.round(depositDto.getAmount()));
                                statisticsBo.setTransactionType(TransactionTypeEnum.DEPOSIT);
                                statisticsBo.setAccountNumber(depositDto.getToIban());
                                statisticsBo.setTimestamp(depositDto.getTimestamp());
                                statisticsBo.setCurrency(depositDto.getCurrency());
                                statisticsBoList.add(statisticsBo);
                            }
                            case "WITHDRAWAL" -> {
                                WithdrawalDto withdrawalDto = objectMapper.convertValue(transactionDto, WithdrawalDto.class);
                                GeneralTransactionStatisticsBo statisticsBo = new GeneralTransactionStatisticsBo();
                                statisticsBo.setMoneyTransferred(Math.round(withdrawalDto.getAmount()));
                                statisticsBo.setTransactionType(TransactionTypeEnum.WITHDRAWAL);
                                statisticsBo.setAccountNumber(withdrawalDto.getFromIban());
                                statisticsBo.setTimestamp(withdrawalDto.getTimestamp());
                                statisticsBo.setCurrency(withdrawalDto.getCurrency());
                                statisticsBoList.add(statisticsBo);
                            }
                            case "IMMEDIATE" -> {
                                ImmediateDto immediateDto = objectMapper.convertValue(transactionDto, ImmediateDto.class);
                                GeneralTransactionStatisticsBo statisticsBo = new GeneralTransactionStatisticsBo();
                                statisticsBo.setMoneyTransferred(-Math.round(immediateDto.getAmount()));
                                statisticsBo.setTransactionType(TransactionTypeEnum.OUTGOING_PAYMENT);
                                statisticsBo.setAccountNumber(immediateDto.getSenderIban());
                                statisticsBo.setTimestamp(immediateDto.getTimestamp());
                                statisticsBo.setCurrency(immediateDto.getCurrency());
                                statisticsBoList.add(statisticsBo);
                                GeneralTransactionStatisticsBo statisticsBo2 = new GeneralTransactionStatisticsBo();
                                statisticsBo2.setMoneyTransferred(Math.round(immediateDto.getAmount()));
                                statisticsBo2.setTransactionType(TransactionTypeEnum.INCOMING_PAYMENT);
                                statisticsBo2.setAccountNumber(immediateDto.getReceiverIban());
                                statisticsBo2.setTimestamp(immediateDto.getTimestamp());
                                statisticsBo2.setCurrency(immediateDto.getCurrency());
                                statisticsBoList.add(statisticsBo2);
                            }
                            default -> throw new IllegalArgumentException("Unknown transaction: " + transactionDto);
                        }
                    });
                    statisticsBoList.forEach(statisticsBo -> statisticsBo.setTimestamp(statisticsBo.getTimestamp().plusHours(2)));
                    mongoRepoService.saveAllTransactions(statisticsBoList);
                });
    }

    private final String jwtToken = "eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJFTVBMT1lFRUBtdW5pLmN6IiwiaXNzIjoiT25saW5lQmFua2luZ1" +
            "N5c3RlbSIsInNjb3BlIjoiQ1VTVE9NRVIgRU1QTE9ZRUUgdGVzdF8xIHRlc3RfMiIsImlhdCI6MTcxNjIxNTI5NX0.neQDIrJcfGJM9Swx-" +
            "UrP5wArj60dtTidSxYVh7jPwB5ryIMFeC11XzC7zkwBzwniHCYQhAE6KXYRJRU3U9xOfV3dHD0m5YONFbKSOPxnib64k8PIQ7Jqp8v5dNVJ" +
            "jvpM0gnOMpRs1QIFB-REtG4EsIvpTmGUN2Tgl6eELG03VOzXHmxbgw8WPmeKT0rkzwHCSj8nl48x8i7uqv7i_1BYZr0wlBWwUPQU089wdek" +
            "ZIxv0Ah3LLbJwNQBm1fI4PMoGjQajfbywt0tBfhSrAeHOHSP0YB_t0NL0ePkzWVZR_xqVG9Vu1EVuCM8EOeNe5qfg4Oq6eyP_u-7YANt-ym" +
            "_eUA";
}
