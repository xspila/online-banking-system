package cz.muni.fi.pa165.purple.reportingmanager.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
@EnableScheduling
public class WebClientConfig {

    @Value("${transactionManager.url}")
    private String transactionManagerUrl;

    @Bean
    public WebClient transactionManagerWebClient() {
        return WebClient.builder()
                .baseUrl(transactionManagerUrl)
                .build();
    }
}