package cz.muni.fi.pa165.purple.reportingmanager.validator;

import cz.muni.fi.pa165.purple.reportingmanager.error.exception.FromAfterToTimeException;
import cz.muni.fi.pa165.purple.reportingmanager.error.exception.InvalidAccountNumberException;
import cz.muni.fi.pa165.purple.reportingmanager.error.exception.InvalidTimePeriodException;
import org.apache.commons.validator.routines.IBANValidator;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class ReportRequestValidator {

    /**
     * Validates if the time period is valid.
     * fromTime must be before toTime.
     * fromTime cannot be equal to toTime.
     *
     * @param from start of the time period
     * @param to   end of the time period
     * @throws FromAfterToTimeException   if fromTime is after toTime
     * @throws InvalidTimePeriodException if fromTime is equal to toTime
     */
    public void validateTimePeriod(OffsetDateTime from, OffsetDateTime to) {
        if (from != null && to != null) {
            if (from.isAfter(to)) {
                throw new FromAfterToTimeException();
            }
            if (from.isEqual(to)) {
                throw new InvalidTimePeriodException();
            }
        }
    }

    /**
     * Validates if the account numbers are valid. Also validates checksums, not only the format.
     *
     * @param accountNumbers list of account numbers to validate
     * @throws InvalidAccountNumberException if any of the account numbers is invalid
     */
    public void validateAccountNumbers(List<String> accountNumbers) {
        if (CollectionUtils.isEmpty(accountNumbers)) {
            return;
        }
        IBANValidator ibanValidator = IBANValidator.getInstance();
        List<String> invalidAccountNumbers = new ArrayList<>();
        accountNumbers.forEach(accountNumber -> {
            if (!ibanValidator.isValid(accountNumber)) {
                invalidAccountNumbers.add(accountNumber);
            }
        });
        if (!invalidAccountNumbers.isEmpty()) {
            throw new InvalidAccountNumberException(invalidAccountNumbers);
        }
    }
}
