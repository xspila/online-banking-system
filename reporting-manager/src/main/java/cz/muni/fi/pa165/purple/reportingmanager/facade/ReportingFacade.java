package cz.muni.fi.pa165.purple.reportingmanager.facade;

import cz.muni.fi.pa165.purple.reportingmanager.model.ReportAverageResponse;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportCashFlowRequest;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportCashResponse;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportRequest;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportTotalResponse;
import cz.muni.fi.pa165.purple.reportingmanager.service.ReportingService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ReportingFacade {

    private final ReportingService reportingService;

    public String testEndpoint() {
        return reportingService.testEndpoint();
    }

    public ReportAverageResponse getReportAverage(ReportRequest reportRequest) {
        return reportingService.getReportAverage(reportRequest);
    }

    public ReportTotalResponse getReportTotal(ReportRequest reportRequest) {
        return reportingService.getReportTotal(reportRequest);
    }

    public ReportCashResponse getReportCash(ReportCashFlowRequest reportRequest) {
        return reportingService.getReportCash(reportRequest);
    }

}
