package cz.muni.fi.pa165.purple.reportingmanager.model.dto;

import cz.muni.fi.pa165.purple.reportingmanager.model.CurrencyEnum;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class TransactionDto implements Serializable {
    private String id;
    private double amount;
    private String type;
    private LocalDateTime timestamp;
    private CurrencyEnum currency;
}
