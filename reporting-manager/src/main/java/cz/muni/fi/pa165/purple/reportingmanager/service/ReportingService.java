package cz.muni.fi.pa165.purple.reportingmanager.service;

import cz.muni.fi.pa165.purple.reportingmanager.mapper.MongoResponseToOutputResponseMapper;
import cz.muni.fi.pa165.purple.reportingmanager.model.CurrencyEnum;
import cz.muni.fi.pa165.purple.reportingmanager.model.GeneralTransactionStatisticsBo;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportAverageResponse;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportCashFlowRequest;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportCashResponse;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportRequest;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportTotalResponse;
import cz.muni.fi.pa165.purple.reportingmanager.model.TransactionTypeEnum;
import cz.muni.fi.pa165.purple.reportingmanager.repository.MongoRepoService;
import cz.muni.fi.pa165.purple.reportingmanager.validator.ReportRequestValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;


@Service
@RequiredArgsConstructor
public class ReportingService {

    private final ReportRequestValidator reportRequestValidator;
    private final MongoRepoService mongoRepoService;
    private final MongoResponseToOutputResponseMapper mongoResponseToOutputResponseMapper;


    public String testEndpoint() {
        List<String> accountNumbers = List.of("CZ2850514863768494113256", "CZ6050516263453833731579", "CZ1750512527682967891453", "CZ2550515548468511346336", "CZ3250517347693611456594", "CZ8450516554635931338193", "CZ1150515754779739431496");
        Random random = new Random();
        for (int i = 0; i < 100; i++) {
            insertTransaction(random, accountNumbers);
        }
        return "Transactions added";
    }

    // TODO: delete when the app is integrated with the rest of the system
    public void insertTransaction(Random random, List<String> accountNumbers) {
        GeneralTransactionStatisticsBo transaction = new GeneralTransactionStatisticsBo();
        transaction.setAccountNumber(accountNumbers.get(random.nextInt(accountNumbers.size())));
        transaction.setTransactionType(TransactionTypeEnum.values()[random.nextInt(TransactionTypeEnum.values().length)]);
        if (transaction.getTransactionType() == TransactionTypeEnum.WITHDRAWAL || transaction.getTransactionType() == TransactionTypeEnum.OUTGOING_PAYMENT) {
            transaction.setMoneyTransferred(-random.nextInt(5000));
        } else {
            transaction.setMoneyTransferred(random.nextInt(5000));
        }
        transaction.setCurrency(CurrencyEnum.values()[random.nextInt(CurrencyEnum.values().length)]);
        transaction.setTimestamp(LocalDateTime.now().minusSeconds(random.nextInt(1000)));
        mongoRepoService.saveTransaction(transaction);
    }

    public ReportTotalResponse getReportTotal(ReportRequest reportRequest) {
        reportRequestValidator.validateTimePeriod(reportRequest.getFrom(), reportRequest.getTo());
        reportRequestValidator.validateAccountNumbers(reportRequest.getAccountNumbers());

        return mongoResponseToOutputResponseMapper.mapToOutputTotalResponse(mongoRepoService.getTotalTransactionCountPerType(reportRequest));
    }

    public ReportAverageResponse getReportAverage(ReportRequest reportRequest) {
        reportRequestValidator.validateTimePeriod(reportRequest.getFrom(), reportRequest.getTo());
        reportRequestValidator.validateAccountNumbers(reportRequest.getAccountNumbers());

        return mongoResponseToOutputResponseMapper.mapToOutputAverageResponse(mongoRepoService.getAverageTransactionCountPerType(reportRequest));
    }

    public ReportCashResponse getReportCash(ReportCashFlowRequest reportRequest) {
        reportRequestValidator.validateTimePeriod(reportRequest.getFrom(), reportRequest.getTo());
        reportRequestValidator.validateAccountNumbers(reportRequest.getAccountNumbers());

        return mongoRepoService.getCashFlow(reportRequest);
    }
}
