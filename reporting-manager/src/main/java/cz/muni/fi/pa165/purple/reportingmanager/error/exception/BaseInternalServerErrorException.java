package cz.muni.fi.pa165.purple.reportingmanager.error.exception;

import org.springframework.http.HttpStatus;

public class BaseInternalServerErrorException extends BaseRestException {

    public BaseInternalServerErrorException() {
        super();
    }

    @Override
    public String getCode() {
        return "internalError";
    }

    @Override
    public HttpStatus status() {
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }
}
