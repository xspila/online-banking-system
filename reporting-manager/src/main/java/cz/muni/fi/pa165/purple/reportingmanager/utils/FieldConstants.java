package cz.muni.fi.pa165.purple.reportingmanager.utils;


public class FieldConstants {
    public static final String TRANSACTION_TYPE = "transactionType";
    public static final String TIMESTAMP = "timestamp";
    public static final String ACCOUNT_NUMBER = "accountNumber";
    public static final String CURRENCY = "currency";
    public static final String MONEY_TRANSFERRED = "moneyTransferred";
}
