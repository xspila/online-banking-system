package cz.muni.fi.pa165.purple.reportingmanager.error.exception;

import org.springframework.http.HttpStatus;

public interface RestException {

    String getCode();

    HttpStatus status();
}
