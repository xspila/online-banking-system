package cz.muni.fi.pa165.purple.reportingmanager.server;

import cz.muni.fi.pa165.purple.reportingmanager.ReportingApiDelegate;
import cz.muni.fi.pa165.purple.reportingmanager.facade.ReportingFacade;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportAverageResponse;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportCashFlowRequest;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportCashResponse;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportRequest;
import cz.muni.fi.pa165.purple.reportingmanager.model.ReportTotalResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ReportingManagerServer implements ReportingApiDelegate {

    private final ReportingFacade reportingFacade;

    // test endpoint for inserting 100 random transactions
    @Override
    public ResponseEntity<String> testEndpoint() {
        return ResponseEntity.ok(reportingFacade.testEndpoint());
    }

    @Override
    public ResponseEntity<ReportAverageResponse> getReportAverage(ReportRequest reportRequest) {
        return ResponseEntity.ok(reportingFacade.getReportAverage(reportRequest));
    }

    @Override
    public ResponseEntity<ReportTotalResponse> getReportTotal(ReportRequest reportRequest) {
        return ResponseEntity.ok(reportingFacade.getReportTotal(reportRequest));
    }

    @Override
    public ResponseEntity<ReportCashResponse> getReportCash(ReportCashFlowRequest reportRequest) {
        return ResponseEntity.ok(reportingFacade.getReportCash(reportRequest));
    }
}
