package cz.muni.fi.pa165.purple.reportingmanager.error.exception;

public class FromAfterToTimeException extends InvalidTimePeriodException {

        @Override
        public String getCode() {
            return this.getCode() + ".fromAfterTo";
        }
}
