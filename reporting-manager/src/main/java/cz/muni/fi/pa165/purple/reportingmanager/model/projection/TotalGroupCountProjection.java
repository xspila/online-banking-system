package cz.muni.fi.pa165.purple.reportingmanager.model.projection;

import cz.muni.fi.pa165.purple.reportingmanager.model.TransactionTypeEnum;
import lombok.Getter;

@Getter
public class TotalGroupCountProjection {
    private TransactionTypeEnum type;
    private long count;
}
