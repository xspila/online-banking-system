package cz.muni.fi.pa165.purple.reportingmanager.repository;

import cz.muni.fi.pa165.purple.reportingmanager.model.GeneralTransactionStatisticsBo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface MongoRepo extends MongoRepository<GeneralTransactionStatisticsBo, Long> {

}