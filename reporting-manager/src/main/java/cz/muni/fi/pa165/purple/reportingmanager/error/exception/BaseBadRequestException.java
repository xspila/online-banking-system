package cz.muni.fi.pa165.purple.reportingmanager.error.exception;

import org.springframework.http.HttpStatus;

public class BaseBadRequestException extends BaseRestException {

    public BaseBadRequestException(Object[] args) {
        super(args);
    }

    @Override
    public String getCode() {
        return "badRequest";
    }

    @Override
    public HttpStatus status() {
        return HttpStatus.BAD_REQUEST;
    }
}
