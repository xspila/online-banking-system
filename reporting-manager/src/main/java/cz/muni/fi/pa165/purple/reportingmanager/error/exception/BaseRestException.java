package cz.muni.fi.pa165.purple.reportingmanager.error.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public abstract class BaseRestException extends RuntimeException implements RestException {

    private Object[] args;

    protected BaseRestException() {
        super();
    }
}
