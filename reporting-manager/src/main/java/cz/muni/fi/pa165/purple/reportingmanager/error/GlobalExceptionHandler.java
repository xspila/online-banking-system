package cz.muni.fi.pa165.purple.reportingmanager.error;

import cz.muni.fi.pa165.purple.reportingmanager.error.exception.BaseInternalServerErrorException;
import cz.muni.fi.pa165.purple.reportingmanager.error.exception.BaseRestException;
import cz.muni.fi.pa165.purple.reportingmanager.error.mapper.RestErrorMapper;
import cz.muni.fi.pa165.purple.reportingmanager.error.model.RestError;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Locale;

@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler {

    private final RestErrorMapper errorMapper;

    /**
     * Common handler for all exceptions that inherit from {@link BaseRestException}.
     */
    @ExceptionHandler(BaseRestException.class)
    public ResponseEntity<RestError> handleException(@NonNull BaseRestException ex) {
        RestError restError = errorMapper.map(ex, Locale.ENGLISH);
        log.error("Handling rest exception, uuid={}", restError.uuid(), ex);
        return ResponseEntity.status(ex.status()).body(restError);
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<RestError> handleException(@NonNull HttpRequestMethodNotSupportedException ex) {
        RestError restError = errorMapper.map(ex, Locale.ENGLISH);
        log.error("Handling exception, uuid={}", restError.uuid(), ex);
        return ResponseEntity.status(405).body(restError);
    }

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ResponseEntity<RestError> handleException(@NonNull HttpMediaTypeNotSupportedException ex) {
        RestError restError = errorMapper.map(ex, Locale.ENGLISH);
        log.error("Handling exception, uuid={}", restError.uuid(), ex);
        return ResponseEntity.status(415).body(restError);
    }

    /**
     * Common handler for all other exceptions.
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<RestError> handleException(@NonNull Exception ex) {
        var restInternalServerException = new BaseInternalServerErrorException();
        RestError restError = errorMapper.map(restInternalServerException, Locale.ENGLISH);
        log.error("Handling exception, uuid={}", restError.uuid(), ex);
        return ResponseEntity.status(restInternalServerException.status()).body(restError);
    }
}
