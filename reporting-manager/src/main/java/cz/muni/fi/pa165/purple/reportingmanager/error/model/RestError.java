package cz.muni.fi.pa165.purple.reportingmanager.error.model;

import java.time.OffsetDateTime;
import java.util.UUID;


/**
 * Main DTO class for rest error response returned by handlers defined in GlobalExceptionHandler with corresponding status code
 */
public record RestError(UUID uuid, RestErrorMessage error, OffsetDateTime timestamp) {
}