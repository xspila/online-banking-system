package cz.muni.fi.pa165.purple.usermanager.mappers;

import cz.muni.fi.pa165.purple.usermanager.api.UserBasicViewDto;
import cz.muni.fi.pa165.purple.usermanager.api.UserDetailedViewDto;
import cz.muni.fi.pa165.purple.usermanager.api.UserRegistrationDto;
import cz.muni.fi.pa165.purple.usermanager.data.model.User;

import java.util.List;

public interface UserMapper {
    UserBasicViewDto mapToBasicDto(User user);

    UserDetailedViewDto mapToDetailedDto(User user);

    List<UserBasicViewDto> mapToBasicList(List<User> users);

    List<UserDetailedViewDto> mapToDetailedList(List<User> users);
    User mapToUser(UserRegistrationDto userRegistrationDto);
}
