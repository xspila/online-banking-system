package cz.muni.fi.pa165.purple.usermanager.api;

public record UserDetailedViewDto(
        String userName,
        String name,
        String email,
        String phoneNumber,
        String mobilePhoneNumber,
        String address,
        String accountState,
        Long lastLoginTimestamp,
        Long lastPasswordChangeTimestamp,
        Long accountCreationTimestamp,
        Boolean isEmployee
) {
}
