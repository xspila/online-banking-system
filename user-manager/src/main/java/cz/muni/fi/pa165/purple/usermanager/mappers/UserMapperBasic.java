package cz.muni.fi.pa165.purple.usermanager.mappers;

import cz.muni.fi.pa165.purple.usermanager.api.UserBasicViewDto;
import cz.muni.fi.pa165.purple.usermanager.api.UserDetailedViewDto;
import cz.muni.fi.pa165.purple.usermanager.api.UserRegistrationDto;
import cz.muni.fi.pa165.purple.usermanager.data.enums.ActionType;
import cz.muni.fi.pa165.purple.usermanager.data.model.ActionLog;
import cz.muni.fi.pa165.purple.usermanager.data.model.Address;
import cz.muni.fi.pa165.purple.usermanager.data.model.Contact;
import cz.muni.fi.pa165.purple.usermanager.data.model.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserMapperBasic implements UserMapper {


    @Override
    public UserBasicViewDto mapToBasicDto(User user) {
        return new UserBasicViewDto(
                user.getUsername(),
                user.getFullName(),
                user.getContact().getEmail(),
                user.getIsEmployee()
        );
    }

    @Override
    public UserDetailedViewDto mapToDetailedDto(User user) {
        Contact contact = user.getContact();
        return new UserDetailedViewDto(
                user.getUsername(),
                user.getFullName(),
                contact.getEmail(),
                contact.getPhoneNumber(),
                contact.getMobilePhoneNumber(),
                contact.getAddress().toString(),
                user.getAccountState().toString(),
                user.getLastLoginTimestamp(),
                user.getLastPasswordChangeTimestamp(),
                user.getAccountCreationTimestamp(),
                user.getIsEmployee()
        );
    }

    @Override
    public List<UserBasicViewDto> mapToBasicList(List<User> users) {
        return users.stream()
                .map(this::mapToBasicDto)
                .toList();
    }

    @Override
    public List<UserDetailedViewDto> mapToDetailedList(List<User> users) {
        return users.stream()
                .map(this::mapToDetailedDto)
                .toList();
    }

    @Override
    public User mapToUser(UserRegistrationDto userRegistrationDto) {
        Address address = new Address(
                userRegistrationDto.street(),
                userRegistrationDto.city(),
                userRegistrationDto.state(),
                userRegistrationDto.country(),
                userRegistrationDto.postalCode()
        );
        Contact contact = new Contact(
                userRegistrationDto.phoneNumber(),
                userRegistrationDto.mobilePhoneNubmer(),
                userRegistrationDto.email(),
                address
        );
        return new User(
                userRegistrationDto.username(),
                userRegistrationDto.password(),
                userRegistrationDto.firstName(),
                userRegistrationDto.lastName(),
                userRegistrationDto.nationality(),
                contact,
                new ArrayList<>(),
                userRegistrationDto.isEmployee()
        );
    }
}
