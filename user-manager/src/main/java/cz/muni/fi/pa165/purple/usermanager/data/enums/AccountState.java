package cz.muni.fi.pa165.purple.usermanager.data.enums;

public enum AccountState {
    NON_ACTIVATED, // account is created but was not activated yet or was deactivated
    ACTIVE,
    DELETED
}
