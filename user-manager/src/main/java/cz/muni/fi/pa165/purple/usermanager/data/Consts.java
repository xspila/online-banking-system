package cz.muni.fi.pa165.purple.usermanager.data;

public final class Consts {
    private Consts(){};
    private static final String SCOPE_PREFIX = "SCOPE_";
    public static final String TEST_1 = "test_1";
    public static final String TEST_2 = "test_2";
    public static final String OAUTH2_ACCESS_SCOPE1 = SCOPE_PREFIX + TEST_1;
    public static final String OAUTH2_ACCESS_SCOPE2 = SCOPE_PREFIX + TEST_2;

    public static final String ROLE_PREFIX = "ROLE_";
    public static final String EMPLOYEE_SCOPE = "SCOPE_EMPLOYEE";
    public static final String CUSTOMER_SCOPE = "SCOPE_CUSTOMER";
}
