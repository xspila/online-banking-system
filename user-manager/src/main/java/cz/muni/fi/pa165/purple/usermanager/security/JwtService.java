package cz.muni.fi.pa165.purple.usermanager.security;

import cz.muni.fi.pa165.purple.usermanager.data.model.Authority;
import cz.muni.fi.pa165.purple.usermanager.data.model.User;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;
import org.testcontainers.shaded.org.bouncycastle.util.io.pem.PemObject;
import org.testcontainers.shaded.org.bouncycastle.util.io.pem.PemReader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class JwtService {

    private PrivateKey privateKey;
    public PublicKey publicKey;

    @Autowired
    public JwtService(@Value("${private.key.file}") Resource privateKeyResource,
                      @Value("${public.key.file}") Resource publicKeyResource) throws Exception {
        KeyFactory factory = KeyFactory.getInstance("RSA");
        try (InputStream keyStream = privateKeyResource.getInputStream();
             PemReader pemReader = new PemReader(new InputStreamReader(keyStream))) {

            PemObject pemObject = pemReader.readPemObject();
            byte[] content = pemObject.getContent();
            PKCS8EncodedKeySpec privKeySpec = new PKCS8EncodedKeySpec(content);
            privateKey = factory.generatePrivate(privKeySpec);
        }
        try (InputStream keyStream = publicKeyResource.getInputStream();
             PemReader pemReader = new PemReader(new InputStreamReader(keyStream))) {

            PemObject pemObject = pemReader.readPemObject();
            byte[] content = pemObject.getContent();
            X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(content);
            publicKey = factory.generatePublic(pubKeySpec);
        }
    }

    public String generateToken(UsernamePasswordAuthenticationToken authentication) {
        if (authentication == null) {
            throw new IllegalStateException("No user is logged in");
        }
        if (!(authentication.getPrincipal() instanceof User user)) {
            log.info("principal is of type {}", authentication.getPrincipal().getClass());
            throw new IllegalStateException("Principal is not an instance of user");
        }
        String username = user.getUsername();
        Date now = new Date();
        String authorities = user.getAuthorities().stream()
                .map(Authority::getName)
                .collect(Collectors.joining(" "));

        return Jwts.builder()
                .setHeader(Map.of("alg", "RS256", "typ", "JWT"))
                .setSubject(username)
                .setIssuer("OnlineBankingSystem")
                .claim("scope", authorities)
                .setIssuedAt(now)
                .signWith(privateKey, SignatureAlgorithm.RS256)
                .compact();

    }
}