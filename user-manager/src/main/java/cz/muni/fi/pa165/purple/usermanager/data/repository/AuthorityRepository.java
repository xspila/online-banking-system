package cz.muni.fi.pa165.purple.usermanager.data.repository;

import cz.muni.fi.pa165.purple.usermanager.data.model.Authority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorityRepository extends JpaRepository<Authority, Long> {
    Authority findByName(String name);

    @Query("SELECT a FROM Authority a WHERE a.name = 'CUSTOMER' OR a.name = 'test_1'")
    List<Authority> getCustomerAuthories();
}
