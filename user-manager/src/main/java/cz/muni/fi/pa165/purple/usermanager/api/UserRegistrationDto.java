package cz.muni.fi.pa165.purple.usermanager.api;

import cz.muni.fi.pa165.purple.usermanager.data.enums.Country;
import cz.muni.fi.pa165.purple.usermanager.data.model.Contact;

import java.io.Serializable;

public record UserRegistrationDto (
        String username,
        String password,
        String firstName,
        String lastName,
        Country nationality,
        String phoneNumber,
        String mobilePhoneNubmer,
        String email,
        String street,
        String city,
        String state,
        String country,
        String postalCode,
        Boolean isEmployee
) implements Serializable {
}
