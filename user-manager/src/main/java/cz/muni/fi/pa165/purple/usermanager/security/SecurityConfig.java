package cz.muni.fi.pa165.purple.usermanager.security;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManagerResolver;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.SecurityFilterChain;

import static cz.muni.fi.pa165.purple.usermanager.data.Consts.CUSTOMER_SCOPE;
import static cz.muni.fi.pa165.purple.usermanager.data.Consts.EMPLOYEE_SCOPE;
import static cz.muni.fi.pa165.purple.usermanager.data.Consts.OAUTH2_ACCESS_SCOPE1;
import static cz.muni.fi.pa165.purple.usermanager.data.Consts.OAUTH2_ACCESS_SCOPE2;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@Configuration
public class SecurityConfig {

    @Autowired
    private AuthenticationManagerResolver<HttpServletRequest> tokenAuthenticationManagerResolver;

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(x -> x
                        .requestMatchers("/users/login").hasAnyAuthority(OAUTH2_ACCESS_SCOPE1, OAUTH2_ACCESS_SCOPE2)
                        .requestMatchers("/users/**").hasAnyAuthority(EMPLOYEE_SCOPE, CUSTOMER_SCOPE)
                        .anyRequest().permitAll()
                )
                .oauth2Login(Customizer.withDefaults())
                .csrf(AbstractHttpConfigurer::disable)
                .sessionManagement(sessionConfig -> sessionConfig.sessionCreationPolicy(STATELESS))
                .oauth2ResourceServer(oauth -> oauth.authenticationManagerResolver(tokenAuthenticationManagerResolver))
        ;
        return http.build();
    }
}
