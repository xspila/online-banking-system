package cz.muni.fi.pa165.purple.usermanager.utils;

import java.util.regex.Pattern;

public class InputValidator {
    private static final String EMAIL_REGEX = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
    private static final Pattern EMAIL_VALIDATION_PATTERN = Pattern.compile(EMAIL_REGEX, Pattern.CASE_INSENSITIVE);
    private static final String PHONE_NUMBER_REGEX = "^(\\+\\d{1,3}( )?)?((\\(\\d{1,3}\\))|\\d{1,3})[- .]?\\d{3,4}[- .]?\\d{4}$";
    private static final Pattern PHONE_NUMBER_VALIDATION_PATTERN = Pattern.compile(PHONE_NUMBER_REGEX);

    private InputValidator() {
    }

    public static Boolean validatePhoneNumber(String phoneNumber) {
        return PHONE_NUMBER_VALIDATION_PATTERN.matcher(phoneNumber).matches();
    }

    public static Boolean validateEmail(String email) {
        return EMAIL_VALIDATION_PATTERN.matcher(email).matches();
    }
}
