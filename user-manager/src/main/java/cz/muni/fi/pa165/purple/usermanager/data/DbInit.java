package cz.muni.fi.pa165.purple.usermanager.data;

import cz.muni.fi.pa165.purple.usermanager.data.model.Authority;
import cz.muni.fi.pa165.purple.usermanager.data.repository.AuthorityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
@RequiredArgsConstructor
public class DbInit {

    private final AuthorityRepository authorityRepository;

    @Bean
    public CommandLineRunner initDatabase() {
        return args -> {
            // Create and save default authorities
            // check if they exist
            List<Authority> missingAuthorities = new ArrayList<>();
            List<Authority> authorities = authorityRepository.findAll();
            if (authorities.stream().noneMatch(a -> a.getName().equals("EMPLOYEE"))) {
                Authority employeeAuthority = new Authority();
                employeeAuthority.setName("EMPLOYEE");
                missingAuthorities.add(employeeAuthority);
            }
            if (authorities.stream().noneMatch(a -> a.getName().equals("CUSTOMER"))) {
                Authority customerAuthority = new Authority();
                customerAuthority.setName("CUSTOMER");
                missingAuthorities.add(customerAuthority);
            }
            if (authorities.stream().noneMatch(a -> a.getName().equals("test_1"))) {
                Authority testAuthority = new Authority();
                testAuthority.setName("test_1");
                missingAuthorities.add(testAuthority);
            }
            authorityRepository.saveAll(missingAuthorities);
        };
    }
}
