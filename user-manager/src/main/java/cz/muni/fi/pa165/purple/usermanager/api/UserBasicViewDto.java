package cz.muni.fi.pa165.purple.usermanager.api;

public record UserBasicViewDto(
        String userName,
        String name,
        String email,
        Boolean isEmployee
) {
}
