package cz.muni.fi.pa165.purple.usermanager.data.model;

import cz.muni.fi.pa165.purple.usermanager.data.enums.ActionType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.io.Serializable;

@Data
@Entity
@Table(name = "ACTION_LOG")
public class ActionLog implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @NotBlank
    @ManyToOne()
    @JoinColumn(name = "USER_ID")
    private User app_user;

    @NotBlank
    @Column(name = "TIMESTAMP")
    private Long timestamp;

    @NotBlank
    @Enumerated(EnumType.STRING)
    @Column(name = "ACTION_TYPE")
    private ActionType actionType;

    public ActionLog() {
    }

    public ActionLog(ActionType actionType) {
        timestamp = System.currentTimeMillis();
        this.actionType = actionType;
    }
}