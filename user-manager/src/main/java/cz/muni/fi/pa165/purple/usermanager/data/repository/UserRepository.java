package cz.muni.fi.pa165.purple.usermanager.data.repository;

import cz.muni.fi.pa165.purple.usermanager.data.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("SELECT u FROM User u WHERE u.id = :id")
    User findByid(Long id);

    @Query("SELECT u FROM User u JOIN FETCH u.authorities WHERE u.username = :username")
    User findByUsername(String username);

    @Query("SELECT u FROM User u")
    List<User> getAllUsers();
}