package cz.muni.fi.pa165.purple.usermanager.data.enums;

public enum ActionType {
    ACCOUNT_CREATION,
    ACCOUNT_ACTIVATION,
    ACCOUNT_DEACTIVATION,
    ACCOUNT_DELETION,
    LOGIN,
    PASSWORD_CHANGE
}
