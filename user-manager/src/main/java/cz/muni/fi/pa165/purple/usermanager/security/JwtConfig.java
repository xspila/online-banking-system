package cz.muni.fi.pa165.purple.usermanager.security;

import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.proc.DefaultJOSEObjectTypeVerifier;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationManagerResolver;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationProvider;

import java.security.interfaces.RSAPublicKey;

@Configuration
public class JwtConfig {

    @Autowired
    private JwtService jwtService;

    @Value("${spring.security.oauth2.resourceserver.jwt.issuer-uri}")
    private String jwkSetUri;

    @Bean(name = "jwtDecoderMuni")
    public JwtDecoder jwtDecoderMuni() {
        // Customize the processor to accept "at+jwt" type
        return NimbusJwtDecoder.withIssuerLocation(this.jwkSetUri).jwtProcessorCustomizer(jwtProcessorCustomizer ->
                jwtProcessorCustomizer.setJWSTypeVerifier(new DefaultJOSEObjectTypeVerifier<>(new JOSEObjectType("at+jwt"),
                        new JOSEObjectType("JWT")))).build();
    }

    @Bean(name = "jwtDecoderLocal")
    public JwtDecoder jwtDecoderLocal() {
        return NimbusJwtDecoder.withPublicKey((RSAPublicKey) jwtService.publicKey).build();
    }

    @Bean
    AuthenticationManagerResolver<HttpServletRequest> tokenAuthenticationManagerResolver
            (@Qualifier(value = "jwtDecoderMuni") JwtDecoder jwtDecoderMuni,
             @Qualifier(value = "jwtDecoderLocal") JwtDecoder jwtDecoderLocal) {
        AuthenticationManager muniProvider = new ProviderManager(new JwtAuthenticationProvider(jwtDecoderMuni));
        AuthenticationManager localProvider = new ProviderManager(new JwtAuthenticationProvider(jwtDecoderLocal));
        return request -> {
            if ("/users/login".equals(request.getServletPath())) {
                return muniProvider;
            } else {
                return localProvider;
            }
        };
    }

    @Bean
    public PasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public JWKSet jwkSet() {
        RSAKey.Builder builder = new RSAKey.Builder((RSAPublicKey) jwtService.publicKey)
                .keyUse(KeyUse.SIGNATURE)
                .algorithm(JWSAlgorithm.RS256)
                .keyID("pa165");
        return new JWKSet(builder.build());
    }
}
