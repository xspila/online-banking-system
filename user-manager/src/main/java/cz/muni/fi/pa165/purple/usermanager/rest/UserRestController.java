package cz.muni.fi.pa165.purple.usermanager.rest;

import cz.muni.fi.pa165.purple.usermanager.api.UserBasicViewDto;
import cz.muni.fi.pa165.purple.usermanager.api.UserDetailedViewDto;
import cz.muni.fi.pa165.purple.usermanager.api.UserRegistrationDto;
import cz.muni.fi.pa165.purple.usermanager.data.enums.AccountState;
import cz.muni.fi.pa165.purple.usermanager.data.enums.ActionType;
import cz.muni.fi.pa165.purple.usermanager.exceptions.UserNotFoundException;
import cz.muni.fi.pa165.purple.usermanager.facade.UserFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/users")
public class UserRestController {

    private final UserFacade userFacade;

    @Autowired
    public UserRestController(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<UserBasicViewDto> findById(@PathVariable("id") Long id) {
        try {
            return ResponseEntity.ok(userFacade.findById(id));
        } catch (UserNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(path = "/{id}/state")
    public ResponseEntity<AccountState> stateById(@PathVariable("id") Long id) {
        try {
            return ResponseEntity.ok(userFacade.getUserState(id));
        } catch (UserNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(path = "/{id}/details")
    public ResponseEntity<UserDetailedViewDto> findByIdDetailed(@PathVariable("id") Long id) {
        try {
            return ResponseEntity.ok(userFacade.findByIdDetailed(id));
        } catch (UserNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(path = "/login")
    public ResponseEntity<String> login(@RequestParam("idToken") String idToken) {
        String token = userFacade.login(idToken);
        if (token == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        return ResponseEntity.ok(token);
    }

    @GetMapping
    public ResponseEntity<List<UserBasicViewDto>> getAllUsers() {
        return ResponseEntity.ok(userFacade.getAllUsers());
    }

    @GetMapping(path = "/details")
    public ResponseEntity<List<UserDetailedViewDto>> getAllUsersDetailed() {
        return ResponseEntity.ok(userFacade.getAllUsersDetailed());
    }

    @PostMapping(path = "/{id}/activate")
    public ResponseEntity<Void> activateUser(@PathVariable("id") Long id) {
        return performAction(id, ActionType.ACCOUNT_ACTIVATION);
    }

    @PostMapping(path = "/{id}/deactivate")
    public ResponseEntity<Void> deactivateUser(@PathVariable("id") Long id) {
        return performAction(id, ActionType.ACCOUNT_DEACTIVATION);
    }

    @PostMapping(path = "/{id}/delete")
    public ResponseEntity<Void> deleteUser(@PathVariable("id") Long id) {
        return performAction(id, ActionType.ACCOUNT_DELETION);
    }

    private ResponseEntity<Void> performAction(Long id, ActionType actionType) {
        if(userFacade.getUserState(id) == AccountState.DELETED)
        {
            return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).build();
        }

        try {
            userFacade.performAction(id, actionType);
            return ResponseEntity.ok().build();
        } catch (UserNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(path = "create")
    public ResponseEntity<Void> createUser(UserRegistrationDto user) {
        userFacade.createUser(user);
        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "/search/username/{username}")
    public ResponseEntity<UserBasicViewDto> searchByUsername(@PathVariable("username") String username) {
        try {
            return ResponseEntity.ok(userFacade.findByUsername(username));
        } catch (UserNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
