package cz.muni.fi.pa165.purple.usermanager.facade;

import cz.muni.fi.pa165.purple.usermanager.api.UserBasicViewDto;
import cz.muni.fi.pa165.purple.usermanager.api.UserDetailedViewDto;
import cz.muni.fi.pa165.purple.usermanager.api.UserRegistrationDto;
import cz.muni.fi.pa165.purple.usermanager.data.enums.AccountState;
import cz.muni.fi.pa165.purple.usermanager.data.enums.ActionType;
import cz.muni.fi.pa165.purple.usermanager.data.model.ActionLog;
import cz.muni.fi.pa165.purple.usermanager.data.model.User;
import cz.muni.fi.pa165.purple.usermanager.mappers.UserMapper;
import cz.muni.fi.pa165.purple.usermanager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

import javax.swing.*;
import java.util.List;

@Service
public class UserFacade {
    private final UserService userService;
    private final UserMapper userMapper;


    @Autowired
    public UserFacade(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    public void createUser (UserRegistrationDto registrationDto) {
        User user = userMapper.mapToUser(registrationDto);
        user.performAction(ActionType.ACCOUNT_CREATION);
        userService.createUser(user);
    }

    public AccountState getUserState(Long id) {
        User user = userService.findById(id);
        return user.getAccountState();
    }

    public UserBasicViewDto findById(Long id) {
        User user = userService.findById(id);
        return userMapper.mapToBasicDto(user);
    }

    public String login(String idToken) throws AuthenticationException {
        return userService.loginUser(idToken);
    }

    public UserBasicViewDto findByUsername(String username) {
        User user = userService.findByUsername(username);
        return userMapper.mapToBasicDto(user);
    }

    public List<UserBasicViewDto> getAllUsers() {
        List<User> users = userService.getAllUsers();
        return userMapper.mapToBasicList(users);
    }

    public UserDetailedViewDto findByIdDetailed(Long id) {
        User user = userService.findById(id);
        return userMapper.mapToDetailedDto(user);
    }

    public List<UserDetailedViewDto> getAllUsersDetailed() {
        List<User> users = userService.getAllUsers();
        return userMapper.mapToDetailedList(users);
    }


    public Boolean performAction(Long id, ActionType actionType) {
        User user;
        try {
            user = userService.findById(id);
        } catch (RuntimeException e) {
            return false;
        }
        user.performAction(actionType);
        userService.flush();
        return true;
    }
}
