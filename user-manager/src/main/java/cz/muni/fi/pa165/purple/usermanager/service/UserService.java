package cz.muni.fi.pa165.purple.usermanager.service;

import cz.muni.fi.pa165.purple.usermanager.api.UserRegistrationDto;
import cz.muni.fi.pa165.purple.usermanager.data.model.ActionLog;
import cz.muni.fi.pa165.purple.usermanager.data.model.Authority;
import cz.muni.fi.pa165.purple.usermanager.data.model.Contact;
import cz.muni.fi.pa165.purple.usermanager.data.model.User;
import cz.muni.fi.pa165.purple.usermanager.data.repository.AuthorityRepository;
import cz.muni.fi.pa165.purple.usermanager.data.repository.UserRepository;
import cz.muni.fi.pa165.purple.usermanager.security.JwtService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final JwtDecoder jwtDecoderMuni;

    @Value("${default.password:passwd}")
    private String defaultPassword;

    @Transactional
    public void createUser(User user) {
        userRepository.saveAndFlush(user);
    }

    @Transactional
    public void flush() {
        userRepository.flush();
    }

    @Transactional(readOnly = true)
    public User findById(Long id) {
        return userRepository.findByid(id);
    }

    @Transactional(readOnly = true)
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Transactional
    public String loginUser(String idToken) {
        Jwt jwt = jwtDecoderMuni.decode(idToken);
        User userBo = userRepository.findByUsername(jwt.getSubject());
        if (userBo == null) {
            Contact contact = new Contact();
            contact.setEmail(jwt.getClaimAsString("email"));
            userBo = new User();
            userBo.setUsername(jwt.getSubject());
            userBo.setFirstName(jwt.getClaimAsString("given_name"));
            userBo.setLastName(jwt.getClaimAsString("family_name"));
            userBo.setContact(contact);
            userBo.setAuthorities(authorityRepository.getCustomerAuthories());
            createCustomer(userBo);
        }
        return jwtService.generateToken(new UsernamePasswordAuthenticationToken(userBo, null, userBo.convertAuthorities()));
    }

    @Transactional(readOnly = true)
    public List<User> getAllUsers() {
        return userRepository.getAllUsers();
    }

    private Boolean createCustomer(User user) {
        try {
            List<Authority> authorities = authorityRepository.getCustomerAuthories();
            user.setPassword(passwordEncoder.encode(defaultPassword));
            user.setAuthorities(authorities);
            userRepository.save(user);
            List<SimpleGrantedAuthority> grantedAuthorities = user.getAuthorities().stream()
                    .map(authority -> new SimpleGrantedAuthority(authority.getName()))
                    .toList();
            Authentication auth = new UsernamePasswordAuthenticationToken(user, null, grantedAuthorities);
            SecurityContextHolder.getContext().setAuthentication(auth);
        } catch (Exception e) {
            log.error("Error while creating customer: {}", e.getMessage());
            return false;
        }
        return true;
    }
}
