package cz.muni.fi.pa165.purple.usermanager.data.model;

import cz.muni.fi.pa165.purple.usermanager.data.enums.AccountState;
import cz.muni.fi.pa165.purple.usermanager.data.enums.ActionType;
import cz.muni.fi.pa165.purple.usermanager.data.enums.Country;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;

@Data
@Entity
@Table(name = "users")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @NotBlank
    @Column(name = "username", length = 50, unique = true)
    private String username;

    @NotBlank
    @Column(name = "password", length = 64)
    private String password;

    @NotBlank
    @Column(name = "FIRST_NAME", length = 64)
    private String firstName;

    @NotBlank
    @Column(name = "LAST_NAME", length = 64)
    private String lastName;

    @NotBlank
    @Enumerated(EnumType.STRING)
    @Column(name = "NATIONALITY")
    private Country nationality;

    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "CONTACT_ID", referencedColumnName = "ID")
    private Contact contact;

    @OneToMany(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            mappedBy = "app_user")
    private List<ActionLog> actionLogs;

    @Column(name = "IS_EMPLOYEE")
    private Boolean isEmployee;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_authorities",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "authority_id")
    )
    private List<Authority> authorities;

    public User() {
    }

    public User(String username, String password, String firstName, String lastName,
                Country nationality, Contact contact, List<ActionLog> actionLogs, Boolean isEmployee) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.nationality = nationality;
        this.contact = contact;
        this.actionLogs = actionLogs;
        this.isEmployee = isEmployee;
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String username, String password, boolean isEmployee) {
        this.username = username;
        this.password = password;
        this.isEmployee = isEmployee;
    }

    public String getFullName() {
        return getFirstName() + " " + getLastName();
    }

    public AccountState getAccountState() {
        if (isDeleted()) {
            return AccountState.DELETED;
        }
        ActionType lastActivationAction = getActionLogs().stream()
                .filter(log -> log.getActionType() == ActionType.ACCOUNT_ACTIVATION ||
                        log.getActionType() == ActionType.ACCOUNT_DEACTIVATION)
                .max(Comparator.comparing(ActionLog::getTimestamp))
                .map(ActionLog::getActionType)
                .orElse(ActionType.ACCOUNT_DEACTIVATION);
        if (lastActivationAction == ActionType.ACCOUNT_ACTIVATION) {
            return AccountState.ACTIVE;
        } else {
            if (getActionLogs().stream().noneMatch(log -> log.getActionType() == ActionType.ACCOUNT_ACTIVATION)) {
                return AccountState.NON_ACTIVATED;
            }
            return AccountState.DELETED;
        }
    }

    public long getLastLoginTimestamp() {
        return getActionLogs().stream()
                .filter(actionLog -> actionLog.getActionType() == ActionType.LOGIN)
                .max(Comparator.comparing(ActionLog::getTimestamp))
                .map(ActionLog::getTimestamp)
                .orElse(0L);
    }

    public long getLastPasswordChangeTimestamp() {
        return getActionLogs().stream()
                .filter(actionLog -> actionLog.getActionType() == ActionType.PASSWORD_CHANGE)
                .max(Comparator.comparing(ActionLog::getTimestamp))
                .map(ActionLog::getTimestamp)
                .orElse(0L);
    }

    public long getAccountCreationTimestamp() {
        return getActionLogs().stream()
                .filter(actionLog -> actionLog.getActionType() == ActionType.ACCOUNT_CREATION)
                .min(Comparator.comparing(ActionLog::getTimestamp))
                .map(ActionLog::getTimestamp)
                .orElseThrow();
    }

    private Boolean isDeleted() {
        return actionLogs.stream()
                .anyMatch(log -> log.getActionType() == ActionType.ACCOUNT_DELETION);

    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", nationality=" + nationality +
                ", isEmployee=" + isEmployee +
                '}';
    }

    public List<SimpleGrantedAuthority> convertAuthorities() {
        return authorities.stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getName()))
                .toList();
    }

    public ActionLog performAction(ActionType actionType) {
        ActionLog actionLog = new ActionLog(actionType);
        actionLog.setApp_user(this);
        getActionLogs().add(actionLog);
        return actionLog;
    }
}
