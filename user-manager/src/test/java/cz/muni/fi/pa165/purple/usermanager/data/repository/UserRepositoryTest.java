package cz.muni.fi.pa165.purple.usermanager.data.repository;

import cz.muni.fi.pa165.purple.usermanager.data.model.User;
import cz.muni.fi.pa165.purple.usermanager.util.TestDataFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DataJpaTest
public class UserRepositoryTest {
    @Autowired
    TestEntityManager testEntityManager;
    @Autowired
    private UserRepository userRepository;
    @MockBean
    private OpaqueTokenIntrospector opaqueTokenIntrospector;

    @BeforeEach
    void initData() {
        User user = TestDataFactory.getUserEntityFactory();

        testEntityManager.persist(user);
    }

    @AfterEach
    void clearPersistenceContext() {
        testEntityManager.clear();
    }

    @Test
    void findById_idExists_returnMatchingUser() {
        User user = userRepository.findById(1L).get();

        assertThat(user.getUsername()).isEqualTo(TestDataFactory.userEntity.getUsername());
    }

    @Test
    void findById_idDoesNotExist_returnNull() {
        var user = userRepository.findById(-1L);

        assertThat(user).isEmpty();
    }

    @Test
    void findById_returnMatchingUser() {
        assertThrows(ConstraintViolationException.class, () -> {
            testEntityManager.persistAndFlush(TestDataFactory.userEntity);
        });
    }

}
