package cz.muni.fi.pa165.purple.usermanager.util;

import cz.muni.fi.pa165.purple.usermanager.api.UserBasicViewDto;
import cz.muni.fi.pa165.purple.usermanager.data.enums.ActionType;
import cz.muni.fi.pa165.purple.usermanager.data.enums.Country;
import cz.muni.fi.pa165.purple.usermanager.data.model.ActionLog;
import cz.muni.fi.pa165.purple.usermanager.data.model.Address;
import cz.muni.fi.pa165.purple.usermanager.data.model.Contact;
import cz.muni.fi.pa165.purple.usermanager.data.model.User;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TestDataFactory {
    public static UserBasicViewDto userBasicViewDto = getUserUserBasicViewDtoFactory();
    public static User userEntity = getUserEntityFactory();

    private static UserBasicViewDto getUserUserBasicViewDtoFactory() {
        return new UserBasicViewDto("testUsername", "John Doe", "john.doe@email.com", false);
    }

    public static User getUserEntityFactory() {
        return new User(
                "testUsername",
                "passwordHash",
                "John",
                "Doe",
                Country.CZECH_REPUBLIC,
                new Contact(
                        "+420123456789",
                        "+420701234567",
                        "john.doe@email.com",
                        new Address(
                                "Brno",
                                "South Moravian",
                                "Czech Republic",
                                "Czech Republic",
                                "602 00"
                        )
                ),
                List.of(new ActionLog(ActionType.ACCOUNT_CREATION)),
                false
        );
    }
}

