package cz.muni.fi.pa165.purple.usermanager;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.purple.usermanager.api.UserBasicViewDto;
import cz.muni.fi.pa165.purple.usermanager.data.repository.UserRepository;
import cz.muni.fi.pa165.purple.usermanager.rest.JwkSetRestController;
import cz.muni.fi.pa165.purple.usermanager.util.ObjectConverter;
import cz.muni.fi.pa165.purple.usermanager.util.TestDataFactory;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest(webEnvironment = DEFINED_PORT)
@AutoConfigureMockMvc
@Testcontainers
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@WithMockUser(username = "admin", authorities = {"SCOPE_EMPLOYEE", "SCOPE_CUSTOMER", "SCOPE_test_1"})
class UserRestControllerIT {

    @Container
    static PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>(DockerImageName.parse("postgres:latest"));
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private JwkSetRestController jwkSetRestController;

    @DynamicPropertySource
    static void datasourceProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
        registry.add("spring.datasource.username", postgreSQLContainer::getUsername);
        registry.add("spring.datasource.password", postgreSQLContainer::getPassword);
    }

    @Test
    @Order(1)
    void testSaveTransaction() {
        createTestUsers();
    }

    @Test
    @Order(2)
    void testGetUserEndpoint() throws Exception {
        String responseJson = mockMvc.perform(get("/users/{id}", 1L)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        UserBasicViewDto response = ObjectConverter.convertJsonToObject(responseJson, UserBasicViewDto.class);

        assertThat(response).isEqualTo(TestDataFactory.userBasicViewDto);
    }

    private void createTestUsers() {
        userRepository.saveAndFlush(TestDataFactory.getUserEntityFactory());
    }


}