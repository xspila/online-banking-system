package cz.muni.fi.pa165.purple.usermanager.facade;

import cz.muni.fi.pa165.purple.usermanager.api.UserBasicViewDto;
import cz.muni.fi.pa165.purple.usermanager.mappers.UserMapper;
import cz.muni.fi.pa165.purple.usermanager.service.UserService;
import cz.muni.fi.pa165.purple.usermanager.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class UserFacadeTest {

    @Mock
    private UserService userService;
    @Mock
    private UserMapper userMapper;

    @InjectMocks
    private UserFacade userFacade;

    @Test
    void findById_userFound_returnsUser() {
        // Arrange
        Long searchedId = 1L;
        Mockito.when(userService.findById(searchedId)).thenReturn(TestDataFactory.userEntity);
        Mockito.when(userMapper.mapToBasicDto(any())).thenReturn(TestDataFactory.userBasicViewDto);

        // Act
        UserBasicViewDto foundEntity = userFacade.findById(searchedId);

        // Assert
        assertThat(foundEntity).isEqualTo(TestDataFactory.userBasicViewDto);
    }
}
