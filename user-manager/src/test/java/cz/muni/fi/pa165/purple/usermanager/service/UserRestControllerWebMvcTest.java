package cz.muni.fi.pa165.purple.usermanager.service;

import cz.muni.fi.pa165.purple.usermanager.api.UserBasicViewDto;
import cz.muni.fi.pa165.purple.usermanager.facade.UserFacade;
import cz.muni.fi.pa165.purple.usermanager.rest.UserRestController;
import cz.muni.fi.pa165.purple.usermanager.util.ObjectConverter;
import cz.muni.fi.pa165.purple.usermanager.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@WebMvcTest(controllers = UserRestController.class)
@WithMockUser(username = "admin", authorities = {"SCOPE_EMPLOYEE", "SCOPE_CUSTOMER", "SCOPE_test_1"})
class UserRestControllerWebMvcTest {

    private static final String TEST_USERNAME = TestDataFactory.userEntity.getUsername();
    private static final String INVALID_USERNAME = "INVALID_USERNAME";
    private static final long TEST_ID = 1L;
    private static final long INVALID_ID = -1L;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserFacade userFacade;

    @Test
    void findById_userFound_returnsUser() throws Exception {
        doReturn(TestDataFactory.userBasicViewDto).when(userFacade).findById(TEST_ID);

        String responseJson = mockMvc.perform(get("/users/{id}", TEST_ID)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        UserBasicViewDto response = ObjectConverter.convertJsonToObject(responseJson, UserBasicViewDto.class);

        assertThat(response).isEqualTo(TestDataFactory.userBasicViewDto);
    }

    @Test
    void findById_userNotFound_returnsEmptyResponse() throws Exception {
        Mockito.when(userFacade.findById(INVALID_ID)).thenReturn(null);

        String responseJson = mockMvc.perform(get("/users/{id}", INVALID_ID)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        assertThat(responseJson).isEmpty();
    }

    @Test
    void usernameSearch_userFound_returnsUser() throws Exception {
        Mockito.when(userFacade.findByUsername(TEST_USERNAME)).thenReturn(TestDataFactory.userBasicViewDto);

        String responseJson = mockMvc.perform(get("/users/search/username/{username}", TEST_USERNAME)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        UserBasicViewDto response = ObjectConverter.convertJsonToObject(responseJson, UserBasicViewDto.class);

        assertThat(response).isEqualTo(TestDataFactory.userBasicViewDto);
    }

    @Test
    void usernameSearch_userNotFound_returnsUser() throws Exception {
        Mockito.when(userFacade.findByUsername(INVALID_USERNAME)).thenReturn(null);

        String responseJson = mockMvc.perform(get("/users/search/username/{username}", INVALID_USERNAME)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        assertThat(responseJson).isEmpty();
    }
}
