package cz.muni.fi.pa165.purple.usermanager.rest;

import cz.muni.fi.pa165.purple.usermanager.api.UserBasicViewDto;
import cz.muni.fi.pa165.purple.usermanager.facade.UserFacade;
import cz.muni.fi.pa165.purple.usermanager.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ExtendWith(MockitoExtension.class)
public class UserRestControllerTest {
    private static final String TEST_USERNAME = TestDataFactory.userEntity.getUsername();
    private static final String INVALID_USERNAME = "INVALID_USERNAME";
    private static final long TEST_ID = 1L;
    private static final long INVALID_ID = -1L;
    @Mock
    private UserFacade userFacade;

    @Test
    public void findByUsername_userFound_returnsUser() {
        Mockito.when(userFacade.findByUsername(TEST_USERNAME)).thenReturn(TestDataFactory.userBasicViewDto);

        UserBasicViewDto foundDto = userFacade.findByUsername(TEST_USERNAME);

        assertThat(foundDto).isEqualTo(TestDataFactory.userBasicViewDto);
    }

    @Test
    public void findByUsername_userNotFound_returnsNull() {
        Mockito.when(userFacade.findByUsername(INVALID_USERNAME)).thenReturn(null);

        UserBasicViewDto foundDto = userFacade.findByUsername(INVALID_USERNAME);

        assertThat(foundDto).isNull();
    }

    @Test
    public void findById_userFound_returnsUser() {
        Mockito.when(userFacade.findById(TEST_ID)).thenReturn(TestDataFactory.userBasicViewDto);

        UserBasicViewDto foundDto = userFacade.findById(TEST_ID);

        assertThat(foundDto).isEqualTo(TestDataFactory.userBasicViewDto);
    }

    @Test
    public void findById_userNotFound_returnsNull() {
        Mockito.when(userFacade.findById(INVALID_ID)).thenReturn(null);

        UserBasicViewDto foundDto = userFacade.findById(INVALID_ID);

        assertThat(foundDto).isNull();
    }
}
