package cz.muni.fi.pa165.purple.usermanager.service;

import cz.muni.fi.pa165.purple.usermanager.data.model.User;
import cz.muni.fi.pa165.purple.usermanager.data.repository.UserRepository;
import cz.muni.fi.pa165.purple.usermanager.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @Test
    void findByid_person_Found_returnsPerson() {
        // Arrange
        Long searchedId = 1L;
        Mockito.when(userRepository.findByid(searchedId)).thenReturn(TestDataFactory.userEntity);

        // Act
        User foundEntity = userService.findById(searchedId);

        // Assert
        assertThat(foundEntity).isEqualTo(TestDataFactory.userEntity);
    }
}
