package cz.muni.fi.pa165.purple.transactionmanager.service;

import cz.muni.fi.pa165.purple.transactionmanager.data.model.Transaction;
import cz.muni.fi.pa165.purple.transactionmanager.data.repository.TransactionRepository;
import cz.muni.fi.pa165.purple.transactionmanager.rest.specification.SpecificationFilterDto;
import cz.muni.fi.pa165.purple.transactionmanager.rest.specification.TransactionSpecification;
import cz.muni.fi.pa165.purple.transactionmanager.util.TestDataFactory;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import static org.assertj.core.api.Assertions.assertThat;


@ExtendWith(MockitoExtension.class)
public class TransactionServiceTest {
    @Mock
    private TransactionRepository transactionRepository;

    @InjectMocks
    private TransactionService transactionService;

    @Test
    void getAllTransactions_dataFound_returnsAllTransactions() {
        var pageable = PageRequest.of(0, 10);
        var specification = TransactionSpecification.specificationFromFilter(SpecificationFilterDto.builder().build());

        Page<Transaction> transactions = new PageImpl<>(List.of(TestDataFactory.getImmediate(), TestDataFactory.getDeposit(), TestDataFactory.getImmediate()));

        Mockito.when(transactionRepository.findAll(specification, pageable))
                .thenReturn(transactions);

        Page<Transaction> found = transactionService.getAllTransactions(pageable, specification);

        assertThat(found).isEqualTo(transactions);
    }

    @Test
    void getAllTransactions_dataNoFound_returnsEmptyList() {
        var pageable = PageRequest.of(0, 10);
        var specification = TransactionSpecification.specificationFromFilter(SpecificationFilterDto.builder().build());

        Page<Transaction> transactions = new PageImpl<>(List.of());

        Mockito.when(transactionRepository.findAll(specification, pageable))
                .thenReturn(transactions);

        Page<Transaction> found = transactionService.getAllTransactions(pageable, specification);

        assertThat(found).isEqualTo(transactions);
    }

    @Test
    void getTransactionById_transactionFound_returnsTransaction() {
        Mockito.when(transactionRepository.findById(1L)).thenReturn(Optional.of(TestDataFactory.getTransaction()));

        Transaction foundEntity = transactionService.getTransactionById(1L);

        assertThat(foundEntity).isEqualTo(TestDataFactory.getTransaction());
    }

    @Test
    void getAllTransactionsWithIban_findsAllTransaction_returnsPaged(){
        var pageable = PageRequest.of(0, 10);
        var iban = "123";

        Page<Transaction> transactions = new PageImpl<>(List.of(TestDataFactory.getImmediate(), TestDataFactory.getDeposit(), TestDataFactory.getImmediate()));

        Mockito.when(transactionRepository.findByIban(pageable, iban))
                .thenReturn(transactions);

        Page<Transaction> found = transactionService.getAllTransactionsWithIban(pageable, iban);

        assertThat(found).isEqualTo(transactions);
    }

    @Test
    void getAllTransactionsWithIban_noIbanTransactions_returnsEmptyPage(){
        var pageable = PageRequest.of(0, 10);
        var iban = "696969";

        Page<Transaction> transactions = new PageImpl<>(List.of());

        Mockito.when(transactionRepository.findByIban(pageable, iban))
                .thenReturn(transactions);

        Page<Transaction> found = transactionService.getAllTransactionsWithIban(pageable, iban);

        assertThat(found).isEqualTo(transactions);
    }

    @Test
    void makeWithdrawal_newValidTransaction_returnsNewTransaction(){
        Mockito.when(transactionRepository.save(TestDataFactory.getWithdrawal()))
                .thenReturn(TestDataFactory.getWithdrawal());

        Transaction newTransaction = transactionService.makeWithdrawal(TestDataFactory.getWithdrawal());

        assertThat(newTransaction).isEqualTo(TestDataFactory.getWithdrawal());
    }

    @Test
    void makeWithdrawal_transactionInvalid_returnsNull(){
        var request = TestDataFactory.getWithdrawal();
        request.setType(null);

        Mockito.when(transactionRepository.save(request))
                .thenReturn(null);

        Transaction newTransaction = transactionService.makeWithdrawal(request);

        assertThat(newTransaction).isEqualTo(null);
    }

    @Test
    void makeImmediateTransaction_newValidTransaction_returnsNewTransaction(){
        Mockito.when(transactionRepository.save(TestDataFactory.getImmediate()))
                .thenReturn(TestDataFactory.getImmediate());

        Transaction newTransaction = transactionService.makeImmediateTransaction(TestDataFactory.getImmediate());

        assertThat(newTransaction).isEqualTo(TestDataFactory.getImmediate());
    }

    @Test
    void makeImmediateTransaction_transactionInvalid_returnsNull(){
        var request = TestDataFactory.getImmediate();
        request.setCurrency(null);

        Mockito.when(transactionRepository.save(request))
                .thenReturn(null);

        Transaction newTransaction = transactionService.makeImmediateTransaction(request);

        assertThat(newTransaction).isEqualTo(null);
    }

    @Test
    void makeDeposit_newValidTransaction_returnsNewTransaction(){
        Mockito.when(transactionRepository.save(TestDataFactory.getDeposit()))
                .thenReturn(TestDataFactory.getDeposit());

        Transaction newTransaction = transactionService.makeDeposit(TestDataFactory.getDeposit());

        assertThat(newTransaction).isEqualTo(TestDataFactory.getDeposit());
    }

    @Test
    void makeDeposit_transactionInvalid_returnsNull(){
        var request = TestDataFactory.getDeposit();
        request.setToIban(null);

        Mockito.when(transactionRepository.save(request))
                .thenReturn(null);

        Transaction newTransaction = transactionService.makeDeposit(request);

        assertThat(newTransaction).isEqualTo(null);
    }

}

