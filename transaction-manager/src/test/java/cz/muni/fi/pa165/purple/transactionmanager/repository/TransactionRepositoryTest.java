package cz.muni.fi.pa165.purple.transactionmanager.repository;

import cz.muni.fi.pa165.purple.transactionmanager.data.enums.Currency;
import cz.muni.fi.pa165.purple.transactionmanager.data.enums.TransactionType;
import cz.muni.fi.pa165.purple.transactionmanager.data.model.Deposit;
import cz.muni.fi.pa165.purple.transactionmanager.data.model.Immediate;
import cz.muni.fi.pa165.purple.transactionmanager.data.model.Transaction;
import cz.muni.fi.pa165.purple.transactionmanager.data.model.Withdrawal;
import cz.muni.fi.pa165.purple.transactionmanager.data.repository.TransactionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@DataJpaTest
@ActiveProfiles("test")
public class TransactionRepositoryTest {
    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    @MockBean
    private JwtDecoder jwtDecoder;

    private Withdrawal withdrawal;
    private Deposit deposit;
    private Immediate immediate;

    @BeforeEach
    void initData() {
        deposit = createDeposit("123", 420, Currency.EUR, LocalDateTime.of(2024, 4, 21, 12, 0));
        withdrawal = createWithdrawal("123", 69, Currency.USD, LocalDateTime.of(2024, 4, 22, 12, 0));
        immediate = createImmediate("123", "789", 120, Currency.CZK, LocalDateTime.of(2024, 4, 23, 12, 0));
    }

    private Immediate createImmediate(String to, String from, double amount, Currency currency, LocalDateTime date){
        Immediate immediate = new Immediate();
        immediate.setReceiverIban(to);
        immediate.setSenderIban(from);
        immediate.setAmount(amount);
        immediate.setType(TransactionType.IMMEDIATE);
        immediate.setCurrency(currency);
        immediate.setTimestamp(date);
        return testEntityManager.persistFlushFind(immediate);
    }

    private Deposit createDeposit(String to, double amount, Currency currency, LocalDateTime date){
        Deposit deposit = new Deposit();
        deposit.setToIban(to);
        deposit.setAmount(amount);
        deposit.setType(TransactionType.DEPOSIT);
        deposit.setCurrency(currency);
        deposit.setTimestamp(date);
        return testEntityManager.persistFlushFind(deposit);
    }

    private Withdrawal createWithdrawal(String from, double amount, Currency currency, LocalDateTime date){
        Withdrawal withdrawal = new Withdrawal();
        withdrawal.setFromIban(from);
        withdrawal.setAmount(amount);
        withdrawal.setType(TransactionType.WITHDRAWAL);
        withdrawal.setCurrency(currency);
        withdrawal.setTimestamp(date);
        return testEntityManager.persistFlushFind(withdrawal);
    }

    @Test
    void findByIban_ibanExists_returnMatchingTransactions() {
        testEntityManager.persist(immediate);
        testEntityManager.persist(deposit);
        testEntityManager.persist(withdrawal);
        testEntityManager.flush();

        Page<Transaction> found = transactionRepository.findByIban(Pageable.ofSize(10), "123");

        assertThat(found.getTotalElements()).isEqualTo(3);
    }

    @Test
    void findByIban_ibanNOtFound_returnEmptyPage() {
        testEntityManager.persist(immediate);
        testEntityManager.persist(deposit);
        testEntityManager.flush();

        Page<Transaction> found = transactionRepository.findByIban(Pageable.ofSize(10), "420");

        assertThat(found.getTotalElements()).isZero();
    }
}
