package cz.muni.fi.pa165.purple.transactionmanager.facade;

import cz.muni.fi.pa165.purple.transactionmanager.api.TransactionDto;
import cz.muni.fi.pa165.purple.transactionmanager.data.model.Transaction;
import cz.muni.fi.pa165.purple.transactionmanager.facade.webclient.AccountManagerClient;
import cz.muni.fi.pa165.purple.transactionmanager.mappers.TransactionMapper;
import cz.muni.fi.pa165.purple.transactionmanager.rest.specification.SpecificationFilterDto;
import cz.muni.fi.pa165.purple.transactionmanager.rest.specification.TransactionSpecification;
import cz.muni.fi.pa165.purple.transactionmanager.service.TransactionService;
import cz.muni.fi.pa165.purple.transactionmanager.util.TestDataFactory;
import cz.muni.fi.pa165.purple.transactionmanager.data.enums.TransactionType;
import cz.muni.fi.pa165.purple.transactionmanager.data.enums.Currency;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import reactor.core.publisher.Mono;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class TransactionFacadeTest {
    @Mock
    private TransactionService transactionService;

    @Mock
    private TransactionMapper transactionMapper;

    @Mock
    private AccountManagerClient accountManagerClient;

    @InjectMocks
    private TransactionFacade transactionFacade;

    @Test
    void getTransactionById_TransactionFound_returnFoundTransaction(){
        Mockito.when(transactionService.getTransactionById(1L))
                .thenReturn(TestDataFactory.getTransaction());
        Mockito.when(transactionMapper.mapToTransactionDto(any()))
                .thenReturn(TestDataFactory.getDetailedTransactionDto());

        TransactionDto found = transactionFacade.getTransactionById(1L);

        assertThat(found).isEqualTo(TestDataFactory.getDetailedTransactionDto());
    }

    @Test
    void getTransactionById_NoTransactionFound_returnNone(){
        Mockito.when(transactionService.getTransactionById(6L))
                .thenReturn(null);
        Mockito.when(transactionMapper.mapToTransactionDto(any()))
                .thenReturn(null);

        TransactionDto found = transactionFacade.getTransactionById(6L);

        assertThat(found).isEqualTo(null);
    }

    @Test
    void getAllTransactions_noFiltering_returnPagedTransactions(){
        var pageable = PageRequest.of(0, 10);
        var specification = TransactionSpecification.specificationFromFilter(SpecificationFilterDto.builder().build());

        Page<Transaction> transactions = new PageImpl<>(List.of(TestDataFactory.getImmediate(), TestDataFactory.getDeposit()));
        Page<TransactionDto> dtos = new PageImpl<>(List.of(TestDataFactory.getImmediateDto(), TestDataFactory.getDepositDto()));

        Mockito.when(transactionService.getAllTransactions(pageable, specification))
                .thenReturn(transactions);
        Mockito.when(transactionMapper.mapToTransactionDtoPage(any()))
                .thenReturn(dtos);

        Page<TransactionDto> found = transactionFacade.getAllTransactions(pageable, specification);

        assertThat(found).isEqualTo(dtos);
    }

    @Test
    void getAllTransactions_withSpecificationFilter_returnTransactionFound(){
        var pageable = PageRequest.of(0, 10);
        var specification = TransactionSpecification.specificationFromFilter(SpecificationFilterDto.builder()
                .to("123").type(TransactionType.DEPOSIT).build());

        Page<Transaction> transactions = new PageImpl<>(List.of(TestDataFactory.getDeposit()));
        Page<TransactionDto> dtos = new PageImpl<>(List.of(TestDataFactory.getDepositDto()));

        Mockito.when(transactionService.getAllTransactions(pageable, specification))
                .thenReturn(transactions);
        Mockito.when(transactionMapper.mapToTransactionDtoPage(any()))
                .thenReturn(dtos);

        Page<TransactionDto> found = transactionFacade.getAllTransactions(pageable, specification);

        assertThat(found).isEqualTo(dtos);
    }

    @Test
    void getAllTransactions_withManyFilters_returnEmptyPage(){
        var pageable = PageRequest.of(0, 10);
        var specification = TransactionSpecification.specificationFromFilter(SpecificationFilterDto.builder()
                .to("123")
                .from("987")
                .min(69.123)
                .max(420.0)
                .currency(Currency.EUR)
                .until(LocalDateTime.of(2002, 12, 29, 7, 36))
                .build());

        Page<Transaction> transactions = new PageImpl<>(List.of());
        Page<TransactionDto> dtos = new PageImpl<>(List.of());

        Mockito.when(transactionService.getAllTransactions(pageable, specification))
                .thenReturn(transactions);
        Mockito.when(transactionMapper.mapToTransactionDtoPage(any()))
                .thenReturn(dtos);

        Page<TransactionDto> found = transactionFacade.getAllTransactions(pageable, specification);

        assertThat(found).isEqualTo(dtos);
    }

    @Test
    void getTransactionByIban_findsTransactions_returnsPagedTransactionWithIban() {
        var pageable = PageRequest.of(0, 10);
        var iban = "123";

        Page<Transaction> transactions = new PageImpl<>(List.of(TestDataFactory.getImmediate(), TestDataFactory.getDeposit(), TestDataFactory.getImmediate()));
        Page<TransactionDto> dtos = new PageImpl<>(List.of(TestDataFactory.getImmediateDto(), TestDataFactory.getDepositDto(), TestDataFactory.getImmediateDto()));

        Mockito.when(transactionService.getAllTransactionsWithIban(pageable, iban))
                .thenReturn(transactions);
        Mockito.when(transactionMapper.mapToTransactionDtoPage(any()))
                .thenReturn(dtos);

        Page<TransactionDto> found = transactionFacade.getTransactionByIban(pageable, iban);

        assertThat(found).isEqualTo(dtos);
    }

    @Test
    void getTransactionByIban_noTransactionsFound_returnsEmptyPaged() {
        var pageable = PageRequest.of(0, 10);
        var iban = "987";

        Page<Transaction> transactions = new PageImpl<>(List.of());
        Page<TransactionDto> dtos = new PageImpl<>(List.of());

        Mockito.when(transactionService.getAllTransactionsWithIban(pageable, iban))
                .thenReturn(transactions);
        Mockito.when(transactionMapper.mapToTransactionDtoPage(any()))
                .thenReturn(dtos);

        Page<TransactionDto> found = transactionFacade.getTransactionByIban(pageable, iban);

        assertThat(found).isEqualTo(dtos);
    }

    @Test
    void createWithdrawalTransaction_newValidTransaction_TransactionSuccessfullyAdded(){
        Mockito.when(transactionService.makeWithdrawal(TestDataFactory.getWithdrawal()))
                .thenReturn(TestDataFactory.getWithdrawal());
        Mockito.when(transactionMapper.mapToTransactionDto(any()))
                .thenReturn(TestDataFactory.getWithdrawalDto());
        Mockito.when(transactionMapper.mapToWithdrawal(TestDataFactory.getRequestWithdrawal()))
                .thenReturn(TestDataFactory.getWithdrawal());
        Mockito.when(accountManagerClient.makeWithdrawalRequest(TestDataFactory.getRequestWithdrawal()))
                .thenReturn(Mono.just(ResponseEntity.status(HttpStatus.OK).build()));

        TransactionDto found = transactionFacade.createWithdrawalTransaction(TestDataFactory.getRequestWithdrawal());

        assertThat(found).isEqualTo(TestDataFactory.getWithdrawalDto());
    }

    @Test
    void createWithdrawalTransaction_newInvalidTransaction_returnsNull(){
        Mockito.when(transactionService.makeWithdrawal(TestDataFactory.getWithdrawal()))
                .thenReturn(null);
        Mockito.when(transactionMapper.mapToTransactionDto(any()))
                .thenReturn(null);
        Mockito.when(transactionMapper.mapToWithdrawal(TestDataFactory.getRequestWithdrawal()))
                .thenReturn(TestDataFactory.getWithdrawal());
        Mockito.when(accountManagerClient.makeWithdrawalRequest(TestDataFactory.getRequestWithdrawal()))
                .thenReturn(Mono.just(ResponseEntity.status(HttpStatus.OK).build()));

        TransactionDto found = transactionFacade.createWithdrawalTransaction(TestDataFactory.getRequestWithdrawal());

        assertThat(found).isEqualTo(null);
    }

    @Test
    void createImmediateTransaction_newValidTransaction_TransactionSuccessfullyAdded(){
        Mockito.when(transactionService.makeImmediateTransaction(TestDataFactory.getImmediate()))
                .thenReturn(TestDataFactory.getImmediate());
        Mockito.when(transactionMapper.mapToTransactionDto(any()))
                .thenReturn(TestDataFactory.getImmediateDto());
        Mockito.when(transactionMapper.mapToImmediate(TestDataFactory.getRequestImmediate()))
                .thenReturn(TestDataFactory.getImmediate());
        Mockito.when(accountManagerClient.makeImmediateRequest(TestDataFactory.getRequestImmediate()))
                .thenReturn(Mono.just(ResponseEntity.status(HttpStatus.OK).build()));

        TransactionDto found = transactionFacade.createImmediateTransaction(TestDataFactory.getRequestImmediate());

        assertThat(found).isEqualTo(TestDataFactory.getImmediateDto());
    }

    @Test
    void createImmediateTransaction_newInvalidTransaction_returnsNull(){
        Mockito.when(transactionService.makeImmediateTransaction(TestDataFactory.getImmediate()))
                .thenReturn(null);
        Mockito.when(transactionMapper.mapToTransactionDto(any()))
                .thenReturn(null);
        Mockito.when(transactionMapper.mapToImmediate(TestDataFactory.getRequestImmediate()))
                .thenReturn(TestDataFactory.getImmediate());
        Mockito.when(accountManagerClient.makeImmediateRequest(TestDataFactory.getRequestImmediate()))
                .thenReturn(Mono.just(ResponseEntity.status(HttpStatus.OK).build()));

        TransactionDto found = transactionFacade.createImmediateTransaction(TestDataFactory.getRequestImmediate());

        assertThat(found).isEqualTo(null);
    }

    @Test
    void createDepositTransaction_newValidTransaction_TransactionSuccessfullyAdded(){
        Mockito.when(transactionService.makeDeposit(TestDataFactory.getDeposit()))
                .thenReturn(TestDataFactory.getDeposit());
        Mockito.when(transactionMapper.mapToTransactionDto(any()))
                .thenReturn(TestDataFactory.getDepositDto());
        Mockito.when(transactionMapper.mapToDeposit(TestDataFactory.getRequestDeposit()))
                .thenReturn(TestDataFactory.getDeposit());
        Mockito.when(accountManagerClient.makeDepositRequest(TestDataFactory.getRequestDeposit()))
                .thenReturn(Mono.just(ResponseEntity.status(HttpStatus.OK).build()));

        TransactionDto found = transactionFacade.createDepositTransaction(TestDataFactory.getRequestDeposit());

        assertThat(found).isEqualTo(TestDataFactory.getDepositDto());
    }

    @Test
    void createDepositTransaction_newInvalidTransaction_returnsNull(){
        Mockito.when(transactionService.makeDeposit(TestDataFactory.getDeposit()))
                .thenReturn(null);
        Mockito.when(transactionMapper.mapToTransactionDto(any()))
                .thenReturn(null);
        Mockito.when(transactionMapper.mapToDeposit(TestDataFactory.getRequestDeposit()))
                .thenReturn(TestDataFactory.getDeposit());
        Mockito.when(accountManagerClient.makeDepositRequest(TestDataFactory.getRequestDeposit()))
                .thenReturn(Mono.just(ResponseEntity.status(HttpStatus.OK).build()));

        TransactionDto found = transactionFacade.createDepositTransaction(TestDataFactory.getRequestDeposit());

        assertThat(found).isEqualTo(null);
    }

    @Test
    void createDepositTransaction_accountManagerFails_returnsNull(){
        Mockito.when(transactionService.makeDeposit(TestDataFactory.getDeposit()))
                .thenReturn(null);
        Mockito.when(transactionMapper.mapToTransactionDto(any()))
                .thenReturn(null);
        Mockito.when(transactionMapper.mapToDeposit(TestDataFactory.getRequestDeposit()))
                .thenReturn(TestDataFactory.getDeposit());
        Mockito.when(accountManagerClient.makeDepositRequest(TestDataFactory.getRequestDeposit()))
                .thenReturn(Mono.just(ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build()));

        TransactionDto found = transactionFacade.createDepositTransaction(TestDataFactory.getRequestDeposit());

        assertThat(found).isEqualTo(null);
    }
}
