package cz.muni.fi.pa165.purple.transactionmanager.util;

import cz.muni.fi.pa165.purple.transactionmanager.data.enums.TransactionType;
import cz.muni.fi.pa165.purple.transactionmanager.data.model.Deposit;
import cz.muni.fi.pa165.purple.transactionmanager.data.model.Immediate;
import cz.muni.fi.pa165.purple.transactionmanager.data.model.Transaction;
import cz.muni.fi.pa165.purple.transactionmanager.data.model.Withdrawal;
import cz.muni.fi.pa165.purple.transactionmanager.api.DepositDto;
import cz.muni.fi.pa165.purple.transactionmanager.api.ImmediateDto;
import cz.muni.fi.pa165.purple.transactionmanager.api.TransactionDto;
import cz.muni.fi.pa165.purple.transactionmanager.api.WithdrawalDto;
import cz.muni.fi.pa165.purple.transactionmanager.api.request.RequestDepositDto;
import cz.muni.fi.pa165.purple.transactionmanager.api.request.RequestImmediateDto;
import cz.muni.fi.pa165.purple.transactionmanager.api.request.RequestWithdrawalDto;
import cz.muni.fi.pa165.purple.transactionmanager.data.enums.Currency;

import java.time.LocalDateTime;

public class TestDataFactory {

    private static LocalDateTime localTime = LocalDateTime.of(2022, 12, 29, 9, 45, 2);
    public static TransactionDto getDetailedTransactionDto(){
        TransactionDto transaction = new TransactionDto();
        transaction.setId("1");
        transaction.setAmount(500);
        transaction.setType("IMMEDIATE");
        transaction.setCurrency(Currency.CZK);
        transaction.setTimestamp(localTime);
        return transaction;
    }

    public static Transaction getTransaction(){
        Transaction transaction = new Transaction();
        transaction.setId(1L);
        transaction.setAmount(500);
        transaction.setType(TransactionType.IMMEDIATE);
        transaction.setCurrency(Currency.CZK);
        transaction.setTimestamp(localTime);
        return transaction;
    }

    public static DepositDto getDepositDto(){
        DepositDto dto = new DepositDto();
        dto.setId("2");
        dto.setAmount(150);
        dto.setToIban("123");
        dto.setType("DEPOSIT");
        dto.setCurrency(Currency.USD);
        dto.setTimestamp(localTime);
        return dto;
    }

    public static Deposit getDeposit(){
        Deposit deposit = new Deposit();
        deposit.setId(2L);
        deposit.setAmount(150);
        deposit.setType(TransactionType.DEPOSIT);
        deposit.setTimestamp(localTime);
        deposit.setCurrency(Currency.USD);
        deposit.setToIban("123");
        return deposit;
    }

    public static RequestDepositDto getRequestDeposit(){
        RequestDepositDto depositDto = new RequestDepositDto();
        depositDto.setAmount(150);
        depositDto.setCurrency(Currency.USD);
        depositDto.setToIban("123");
        return depositDto;
    }

    public static WithdrawalDto getWithdrawalDto(){
        WithdrawalDto dto = new WithdrawalDto();
        dto.setId("3");
        dto.setAmount(69);
        dto.setFromIban("789");
        dto.setType("WITHDRAWAL");
        dto.setCurrency(Currency.EUR);
        dto.setTimestamp(localTime);
        return dto;
    }

    public static Withdrawal getWithdrawal(){
        Withdrawal withdrawal = new Withdrawal();
        withdrawal.setId(3L);
        withdrawal.setAmount(69);
        withdrawal.setType(TransactionType.WITHDRAWAL);
        withdrawal.setCurrency(Currency.EUR);
        withdrawal.setTimestamp(localTime);
        withdrawal.setFromIban("789");
        return withdrawal;
    }

    public static RequestWithdrawalDto getRequestWithdrawal(){
        RequestWithdrawalDto withdrawalDto = new RequestWithdrawalDto();
        withdrawalDto.setAmount(69);
        withdrawalDto.setCurrency(Currency.EUR);
        withdrawalDto.setFromIban("789");
        return withdrawalDto;
    }


    public static ImmediateDto getImmediateDto(){
        ImmediateDto dto = new ImmediateDto();
        dto.setId("4");
        dto.setAmount(420);
        dto.setSenderIban("123");
        dto.setReceiverIban("789");
        dto.setType("IMMEDIATE");
        dto.setCurrency(Currency.CZK);
        dto.setTimestamp(localTime);
        return dto;
    }

    public static Immediate getImmediate(){
        Immediate immediate = new Immediate();
        immediate.setId(4L);
        immediate.setAmount(420);
        immediate.setType(TransactionType.IMMEDIATE);
        immediate.setCurrency(Currency.CZK);
        immediate.setTimestamp(localTime);
        immediate.setReceiverIban("789");
        immediate.setSenderIban("123");
        return immediate;
    }

    public static RequestImmediateDto getRequestImmediate(){
        RequestImmediateDto immediateDto = new RequestImmediateDto();
        immediateDto.setAmount(69);
        immediateDto.setCurrency(Currency.EUR);
        immediateDto.setReceiverIban("789");
        immediateDto.setSenderIban("123");
        return immediateDto;
    }
}
