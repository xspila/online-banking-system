package cz.muni.fi.pa165.purple.transactionmanager.rest;

import cz.muni.fi.pa165.purple.transactionmanager.api.TransactionDto;
import cz.muni.fi.pa165.purple.transactionmanager.facade.TransactionFacade;
import cz.muni.fi.pa165.purple.transactionmanager.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@WithMockUser(username = "admin", authorities = {"SCOPE_EMPLOYEE", "SCOPE_CUSTOMER", "SCOPE_test_1"})
public class TransactionRestControllerTest {
    @Mock
    private TransactionFacade transactionFacade;

    @InjectMocks
    private TransactionRestController transactionRestController;

//    @Test
//    void getAllTransactions_notFilterApplied_returnsPage(){
//        Pageable pageable = PageRequest.of(0, 10);
//
//        Specification<Transaction> specification = TransactionSpecification.specificationFromFilter(new SpecificationFilterDto());
//        SpecificationFilterDto filter = SpecificationFilterDto.builder().build();
//
//        Page<TransactionDto> expectedPage = new PageImpl<>(List.of(TestDataFactory.getDepositDto(), TestDataFactory.getWithdrawalDto()));
//        when(transactionFacade.getAllTransactions(pageable, specification)).thenReturn(expectedPage);
//
//        Page<TransactionDto> result = transactionRestController.getAllTransactions(pageable, filter);
//
//        assertEquals(expectedPage, result);
//    }

    /* Test not working due to potential stabbing problems. TransactionsSpecifications needs to be @Bean.

    @Test
    void getAllTransactions_filteredByFromIban_returnsPage(){
        Pageable pageable = PageRequest.of(0, 10);

        var filter = SpecificationFilterDto.builder().from("123").build();
        Specification<Transaction> specification = TransactionSpecification.specificationFromFilter(filter);

        Page<TransactionDto> expectedPage = new PageImpl<>(List.of(TestDataFactory.getImmediateDto()));
        when(transactionFacade.getAllTransactions(pageable, specification)).thenReturn(expectedPage);

        Page<TransactionDto> result = transactionRestController.getAllTransactions(pageable, filter);

        assertThat(result).isEqualTo(expectedPage);
    }

    @Test
    void getAllTransactions_filteredByCurrency_returnsPage(){
        Pageable pageable = PageRequest.of(0, 10);

        SpecificationFilterDto filterDto = SpecificationFilterDto.builder().currency(Currency.CZK).build();
        Specification<Transaction> specification = TransactionSpecification.specificationFromFilter(filterDto);

        Page<TransactionDto> expectedPage = new PageImpl<>(List.of(TestDataFactory.getImmediateDto()));
        when(transactionFacade.getAllTransactions(pageable, specification)).thenReturn(expectedPage);

        Page<TransactionDto> result = transactionRestController.getAllTransactions(pageable, filterDto);

        assertEquals(expectedPage, result);
    }

    @Test
    void getAllTransactions_filteredByIbanAndCurrencyAndType_returnsEmptyPage(){
        Pageable pageable = PageRequest.of(0, 10);

        SpecificationFilterDto filter = SpecificationFilterDto.builder()
                .from("963")
                .to("852")
                .type(TransactionType.DEPOSIT)
                .currency(Currency.CZK)
                .build();
        Specification<Transaction> specification = Specification.where(null);

        Page<TransactionDto> expectedPage = new PageImpl<>(List.of());
        when(transactionFacade.getAllTransactions(pageable, specification)).thenReturn(expectedPage);

        Page<TransactionDto> result = transactionRestController.getAllTransactions(pageable,
                filter);

        assertEquals(expectedPage, result);
    }

    @Test
    void getAllTransactions_filteredByPriceRange_returnsPage(){
        Pageable pageable = PageRequest.of(0, 10);

        var filter = SpecificationFilterDto.builder().max(12.3).min(421.2).build();
        Specification<Transaction> specification = Specification.where(null);

        Page<TransactionDto> expectedPage = new PageImpl<>(List.of(TestDataFactory.getImmediateDto(), TestDataFactory.getDepositDto(), TestDataFactory.getWithdrawalDto()));
        when(transactionFacade.getAllTransactions(pageable, specification)).thenReturn(expectedPage);

        Page<TransactionDto> result = transactionRestController.getAllTransactions(pageable, filter);

        assertEquals(expectedPage, result);
    }*/

    @Test
    void getTransactionById_findsTransactionGoodId_returnDto(){
        when(transactionFacade.getTransactionById(1L)).thenReturn(TestDataFactory.getDetailedTransactionDto());

        var response = transactionRestController.getTransaction(1L);

        assertThat(response).isEqualTo(TestDataFactory.getDetailedTransactionDto());
    }

    @Test
    void getTransactionById_findsTransaction_returnDto(){
        when(transactionFacade.getTransactionById(4L)).thenReturn(TestDataFactory.getImmediateDto());

        var response = transactionRestController.getTransaction(4L);

        assertThat(response).isEqualTo(TestDataFactory.getImmediateDto());
    }

    @Test
    void getTransactionByIban_existingIban_returnsAllEntitiesWithIban(){
        Pageable pageable = PageRequest.of(0, 10);

        Page<TransactionDto> expectedPage = new PageImpl<>(List.of(TestDataFactory.getDepositDto(), TestDataFactory.getImmediateDto()));
        when(transactionFacade.getTransactionByIban(pageable, "123")).thenReturn(expectedPage);

        var response = transactionRestController.getTransactionByIban(pageable, "123");

        assertThat(response).isEqualTo(expectedPage);
    }

    @Test
    void getTransactionByIban_ibanWithNoTransactions_returnsEmptyPage(){
        Pageable pageable = PageRequest.of(0, 10);

        Page<TransactionDto> expectedPage = new PageImpl<>(Collections.emptyList());
        when(transactionFacade.getTransactionByIban(pageable, "741")).thenReturn(expectedPage);

        var response = transactionRestController.getTransactionByIban(pageable, "741");

        assertThat(response).isEqualTo(expectedPage);
    }

}