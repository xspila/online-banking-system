package cz.muni.fi.pa165.purple.transactionmanager;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.purple.transactionmanager.api.TransactionDto;
import cz.muni.fi.pa165.purple.transactionmanager.data.repository.TransactionRepository;
import cz.muni.fi.pa165.purple.transactionmanager.util.ObjectConverter;
import cz.muni.fi.pa165.purple.transactionmanager.util.TestDataFactory;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@SpringBootTest(webEnvironment = DEFINED_PORT)
@AutoConfigureMockMvc
@Testcontainers
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@WithMockUser(username = "admin", authorities = {"SCOPE_EMPLOYEE", "SCOPE_CUSTOMER", "SCOPE_test_1"})
public class TransactionManagerIT {
    @Container
    @ServiceConnection
    static PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>(DockerImageName.parse("postgres:latest"));

    @DynamicPropertySource
    static void datasourceProperties(DynamicPropertyRegistry registry) {
        registry.add("postgres.url", postgreSQLContainer::getJdbcUrl);
        registry.add("spring.datasource.username", postgreSQLContainer::getUsername);
        registry.add("spring.datasource.password", postgreSQLContainer::getPassword);
    }

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @Order(0)
    void testSaveTransaction() {
        createTestTransactions();
    }

    @Test
    @Order(1)
    void testIDEndpoint() throws Exception {
        String contentAsString = mockMvc.perform(MockMvcRequestBuilders.get("/transactions/{id}", 1L)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn().getResponse().getContentAsString();
        var response = ObjectConverter.convertJsonToObject(contentAsString, TransactionDto.class);

        assertThat(response.getId()).isEqualTo("1");
    }

    private void createTestTransactions() {
        transactionRepository.save(TestDataFactory.getDeposit());
        transactionRepository.save(TestDataFactory.getWithdrawal());
        transactionRepository.save(TestDataFactory.getImmediate());
    }

}
