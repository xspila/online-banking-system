package cz.muni.fi.pa165.purple.transactionmanager.exceptions;

import org.springframework.http.HttpStatus;

public class TransactionNotFoundException extends AbstractApiException{
    public TransactionNotFoundException(String message) {super(message, HttpStatus.NOT_FOUND);}

}
