package cz.muni.fi.pa165.purple.transactionmanager.api.request.accountservice;

import cz.muni.fi.pa165.purple.transactionmanager.data.enums.Currency;
import lombok.Data;

import java.io.Serializable;

@Data
public class AccountServiceRequestDto implements Serializable {
    private String iban;
    private Currency currency;
    private double amount;
}
