package cz.muni.fi.pa165.purple.transactionmanager.rest.specification;

import cz.muni.fi.pa165.purple.transactionmanager.data.model.Transaction;
import cz.muni.fi.pa165.purple.transactionmanager.data.enums.Currency;
import cz.muni.fi.pa165.purple.transactionmanager.data.enums.TransactionType;
import java.time.LocalDateTime;
import org.springframework.data.jpa.domain.Specification;

public class TransactionSpecification {

    public static Specification<Transaction> specificationFromFilter(SpecificationFilterDto filter){
        Specification<Transaction> specification = Specification.where(null);

        if (filter.getFrom() != null) {
            specification = specification.and(TransactionSpecification.fromIban(filter.getFrom()));
        }
        if (filter.getTo() != null) {
            specification = specification.and(TransactionSpecification.toIban(filter.getTo()));
        }
        if (filter.getType() != null) {
            specification = specification.and(TransactionSpecification.withType(filter.getType()));
        }
        if (filter.getCurrency() != null) {
            specification = specification.and(TransactionSpecification.withCurrency(filter.getCurrency()));
        }
        if (filter.getMax() != null) {
            specification = specification.and(TransactionSpecification.maxValue(filter.getMax()));
        }
        if (filter.getMin() != null) {
            specification = specification.and(TransactionSpecification.minValue(filter.getMin()));
        }
        if (filter.getSince() != null) {
            specification = specification.and(TransactionSpecification.sinceDate(filter.getSince()));
        }
        if (filter.getUntil() != null) {
            specification = specification.and(TransactionSpecification.untilDate(filter.getUntil()));
        }

        return specification;
    }
    public static Specification<Transaction> fromIban(String iban) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.or(
                        criteriaBuilder.equal(root.get("senderIban"), iban),
                        criteriaBuilder.equal(root.get("fromIban"), iban)
                );
    }

    public static Specification<Transaction> toIban(String iban) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.or(
                        criteriaBuilder.equal(root.get("receiverIban"), iban),
                        criteriaBuilder.equal(root.get("toIban"), iban)
                );
    }

    public static Specification<Transaction> withType(TransactionType type) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("type"), type);
    }

    public static Specification<Transaction> withCurrency(Currency currency) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("currency"), currency);
    }

    public static Specification<Transaction> minValue(Double min) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.greaterThanOrEqualTo(root.get("amount"), min);
    }

    public static Specification<Transaction> maxValue(Double max) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.lessThanOrEqualTo(root.get("amount"), max);
    }
    public static Specification<Transaction> sinceDate(LocalDateTime since) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.greaterThanOrEqualTo(root.get("timestamp"), since);
    }

    public static Specification<Transaction> untilDate(LocalDateTime until) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.lessThanOrEqualTo(root.get("timestamp"), until);
    }

}
