package cz.muni.fi.pa165.purple.transactionmanager.exceptions;

import org.springframework.http.HttpStatus;

public class TransactionNotAcceptedException extends AbstractApiException{
    public TransactionNotAcceptedException(String message) {super(message, HttpStatus.NOT_ACCEPTABLE);}
}
