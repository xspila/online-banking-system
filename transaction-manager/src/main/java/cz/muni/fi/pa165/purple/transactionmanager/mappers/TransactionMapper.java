package cz.muni.fi.pa165.purple.transactionmanager.mappers;

import cz.muni.fi.pa165.purple.transactionmanager.api.DepositDto;
import cz.muni.fi.pa165.purple.transactionmanager.api.ImmediateDto;
import cz.muni.fi.pa165.purple.transactionmanager.api.TransactionDto;
import cz.muni.fi.pa165.purple.transactionmanager.api.WithdrawalDto;
import cz.muni.fi.pa165.purple.transactionmanager.api.request.RequestDepositDto;
import cz.muni.fi.pa165.purple.transactionmanager.api.request.RequestImmediateDto;
import cz.muni.fi.pa165.purple.transactionmanager.api.request.RequestWithdrawalDto;
import cz.muni.fi.pa165.purple.transactionmanager.api.request.accountservice.AccountServiceImmediateRequestDto;
import cz.muni.fi.pa165.purple.transactionmanager.api.request.accountservice.AccountServiceRequestDto;
import cz.muni.fi.pa165.purple.transactionmanager.data.model.Deposit;
import cz.muni.fi.pa165.purple.transactionmanager.data.model.Immediate;
import cz.muni.fi.pa165.purple.transactionmanager.data.model.Transaction;
import cz.muni.fi.pa165.purple.transactionmanager.data.model.Withdrawal;
import org.apache.commons.lang3.NotImplementedException;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;

@Mapper(componentModel = "spring")
public interface TransactionMapper {

    default TransactionDto mapToTransactionDto (Transaction transaction){
        return switch (transaction.getClass().getSimpleName()) {
            case "Immediate" -> mapToImmediateDto((Immediate) transaction);
            case "Deposit" -> mapToDepositDto((Deposit) transaction);
            case "Withdrawal" -> mapToWithdrawalDto((Withdrawal) transaction);
            default ->
                    throw new NotImplementedException("Mapping for transaction of type " + transaction.getClass() + " is not implemented!");
        };
    }

    ImmediateDto mapToImmediateDto (Immediate immediate);

    DepositDto mapToDepositDto (Deposit deposit);

    WithdrawalDto mapToWithdrawalDto (Withdrawal withdrawal);

    default Page<TransactionDto> mapToTransactionDtoPage(Page<Transaction> transactionPage) {
        return transactionPage.map(this::mapToTransactionDto);
    }
    Deposit mapToDeposit(RequestDepositDto depositDto);

    Withdrawal mapToWithdrawal(RequestWithdrawalDto withdrawalDto);

    Immediate mapToImmediate(RequestImmediateDto immediateDto);

    @Mapping(target = "fromIban", source = "senderIban")
    @Mapping(target = "toIban", source = "receiverIban")
    AccountServiceImmediateRequestDto mapToAccountServiceImmediateRequestDto(RequestImmediateDto immediateDto);

    @Mapping(target = "iban", source = "toIban")
    AccountServiceRequestDto mapToAccountServiceRequestDto(RequestDepositDto depositDto);

    @Mapping(target = "iban", source = "fromIban")
    AccountServiceRequestDto mapToAccountServiceRequestDto(RequestWithdrawalDto withdrawalDto);

}