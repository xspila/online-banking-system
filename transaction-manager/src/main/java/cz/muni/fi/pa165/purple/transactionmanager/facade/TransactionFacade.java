package cz.muni.fi.pa165.purple.transactionmanager.facade;

import cz.muni.fi.pa165.purple.transactionmanager.api.TransactionDto;
import cz.muni.fi.pa165.purple.transactionmanager.api.request.RequestDepositDto;
import cz.muni.fi.pa165.purple.transactionmanager.api.request.RequestImmediateDto;
import cz.muni.fi.pa165.purple.transactionmanager.api.request.RequestWithdrawalDto;
import cz.muni.fi.pa165.purple.transactionmanager.data.enums.TransactionType;
import cz.muni.fi.pa165.purple.transactionmanager.data.model.Deposit;
import cz.muni.fi.pa165.purple.transactionmanager.data.model.Immediate;
import cz.muni.fi.pa165.purple.transactionmanager.data.model.Transaction;
import cz.muni.fi.pa165.purple.transactionmanager.data.model.Withdrawal;
import cz.muni.fi.pa165.purple.transactionmanager.exceptions.TransactionFailedException;
import cz.muni.fi.pa165.purple.transactionmanager.facade.webclient.AccountManagerClient;
import cz.muni.fi.pa165.purple.transactionmanager.mappers.TransactionMapper;
import cz.muni.fi.pa165.purple.transactionmanager.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

@Service
@Transactional
public class TransactionFacade {
    private final TransactionService transactionService;
    private final TransactionMapper transactionMapper;
    private final AccountManagerClient accountManagerClient;

    @Autowired
    public TransactionFacade(TransactionService transactionService,
                             TransactionMapper transactionMapper,
                             AccountManagerClient accountManagerClient) {
        this.transactionService = transactionService;
        this.transactionMapper = transactionMapper;
        this.accountManagerClient = accountManagerClient;
    }

    public Page<TransactionDto> getAllTransactions(Pageable pageable, Specification<Transaction> specification) {
        Page<Transaction> transactions = transactionService.getAllTransactions(pageable, specification);
        return transactionMapper.mapToTransactionDtoPage(transactions);
    }

    public TransactionDto getTransactionById(Long id){
        Transaction transaction = transactionService.getTransactionById(id);
        return transactionMapper.mapToTransactionDto(transaction);
    }

    public void deleteTransactionById(Long id){
        transactionService.deleteTransactionById(id);
    }

    public Page<TransactionDto> getTransactionByIban(Pageable pageable, String iban){
        Page<Transaction> transactions = transactionService.getAllTransactionsWithIban(pageable, iban);
        return transactionMapper.mapToTransactionDtoPage(transactions);
    }

    public TransactionDto createWithdrawalTransaction(RequestWithdrawalDto withdrawalDto) {
        Mono<ResponseEntity<Void>> monoResponse = accountManagerClient.makeWithdrawalRequest(withdrawalDto);
        monoResponse.subscribe(
                responseEntity -> {
                    if (!responseEntity.getStatusCode().is2xxSuccessful()){
                        throw new TransactionFailedException("Transaction not accepted: " + responseEntity.getStatusCode());
                    }
                }
        );

        Withdrawal withdrawal = transactionMapper.mapToWithdrawal(withdrawalDto);
        withdrawal.setType(TransactionType.WITHDRAWAL);
        return transactionMapper.mapToTransactionDto((transactionService.makeWithdrawal(withdrawal)));
    }

    public TransactionDto createImmediateTransaction(RequestImmediateDto immediateDto) {
        Mono<ResponseEntity<Void>> monoResponse = accountManagerClient.makeImmediateRequest(immediateDto);
        monoResponse.subscribe(
                responseEntity -> {
                    if (!responseEntity.getStatusCode().is2xxSuccessful()){
                        throw new TransactionFailedException("Transaction not accepted: " + responseEntity.getStatusCode());
                    }
                }
        );

        Immediate immediate = transactionMapper.mapToImmediate(immediateDto);
        immediate.setType(TransactionType.IMMEDIATE);
        return transactionMapper.mapToTransactionDto((transactionService.makeImmediateTransaction(immediate)));
    }

    public TransactionDto createDepositTransaction(RequestDepositDto depositDto) {
        Mono<ResponseEntity<Void>> monoResponse = accountManagerClient.makeDepositRequest(depositDto);
        monoResponse.subscribe(
                responseEntity -> {
                    if (!responseEntity.getStatusCode().is2xxSuccessful()){
                        throw new TransactionFailedException("Transaction not accepted: " + responseEntity.getStatusCode());
                    }
                }
        );

        Deposit deposit = transactionMapper.mapToDeposit(depositDto);
        deposit.setType(TransactionType.DEPOSIT);
        return transactionMapper.mapToTransactionDto((transactionService.makeDeposit(deposit)));
    }
}