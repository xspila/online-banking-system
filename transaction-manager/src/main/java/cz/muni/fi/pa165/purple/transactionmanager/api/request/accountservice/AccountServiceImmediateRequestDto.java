package cz.muni.fi.pa165.purple.transactionmanager.api.request.accountservice;

import cz.muni.fi.pa165.purple.transactionmanager.data.enums.Currency;
import lombok.Data;

@Data
public class AccountServiceImmediateRequestDto {
    private String fromIban;
    private String toIban;
    private Currency currency;
    private double amount;
}
