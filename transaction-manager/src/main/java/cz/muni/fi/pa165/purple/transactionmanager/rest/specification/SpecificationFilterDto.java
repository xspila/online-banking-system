package cz.muni.fi.pa165.purple.transactionmanager.rest.specification;

import cz.muni.fi.pa165.purple.transactionmanager.data.enums.TransactionType;
import cz.muni.fi.pa165.purple.transactionmanager.data.enums.Currency;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SpecificationFilterDto {
    private String from;
    private String to;
    private TransactionType type;
    private Currency currency;
    private Double max;
    private Double min;
    private LocalDateTime since;
    private LocalDateTime until;
}
