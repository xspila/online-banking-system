package cz.muni.fi.pa165.purple.transactionmanager.api.request;

import cz.muni.fi.pa165.purple.transactionmanager.data.enums.Currency;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
public class RequestImmediateDto implements Serializable {
    @NotEmpty(message = "Iban can not be empty.")
    private String senderIban;

    @NotEmpty(message = "Iban can not be empty.")
    private String receiverIban;

    @Positive(message = "Amount has to be positive number.")
    private double amount;

    @NotNull
    private Currency currency;
}
