package cz.muni.fi.pa165.purple.transactionmanager.exceptions;

import org.springframework.http.HttpStatus;

public class TransactionFailedException extends AbstractApiException{
    public TransactionFailedException(String message) {super(message, HttpStatus.BAD_REQUEST);}
}
