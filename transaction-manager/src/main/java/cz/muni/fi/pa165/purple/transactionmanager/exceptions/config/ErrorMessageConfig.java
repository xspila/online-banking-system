package cz.muni.fi.pa165.purple.transactionmanager.exceptions.config;

import java.nio.charset.StandardCharsets;
import java.util.Locale;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

@Configuration
public class ErrorMessageConfig {

    @Bean
    public MessageSource errorMessageSource() {
        ResourceBundleMessageSource outcome = new ResourceBundleMessageSource();
        outcome.setBasenames("errorMessages");
        outcome.setDefaultLocale(Locale.ENGLISH);
        outcome.setDefaultEncoding(StandardCharsets.UTF_8.name());
        return outcome;
    }

}