package cz.muni.fi.pa165.purple.transactionmanager.data.repository;

import cz.muni.fi.pa165.purple.transactionmanager.data.model.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction,Long>, JpaSpecificationExecutor<Transaction> {

    @Query("SELECT t " +
            "FROM Transaction t " +
            "LEFT JOIN Immediate i ON t.id = i.id " +
            "LEFT JOIN Deposit d ON t.id = d.id " +
            "LEFT JOIN Withdrawal w ON t.id = w.id " +
            "WHERE i.senderIban = :iban OR d.toIban = :iban OR i.receiverIban = :iban OR w.fromIban = :iban")
    Page<Transaction> findByIban(Pageable pageable, String iban);
}
