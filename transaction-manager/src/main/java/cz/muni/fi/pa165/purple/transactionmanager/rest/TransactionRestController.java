package cz.muni.fi.pa165.purple.transactionmanager.rest;


import cz.muni.fi.pa165.purple.transactionmanager.api.TransactionDto;
import cz.muni.fi.pa165.purple.transactionmanager.api.request.RequestDepositDto;
import cz.muni.fi.pa165.purple.transactionmanager.api.request.RequestImmediateDto;
import cz.muni.fi.pa165.purple.transactionmanager.api.request.RequestWithdrawalDto;
import cz.muni.fi.pa165.purple.transactionmanager.data.model.Transaction;
import cz.muni.fi.pa165.purple.transactionmanager.facade.TransactionFacade;
import cz.muni.fi.pa165.purple.transactionmanager.rest.specification.SpecificationFilterDto;
import cz.muni.fi.pa165.purple.transactionmanager.rest.specification.TransactionSpecification;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@OpenAPIDefinition(
        info = @Info(title = "Banking system transaction service",
                version = "1.0",
                description = """
                        Simple service for transaction management The API has operations for:
                        - getting all transactions
                        - creating new transactions
                        - getting incoming and outgoing transaction to an account
                        - getting a specific specific transaction by its id
                        """
        ),
        servers = @Server(description = "resource server", url = "http://localhost:8080")

)
@Tag(name = "Transactions", description = "microservice for transactions")
@RequestMapping(path = "/transactions", produces = MediaType.APPLICATION_JSON_VALUE)
public class TransactionRestController {
    private final TransactionFacade transactionFacade;
    @Autowired
    public TransactionRestController(TransactionFacade transactionFacade) {
        this.transactionFacade = transactionFacade;
    }
    @Operation(
            summary = "Get all filtered transactions",
            description = "Returns an array of objects representing transactions filtered by request body")
    @GetMapping
    public List<TransactionDto> getAllTransactions(@ParameterObject Pageable pageable,
                                                   SpecificationFilterDto filter
    ) {

        Specification<Transaction> specification = Specification.where(null);
        if (filter != null){
            specification = TransactionSpecification.specificationFromFilter(filter);
        }

        return transactionFacade.getAllTransactions(pageable, specification).getContent();
    }

    @Operation(
            summary = "Returns identified transaction",
            description = "Looks up a transaction by its id.",
            responses = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "404", description = "transaction not found")
            }
    )
    @GetMapping(path = "/{id}")
    public TransactionDto getTransaction(@PathVariable Long id) {
        TransactionDto transaction = transactionFacade.getTransactionById(id);
        if (transaction != null) {
            return transaction;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "message with id=" + id + " not found");
        }
    }

    @Operation(
            summary = "Deletes identified transaction",
            description = "Deletes a transaction by its id. Only employees can delete.",
            responses = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "404", description = "transaction not found")
            }
    )
    @DeleteMapping(path = "/{id}")
    public void deleteTransaction(@PathVariable Long id) {
        transactionFacade.deleteTransactionById(id);
    }

    @Operation(
            summary = "Returns all transactions to iban",
            description = "Looks up all transactions incoming or outgoing from iban",
            responses = {
                    @ApiResponse(responseCode = "200"),
            }
    )
    @GetMapping(path = "/iban")
    public Page<TransactionDto> getTransactionByIban(@ParameterObject Pageable pageable,
                                                     @RequestParam String iban) {
        return transactionFacade.getTransactionByIban(pageable, iban);
    }


    @Operation(
            summary = "Creates new deposit",
            description = "Creates new transaction and sends request to change account balance")
    @PostMapping("/deposit")
    public ResponseEntity<TransactionDto> createDepositTransaction(@Valid @RequestBody RequestDepositDto requestDto) {

        TransactionDto transactionDto = transactionFacade.createDepositTransaction(requestDto);

        return ResponseEntity.status(HttpStatus.CREATED).body(transactionDto);
    }

    @Operation(
            summary = "Creates new withdrawal",
            description = "Creates new transaction and sends request to change account balance")
    @PostMapping("/withdrawal")
    public ResponseEntity<TransactionDto> createWithdrawalTransaction(@Valid @RequestBody RequestWithdrawalDto requestDto) {

        TransactionDto transactionDto = transactionFacade.createWithdrawalTransaction(requestDto);

        return ResponseEntity.status(HttpStatus.CREATED).body(transactionDto);
    }

    @Operation(
            summary = "Creates new immediate transaction",
            description = "Creates new transaction and sends request to change account balance")
    @PostMapping("/immediate")
    public ResponseEntity<TransactionDto> createImmediateTransaction(@Valid @RequestBody RequestImmediateDto requestDto) {

        TransactionDto transactionDto = transactionFacade.createImmediateTransaction(requestDto);

        return ResponseEntity.status(HttpStatus.CREATED).body(transactionDto);
    }
}
