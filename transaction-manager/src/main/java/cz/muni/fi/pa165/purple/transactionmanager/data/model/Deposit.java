package cz.muni.fi.pa165.purple.transactionmanager.data.model;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "deposits")
public class Deposit extends Transaction{
    private String toIban;
}
