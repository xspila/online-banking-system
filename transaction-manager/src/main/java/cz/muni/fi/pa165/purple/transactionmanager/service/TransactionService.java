package cz.muni.fi.pa165.purple.transactionmanager.service;

import cz.muni.fi.pa165.purple.transactionmanager.data.model.Deposit;
import cz.muni.fi.pa165.purple.transactionmanager.data.model.Immediate;
import cz.muni.fi.pa165.purple.transactionmanager.data.model.Transaction;
import cz.muni.fi.pa165.purple.transactionmanager.data.model.Withdrawal;
import cz.muni.fi.pa165.purple.transactionmanager.data.repository.TransactionRepository;
import cz.muni.fi.pa165.purple.transactionmanager.exceptions.TransactionNotFoundException;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TransactionService {
    private final TransactionRepository transactionRepository;

    @Autowired
    public TransactionService(TransactionRepository transactionRepository){
        this.transactionRepository = transactionRepository;
    }

    @Transactional(readOnly = true)
    public Page<Transaction> getAllTransactions(Pageable pageable, Specification<Transaction> specification) {
        return transactionRepository.findAll(specification, pageable);
    }

    @Transactional(readOnly = true)
    public Transaction getTransactionById(Long id) {
        Optional<Transaction> transaction = transactionRepository.findById(id);
        if (transaction.isEmpty()){
            throw new TransactionNotFoundException("Transaction not found");
        }
        return transaction.get();
    }

    @Transactional
    public void deleteTransactionById(Long id) {
        transactionRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public Page<Transaction> getAllTransactionsWithIban(Pageable pageable, String iban){
        return transactionRepository.findByIban(pageable, iban);
    }

    @Transactional
    public Transaction makeWithdrawal(Withdrawal withdrawal) {
        return transactionRepository.save(withdrawal);
    }

    @Transactional
    public Transaction makeImmediateTransaction(Immediate immediate) {
        return transactionRepository.save(immediate);
    }

    @Transactional
    public Transaction makeDeposit(Deposit deposit) {
        return transactionRepository.save(deposit);
    }
}
