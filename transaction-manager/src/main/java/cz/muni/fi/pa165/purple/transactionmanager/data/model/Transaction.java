package cz.muni.fi.pa165.purple.transactionmanager.data.model;

import cz.muni.fi.pa165.purple.transactionmanager.data.enums.TransactionType;
import cz.muni.fi.pa165.purple.transactionmanager.data.enums.Currency;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.Table;
import lombok.Data;

import java.time.LocalDateTime;
@Data
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "transactions")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private double amount;

    @Enumerated(EnumType.STRING)
    private TransactionType type;

    @Enumerated(EnumType.STRING)
    private Currency currency;

    private LocalDateTime timestamp;
    public Transaction() {
        this.timestamp = LocalDateTime.now();
    }
}