package cz.muni.fi.pa165.purple.transactionmanager.exceptions;

import org.springframework.http.HttpStatus;

public class WebSocketMessageFailedException extends AbstractApiException{
    public WebSocketMessageFailedException(String message) {super(message, HttpStatus.FAILED_DEPENDENCY);}

}
