package cz.muni.fi.pa165.purple.transactionmanager.data.enums;

public enum Currency {
    CZK,
    EUR,
    USD
}
