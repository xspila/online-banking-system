package cz.muni.fi.pa165.purple.transactionmanager.facade.webclient;

import cz.muni.fi.pa165.purple.transactionmanager.api.request.RequestDepositDto;
import cz.muni.fi.pa165.purple.transactionmanager.api.request.RequestImmediateDto;
import cz.muni.fi.pa165.purple.transactionmanager.api.request.RequestWithdrawalDto;
import cz.muni.fi.pa165.purple.transactionmanager.mappers.TransactionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import static cz.muni.fi.pa165.purple.transactionmanager.security.SecurityUtils.getJwtTokenFromSecurityContext;

@Service
public class AccountManagerClient {
    private final WebClient webClient;
    private final TransactionMapper transactionMapper;

    @Autowired
    public AccountManagerClient(WebClient.Builder webClientBuilder, @Value("${accountManager-url}") String baseUrl,
                                TransactionMapper transactionMapper){
        this.transactionMapper = transactionMapper;
        SecurityContextHolder.getContext().getAuthentication();
        this.webClient = webClientBuilder
                .baseUrl(baseUrl)
                .build();
    }
    public Mono<ResponseEntity<Void>> makeImmediateRequest(RequestImmediateDto data) {
        String token = getJwtTokenFromSecurityContext();
        var request = webClient
                .patch()
                .uri("/accounts/transfer")
                .bodyValue(transactionMapper.mapToAccountServiceImmediateRequestDto(data));

        if (token != null) {
            request.header("Authorization", "Bearer " + token);
        }
        return request.retrieve()
                .onStatus(status -> status.value() == 403,
                        clientResponse -> Mono.error(new AccessDeniedException("Forbidden")))
                .toBodilessEntity();
    }

    public Mono<ResponseEntity<Void>> makeWithdrawalRequest(RequestWithdrawalDto data) {
        String token = getJwtTokenFromSecurityContext();

        var request = webClient.patch()
                .uri("/accounts/expense")
                .bodyValue(transactionMapper.mapToAccountServiceRequestDto(data));
        if (token != null) {
            request.header("Authorization", "Bearer " + token);
        }
        return request.retrieve()
                .onStatus(status -> status.value() == 403,
                        clientResponse -> Mono.error(new AccessDeniedException("Forbidden")))
                .toBodilessEntity();

    }

    public Mono<ResponseEntity<Void>> makeDepositRequest(RequestDepositDto data) {
        String token = getJwtTokenFromSecurityContext();

        var request = webClient.patch()
                .uri("/accounts/income")
                .bodyValue(transactionMapper.mapToAccountServiceRequestDto(data));
        if (token != null) {
            request.header("Authorization", "Bearer " + token);
        }
        return request.retrieve()
                .onStatus(status -> status.value() == 403,
                        clientResponse -> Mono.error(new AccessDeniedException("Forbidden")))
                .toBodilessEntity();

    }

}
