package cz.muni.fi.pa165.purple.transactionmanager.api;

import cz.muni.fi.pa165.purple.transactionmanager.data.enums.Currency;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Data;

@Data
public class TransactionDto implements Serializable {
    private String id;
    private double amount;
    private String type;
    private LocalDateTime timestamp;
    private Currency currency;
}
