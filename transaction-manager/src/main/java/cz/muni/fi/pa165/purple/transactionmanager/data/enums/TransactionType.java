package cz.muni.fi.pa165.purple.transactionmanager.data.enums;

public enum TransactionType {
    WITHDRAWAL,
    DEPOSIT,
    IMMEDIATE
}
