package cz.muni.fi.pa165.purple.transactionmanager.data.model;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
@Table(name = "immediates")
public class Immediate extends Transaction{
    private String senderIban;
    private String receiverIban;
}
