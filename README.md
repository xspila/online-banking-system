## Project assignment:

Online Banking System
The Bank Management System is an application that offers a solution for managing bank accounts, transactions, and
customer information. The system allows customers to access their accounts online, check their account balances,
transfer, withdraw and deposit funds, view transaction history and manage their account information. Users can also
setup scheduled periodical payments. Each user account will have an associated currency. If there is an incoming
transaction in a different currency, the system will automatically exchange the currencies. The system also provides a
dashboard for bank employees to manage customer accounts and monitor all customers bank transactions. The system also
provides a statistical module for employees, which can report total and average (per account) transactions (deposits,
withdrawals, outgoing and incoming payments) in a selected date range.

#### Usecase diagram
![Use case diagram](./diagrams/usecase_diagram.svg)
#### Class diagram
![Use case diagram](./diagrams/class_diagram-0.png)

### How to run/demo

To run the application, you need to have Docker running on your machine. Then you can run the following commands in the
root directory of the project:

```shell
mvn clean package
docker compose up --build
```

Ports:

- `8080` - Transaction manager
- `8081` - Account manager
- `8082` - Reporting manager
- `8083` - User manager
- `3000` - Grafana (username: admin, password: admin)
- `9090` - Prometheus

After every service is up and running, you can run the demo python script
to showcase the functionality of the application:
Note: path to output is arbitrary absolute path where you would like
the output to be stored on the host machine filesystem.
```shell
docker build -t bank_demo ./demo
docker run -p 4000:80 -v {path/to/output}:/app/extra bank_demo
```

### Security
Security is handled through JWT tokens. The user service is responsible for issuing tokens and verifying them.
The only way currently to receive a valid token is to login through muni oauth2. But as there is no
UI the token has to be obtained for example through postman. After receiving the access token and
id_token, send GET to {user-manager-host}/users/login, with muni access token in Authorization header as Bearer token
and the id_token in a query parameter:
`GET {user-manager-host}/users/login?id_token={id_token}`.

If successful, a jwt token tied to your username, which will be in format `UČO@muni.cz,
is issued, which grants access to all customer related operations in the system.

### Microservices

The system is divided into 4 microservices, each responsible for a specific functionality. The microservices will be
communicating with each other using REST APIs.


### 1. Reporting Manager service
Author: Andrej Špila xspila@557318

Responsible for generating reports for bank employees. The service will provide a REST API to generate
reports about the movements in the bank accounts. Spring openAPI generator is used to generate the endpoints.
The reports can be filtered by date range, transaction type, generated for specific accounts or
only for specified currency. An example of a response for `POST /reporting/total`:

```json
{
    "totalAll": 4500,
    "totalDeposits": 1000,
    "totalWithdrawals": 500,
    "totalIncomingPayments": 2000,
    "totalOutgoingPayments": 1000
}
```
For more information in the types and the structure of responses, see the generated API documentation on
`/swagger-ui/index.html` after starting the service.

All endpoints listed:
- `POST /reporting/total` - generates a report with total transactions
- `POST /reporting/average` - generates a report with average transactions
- `POST /reporting/cash` - generates a report about accounts debit and credit in selected currency

Transaction types:
- `DEPOSIT`
- `WITHDRAWAL`
- `INCOMING_PAYMENT`
- `OUTGOING_PAYMENT`

![Reporting dto diagram](./diagrams/reporting_manager_dto_diagram.svg)

### 2. User service
Author: Kryštof Suchánek xsuchan7@514661

##### API documentation

The microsrvice API is available in [account-manager/api-docs.json](api-docs.json) file or when running the app
at http://localhost:8080/v3/api-docs as the safe file or at http://localhost:8080/swagger-ui/index.html#/ as Swagger UI.

##### Class diagram
!["Class diagram"](diagrams/userManager-class-diagram.png)

[Text version](diagrams/userManager-class-diagram.txt)

### 3. TransactionManager
Author: Jan Poláček xpolace5@536681

This microservice is taking care of any transactions between bank accounts managed by AccountManager.
There are currently three types of transactions: Immediate transaction (transaction between two bank accounts),
Deposits (transfers money to a single account) and Withdrawal (remove money from a single account). Each transaction will be stored
in database with unique id. This will allow ReportingManager microservice to load previous transactions

To run this microservice in docker: run `mvn clean package` then `docker-compose build` and finally `docker-compose up`

Endpoints:
- `GET /transaction`
- `GET /transaction/{id}`
- `GET /transaction/iban`
- `POST /transaction/deposit`
- `POST /transaction/immediate`
- `POST /transaction/withdrawal`

##### Class diagram
!["Class diagram"](diagrams/transaction_manager_dto_diagram.png)

### 4. AccountManager
Author: Radim Rychlík xrychli1@514233

### How to run
Prerequisites:
- Docker Desktop 3.9 installed and running

Execute: `docker compose up --build`

The AccountManager microservice handles all bank accounts and operations related to them.
It manages accounts in different types (Main, Minor and Saving) and also in different currencies (CZK, EUR and USD).
The service will provide a REST API for account creation and deletion and, primarily, balance 
incrementing and decrementing with conversion to the account currency.

Endpoints:
- `GET /accounts`
- `POST /accounts`
- `GET /accounts/{iban}`
- `DELETE /accounts/{iban}`
- `PATCH /accounts/income`
- `PATCH /accounts/expense`
- `PATCH /accounts/transfer`

Account types:
- `MAIN`
- `MINOR`
- `SAVING`

Currency:
- `CZK`
- `EUR`
- `USD`

##### DTOs class diagram
!["Class diagram"](diagrams/account_manager_dto_class_diagram.svg)

