package cz.muni.fi.pa165.purple.accountmanager.data.repository;

import cz.muni.fi.pa165.purple.accountmanager.data.enums.AccountType;
import cz.muni.fi.pa165.purple.accountmanager.data.enums.Currency;
import cz.muni.fi.pa165.purple.accountmanager.data.model.Account;
import cz.muni.fi.pa165.purple.accountmanager.data.model.Iban;
import cz.muni.fi.pa165.purple.accountmanager.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class DataLoader implements CommandLineRunner {

    private final AccountService accountService;

    @Override
    public void run(String... args) {
        if (accountService.findAll().isEmpty()) {
            Account account1 = new Account();
            account1.setName("Main account");
            account1.setIban(new Iban("CZ331234567890000000007"));
            account1.setBalance(1000d);
            account1.setAccountType(AccountType.MAIN);
            account1.setCurrency(Currency.CZK);
            account1.setOwner("test1@muni.cz");

            Account account2 = new Account();
            account2.setName("Second account");
            account2.setIban(new Iban("CZ331234567890000000008"));
            account2.setBalance(1000d);
            account2.setAccountType(AccountType.MINOR);
            account2.setCurrency(Currency.CZK);
            account2.setOwner("test1@muni.cz");

            Account account3 = new Account();
            account3.setName("Main account");
            account3.setIban(new Iban("CZ331234567890000000008"));
            account3.setBalance(1000d);
            account3.setAccountType(AccountType.MAIN);
            account3.setCurrency(Currency.CZK);
            account3.setOwner("test2@muni.cz");

            Account account4 = new Account();
            account4.setName("Saving account");
            account4.setIban(new Iban("CZ331234567890000000010"));
            account4.setBalance(1000d);
            account4.setAccountType(AccountType.SAVING);
            account4.setCurrency(Currency.CZK);
            account4.setOwner("test3@muni.cz");
            List<Account> accounts = List.of(account1, account2, account3, account4);
            accountService.saveAll(accounts);
        }
    }
}
