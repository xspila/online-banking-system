package cz.muni.fi.pa165.purple.accountmanager.service;

import cz.muni.fi.pa165.purple.accountmanager.data.model.Account;
import cz.muni.fi.pa165.purple.accountmanager.data.model.Iban;
import cz.muni.fi.pa165.purple.accountmanager.data.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static cz.muni.fi.pa165.purple.accountmanager.security.SecurityUtils.getJwtTokenFromSecurityContext;

@Service
@RequiredArgsConstructor
public class AccountService {
    private final AccountRepository accountRepository;
    private final JwtDecoder jwtDecoder;

    @Transactional(readOnly = true)
    public Optional<Account> findByIban(Iban iban) {
        return accountRepository.findByIban(iban);
    }

    @Transactional(readOnly = true)
    public List<Account> findAll() {
        return accountRepository.findAll();
    }

    @Transactional()
    public void create(Account account) {
        accountRepository.save(account);
    }

    @Transactional()
    public void addAll(List<Account> accounts) {
        accountRepository.saveAll(accounts);
    }

    @Transactional()
    public boolean deleteByIban(Iban iban) {
        return accountRepository.deleteByIban(iban) != 0;
    }

    @Transactional()
    public void deleteAll() {
        accountRepository.deleteAll();
    }

    @Transactional
    public void saveAll(List<Account> accounts) {
        accountRepository.saveAll(accounts);
    }

    @Transactional()
    public boolean addIncome(Iban iban, Double value) {
        Optional<Account> account = findByIban(iban);
        if (account.isPresent()) {
            account.get().addBalance(value);
            return true;
        }
        return false;
    }

    @Transactional()
    public boolean subtractExpense(Iban iban, Double value) {
        Optional<Account> account = findByIban(iban);
        if (account.isPresent()) {
            if (account.get().getBalance() < value) {
                throw new IllegalArgumentException("Not enough balance. Current balance: " + account.get().getBalance());
            }
            account.get().removeBalance(value);
            return true;
        }
        return false;
    }

    @Transactional()
    public boolean makeTransfer(Iban from, Iban to, Double value) {
        return addIncome(to, value) && subtractExpense(from, value);
    }

    public void checkJwtSubjectAccountOwnerOrEmployee(Iban iban) {
        String jwtToken = getJwtTokenFromSecurityContext();
        if (jwtToken == null) {
            throw new AccessDeniedException("No JWT token found");
        }
        Jwt decodedToken = jwtDecoder.decode(jwtToken);
        String[] authority = decodedToken.getClaim("scope").toString().split(" ");
        if (Arrays.asList(authority).contains("EMPLOYEE")) {
            return;
        }
        String owner = decodedToken.getSubject();
        Optional<Account> account = accountRepository.findByIban(iban);
        if (account.isEmpty()) return;
        if (!account.get().getOwner().equals(owner)) {
            throw new AccessDeniedException("User is not the account owner");
        }
    }
}
