package cz.muni.fi.pa165.purple.accountmanager.api;

import cz.muni.fi.pa165.purple.accountmanager.data.enums.Currency;
import cz.muni.fi.pa165.purple.accountmanager.data.model.Iban;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Data Transfer Object for money transfer information")
public class BaseTransferDto {
    @Valid
    @NotNull(message = "IBAN cannot be null")
    @Schema(description = "The iban of the account", example = "CZ4212345678900000")
    private Iban iban;

    @NotNull(message = "Currency cannot be null")
    @Schema(description = "The currency of the account", example = "CZK")
    private Currency currency;

    @PositiveOrZero(message = "Amount cannot be negative")
    @Schema(description = "The amount of the transfer", example = "420.42")
    private double amount;
}
