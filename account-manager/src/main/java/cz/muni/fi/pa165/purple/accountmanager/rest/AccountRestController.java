package cz.muni.fi.pa165.purple.accountmanager.rest;

import cz.muni.fi.pa165.purple.accountmanager.api.AccountDetailedDto;
import cz.muni.fi.pa165.purple.accountmanager.api.BaseTransferDto;
import cz.muni.fi.pa165.purple.accountmanager.api.TransferDto;
import cz.muni.fi.pa165.purple.accountmanager.facade.AccountFacade;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@OpenAPIDefinition(
        info = @Info(title = "AccountManager API",
                description = """
                        API for managing accounts in the online banking system.
                                        
                        API defines following operations:
                        - Get all accounts
                        - Get account by iban
                        - Delete account
                        - Create account
                        - Add income to account
                        - Remove expense from account
                        - Make transfer between two accounts
                        """,
                version = "1.0.0"),
        servers = @Server(description = "accounts server", url = "{scheme}://{server}:{port}", variables = {
                @ServerVariable(name = "scheme", allowableValues = {"http", "https"}, defaultValue = "http"),
                @ServerVariable(name = "server", defaultValue = "localhost"),
                @ServerVariable(name = "port", defaultValue = "8081"),
        }))
@Tag(name = "Account", description = "The Account service API")
@RequestMapping(path = "/accounts")
public class AccountRestController {
    private final AccountFacade accountFacade;

    @Autowired
    public AccountRestController(AccountFacade accountFacade) {
        this.accountFacade = accountFacade;
    }

    @Operation(
            summary = "Get a account with IBAN",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Found the account",
                            content = @Content(schema = @Schema(implementation = AccountDetailedDto.class))),
                    @ApiResponse(responseCode = "404", description = "Account not found")
            })
    @GetMapping(path = "/{iban}")
    public ResponseEntity<AccountDetailedDto> findByIban(@PathVariable("iban") String iban) {
        return ResponseEntity.ok(accountFacade.findByIban(iban));
    }

    @Operation(
            summary = "Get all accounts",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Successfully retrieved list",
                            content = @Content(schema = @Schema(implementation = AccountDetailedDto.class))),
            })
    @GetMapping
    public ResponseEntity<List<AccountDetailedDto>> findAll() {
        return ResponseEntity.ok(accountFacade.findAll());
    }

    @Operation(
            summary = "Delete a account with IBAN",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Account deleted"),
                    @ApiResponse(responseCode = "404", description = "Account not found")
            })
    @DeleteMapping(path = "/{iban}")
    public ResponseEntity<Void> deleteAccount(@PathVariable("iban") String iban) {
        accountFacade.deleteByIban(iban);
        return ResponseEntity.ok().build();
    }

    @Operation(
            summary = "Create a new account",
            responses = {
                    @ApiResponse(responseCode = "201", description = "Account created"),
            })
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public ResponseEntity<Void> createAccount(@Valid @RequestBody AccountDetailedDto account) {
        accountFacade.createAccount(account);
        return ResponseEntity.ok().build();
    }

    @Operation(
            summary = "Add amount to account with IBAN",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Amount added"),
                    @ApiResponse(responseCode = "404", description = "Account not found")
            })
    @PatchMapping("/income")
    public ResponseEntity<Void> addIncome(@Valid @RequestBody BaseTransferDto transfer) {
        accountFacade.addIncome(transfer.getIban(), transfer.getAmount());
        return ResponseEntity.ok().build();
    }

    @Operation(
            summary = "Remove amount from account with IBAN",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Amount removed"),
                    @ApiResponse(responseCode = "404", description = "Account not found")
            })
    @PatchMapping("/expense")
    public ResponseEntity<Void> subtractExpense(@Valid @RequestBody BaseTransferDto transfer) {
        accountFacade.subtractExpense(transfer.getIban(), transfer.getAmount());
        return ResponseEntity.ok().build();
    }

    @Operation(
            summary = "Make money transfer between two accounts",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Transfer done"),
                    @ApiResponse(responseCode = "404", description = "Account not found")
            })
    @PatchMapping("/transfer")
    public ResponseEntity<Void> makeTransfer(@Valid @RequestBody TransferDto transfer) {
        accountFacade.makeTransfer(transfer.getFromIban(), transfer.getToIban(), transfer.getAmount());
        return ResponseEntity.ok().build();
    }
}
