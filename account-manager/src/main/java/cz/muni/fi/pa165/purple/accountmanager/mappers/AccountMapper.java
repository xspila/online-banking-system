package cz.muni.fi.pa165.purple.accountmanager.mappers;

import cz.muni.fi.pa165.purple.accountmanager.api.AccountDetailedDto;
import cz.muni.fi.pa165.purple.accountmanager.data.model.Account;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AccountMapper {
    AccountDetailedDto toDetailedDto(Account account);

    Account toAccount(AccountDetailedDto accountDetailedDto);
}
