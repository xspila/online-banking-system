package cz.muni.fi.pa165.purple.accountmanager.data.repository;

import cz.muni.fi.pa165.purple.accountmanager.data.model.Account;
import cz.muni.fi.pa165.purple.accountmanager.data.model.Iban;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
    @Query("select a from Account a where a.iban = ?1")
    Optional<Account> findByIban(Iban iban);

    @Modifying(flushAutomatically = true)
    @Query("delete from Account a where a.iban = ?1")
    int deleteByIban(Iban iban);
}
