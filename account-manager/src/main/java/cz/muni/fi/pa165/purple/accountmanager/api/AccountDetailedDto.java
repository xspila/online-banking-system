package cz.muni.fi.pa165.purple.accountmanager.api;

import cz.muni.fi.pa165.purple.accountmanager.data.enums.AccountType;
import cz.muni.fi.pa165.purple.accountmanager.data.enums.Currency;
import cz.muni.fi.pa165.purple.accountmanager.data.model.Iban;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@Schema(description = "Data Transfer Object for account information without ID")
public class AccountDetailedDto {
    @NotEmpty(message = "Name should not be empty")
    @Schema(description = "The name of the account", example = "My account")
    private String name;

    @Valid
    @NotNull(message = "IBAN cannot be null")
    @Schema(description = "The iban of the account", example = "CZ4212345678900000")
    private Iban iban;

    @NotNull(message = "Balance cannot be null")
    @PositiveOrZero(message = "Balance cannot be negative")
    @Schema(description = "The balance of the account", example = "420.42")
    private Double balance;

    @NotNull(message = "Account type cannot be null")
    @Schema(description = "The type of the account", example = "MAIN")
    private AccountType accountType;

    @NotNull(message = "Currency cannot be null")
    @Schema(description = "The currency of the account", example = "CZK")
    private Currency currency;

    @NotEmpty(message = "Owner should not be empty")
    @Schema(description = "The owner of the account", example = "John Doe")
    private String owner;
}
