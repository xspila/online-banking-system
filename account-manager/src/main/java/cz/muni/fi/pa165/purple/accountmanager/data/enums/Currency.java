package cz.muni.fi.pa165.purple.accountmanager.data.enums;

public enum Currency {
    CZK,
    EUR,
    USD
}
