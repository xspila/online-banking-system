package cz.muni.fi.pa165.purple.accountmanager.security;

import lombok.experimental.UtilityClass;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;

@UtilityClass
public class SecurityUtils {

    public static String getJwtTokenFromSecurityContext() {
        if (SecurityContextHolder.getContext().getAuthentication() != null
                && SecurityContextHolder.getContext().getAuthentication().getCredentials() instanceof Jwt jwt) {
            return jwt.getTokenValue();
        }
        return null;
    }
}
