package cz.muni.fi.pa165.purple.accountmanager.exceptions;

import org.springframework.http.HttpStatus;

public class IbanAlreadyExistsException extends AbstractApiException {
    public IbanAlreadyExistsException(String message) {
        super(message, HttpStatus.BAD_REQUEST);
    }
}
