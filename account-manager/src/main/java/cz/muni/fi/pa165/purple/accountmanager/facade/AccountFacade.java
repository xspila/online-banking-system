package cz.muni.fi.pa165.purple.accountmanager.facade;

import cz.muni.fi.pa165.purple.accountmanager.api.AccountDetailedDto;
import cz.muni.fi.pa165.purple.accountmanager.data.model.Account;
import cz.muni.fi.pa165.purple.accountmanager.data.model.Iban;
import cz.muni.fi.pa165.purple.accountmanager.exceptions.IbanAlreadyExistsException;
import cz.muni.fi.pa165.purple.accountmanager.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.purple.accountmanager.mappers.AccountMapper;
import cz.muni.fi.pa165.purple.accountmanager.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AccountFacade {
    private static final Logger log = LoggerFactory.getLogger(AccountFacade.class);
    private final AccountService accountService;
    private final AccountMapper accountMapper;

    public AccountDetailedDto findByIban(String iban) {
        accountService.checkJwtSubjectAccountOwnerOrEmployee(new Iban(iban));
        Optional<Account> account = accountService.findByIban(new Iban(iban));
        if (account.isEmpty()) {
            throw new ResourceNotFoundException("No existing account with given IBAN: " + iban);
        }
        return accountMapper.toDetailedDto(account.get());
    }

    public void createAccount(AccountDetailedDto accountDto) {
        Optional<Account> account = accountService.findByIban(accountDto.getIban());
        if (account.isPresent()) {
            throw new IbanAlreadyExistsException("IBAN already exists: " + accountDto.getIban().toString());
        }

        accountService.create(accountMapper.toAccount(accountDto));
    }

    public void deleteByIban(String iban) {
        accountService.checkJwtSubjectAccountOwnerOrEmployee(new Iban(iban));
        if (!accountService.deleteByIban(new Iban(iban))) {
            throw new ResourceNotFoundException("No existing account with given IBAN: " + iban);
        }
    }

    public List<AccountDetailedDto> findAll() {
        return accountService.findAll().stream()
                .map(accountMapper::toDetailedDto)
                .toList();
    }

    public void addIncome(Iban iban, Double value) {
        accountService.checkJwtSubjectAccountOwnerOrEmployee(iban);
        if (!accountService.addIncome(iban, value)) {
            log.error("No existing account with given IBAN: {}", iban);
            throw new ResourceNotFoundException("No existing account with given IBAN: " + iban);
        }
    }

    public void subtractExpense(Iban iban, Double value) {
        accountService.checkJwtSubjectAccountOwnerOrEmployee(iban);
        if (!accountService.subtractExpense(iban, value)) {
            throw new ResourceNotFoundException("No existing account with given IBAN: " + iban);
        }
    }

    public void makeTransfer(Iban from, Iban to, Double value) {
        accountService.checkJwtSubjectAccountOwnerOrEmployee(from);
        if (!accountService.makeTransfer(from, to, value)) {
            throw new ResourceNotFoundException("No existing account with given IBAN: " + from);
        }
    }
}
