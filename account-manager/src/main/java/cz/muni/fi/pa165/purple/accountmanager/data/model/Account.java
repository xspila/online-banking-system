package cz.muni.fi.pa165.purple.accountmanager.data.model;

import cz.muni.fi.pa165.purple.accountmanager.data.enums.AccountType;
import cz.muni.fi.pa165.purple.accountmanager.data.enums.Currency;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "accounts")
public class Account implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "Name should not be empty")
    private String name;

    @Valid
    @NotNull(message = "IBAN cannot be null")
    @Embedded
    private Iban iban;

    @NotNull(message = "Balance cannot be null")
    @PositiveOrZero(message = "Balance cannot be negative")
    private Double balance;

    @NotNull(message = "Account type cannot be null")
    @Enumerated(EnumType.STRING)
    private AccountType accountType;

    @NotNull(message = "Currency cannot be null")
    @Enumerated(EnumType.STRING)
    private Currency currency;

    @NotEmpty(message = "Owner should not be empty")
    private String owner;

    public void addBalance(double value) {
        balance += value;
    }

    public void removeBalance(double value) {
        balance -= value;
    }
}
