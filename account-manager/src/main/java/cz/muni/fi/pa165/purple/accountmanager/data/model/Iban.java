package cz.muni.fi.pa165.purple.accountmanager.data.model;

import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class Iban implements Serializable {

    public Iban(String iban) {
        if (iban.length() < 5) {
            throw new IllegalArgumentException("Invalid IBAN length");
        }
        this.countryCode = iban.substring(0, 2);
        this.checkDigits = iban.substring(2, 4);
        this.basicBankAccountNumber = iban.substring(4);
    }

    @NotNull(message = "Country code cannot be null")
    @Size(min = 2, max = 2)
    private String countryCode;

    @NotNull(message = "Check digits cannot be null")
    @Size(min = 2, max = 2)
    private String checkDigits;

    @NotNull(message = "BBAN cannot be null")
    @Size(min = 14, max = 34)
    private String basicBankAccountNumber;

    @Override
    public String toString() {
        return countryCode + checkDigits + basicBankAccountNumber;
    }
}
