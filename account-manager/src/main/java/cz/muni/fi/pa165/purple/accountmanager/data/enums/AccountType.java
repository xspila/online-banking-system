package cz.muni.fi.pa165.purple.accountmanager.data.enums;

public enum AccountType {
    MAIN,
    MINOR,
    SAVING
}
