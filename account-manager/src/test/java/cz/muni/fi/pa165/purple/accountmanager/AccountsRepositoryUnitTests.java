package cz.muni.fi.pa165.purple.accountmanager;

import cz.muni.fi.pa165.purple.accountmanager.data.enums.AccountType;
import cz.muni.fi.pa165.purple.accountmanager.data.enums.Currency;
import cz.muni.fi.pa165.purple.accountmanager.data.model.Account;
import cz.muni.fi.pa165.purple.accountmanager.data.model.Iban;
import cz.muni.fi.pa165.purple.accountmanager.data.repository.AccountRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.oauth2.jwt.JwtDecoder;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class AccountsRepositoryUnitTests {

    // Example data
    private Account account;

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AccountRepository accountRepository;

    @MockBean
    private JwtDecoder jwtDecoder;

    @BeforeEach
    void initData() {
        Iban iban = new Iban("CZ", "42", "12345678900000");
        Account acc = new Account();
        acc.setName("Test Account");
        acc.setIban(iban);
        acc.setBalance(420.42);
        acc.setAccountType(AccountType.MAIN);
        acc.setCurrency(Currency.CZK);
        acc.setOwner("test");
        account = acc;
    }

    @Test
    public void testFindByIban() {
        Iban iban = new Iban("CZ4212345678900000");
        entityManager.persist(account);
        entityManager.flush();

        Optional<Account> found = accountRepository.findByIban(iban);

        assertThat(found).isPresent();
        assertThat(found.get().getIban()).isEqualTo(iban);
    }

    @Test
    public void testDeleteByIban() {
        Iban iban = new Iban("CZ4212345678900000");
        entityManager.persist(account);
        entityManager.flush();

        int deleted = accountRepository.deleteByIban(iban);

        assertThat(deleted).isEqualTo(1);
        Optional<Account> notFound = accountRepository.findByIban(iban);
        assertThat(notFound).isNotPresent();
    }
}
