package cz.muni.fi.pa165.purple.accountmanager;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.purple.accountmanager.api.AccountDetailedDto;
import cz.muni.fi.pa165.purple.accountmanager.api.BaseTransferDto;
import cz.muni.fi.pa165.purple.accountmanager.api.TransferDto;
import cz.muni.fi.pa165.purple.accountmanager.data.enums.AccountType;
import cz.muni.fi.pa165.purple.accountmanager.data.enums.Currency;
import cz.muni.fi.pa165.purple.accountmanager.data.model.Iban;
import cz.muni.fi.pa165.purple.accountmanager.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.purple.accountmanager.facade.AccountFacade;
import cz.muni.fi.pa165.purple.accountmanager.rest.AccountRestController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(AccountRestController.class)
@AutoConfigureMockMvc(addFilters = false)
@WithMockUser(username = "admin", authorities = {"SCOPE_EMPLOYEE", "SCOPE_CUSTOMER", "SCOPE_test_1"})
public class AccountsRestControllerUnitTests {

    // Example data
    private final Iban iban = new Iban("CZ", "42", "12345678900000");
    private final AccountDetailedDto account = new AccountDetailedDto("My account", iban, 420.42, AccountType.MAIN, Currency.CZK, "test");
    private final BaseTransferDto transfer = new BaseTransferDto(iban, Currency.CZK, 420.42);
    private final TransferDto transferDto = new TransferDto(iban, iban, Currency.CZK, 420.42);
    private final AccountDetailedDto accountInvalid = new AccountDetailedDto("My account", null, null, AccountType.MAIN, Currency.CZK, "test");
    private final BaseTransferDto transferInvalid = new BaseTransferDto(null, Currency.CZK, -42);
    private final TransferDto transferDtoInvalid = new TransferDto(null, null, Currency.CZK, -42);

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private AccountFacade accountFacade;

    @Test
    public void testFindByIban() throws Exception {
        Mockito.when(accountFacade.findByIban("CZ4212345678900000")).thenReturn(account);

        mockMvc.perform(MockMvcRequestBuilders.get("/accounts/CZ4212345678900000"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(account.getName())));

        Mockito.verify(accountFacade, times(1)).findByIban("CZ4212345678900000");
    }

    @Test
    public void testFindByIban_NotFound() throws Exception {
        Mockito.when(accountFacade.findByIban("CZ4212345678900000")).thenThrow(new ResourceNotFoundException(""));

        mockMvc.perform(MockMvcRequestBuilders.get("/accounts/CZ4212345678900000"))
                .andExpect(status().isNotFound());

        Mockito.verify(accountFacade, times(1)).findByIban("CZ4212345678900000");
    }

    @Test
    public void testFindAll() throws Exception {
        List<AccountDetailedDto> accounts = List.of(account);
        Mockito.when(accountFacade.findAll()).thenReturn(accounts);

        mockMvc.perform(MockMvcRequestBuilders.get("/accounts"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(account.getName())));
    }

    @Test
    public void testDeleteAccount() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/accounts/CZ4212345678900000"))
                .andExpect(status().isOk());

        Mockito.verify(accountFacade, times(1)).deleteByIban("CZ4212345678900000");
    }

    @Test
    public void testCreateAccount() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/accounts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(account)))
                .andExpect(status().isOk());

        Mockito.verify(accountFacade, times(1)).createAccount(account);
    }

    @Test
    public void testCreateAccount_InvalidInput() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/accounts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(accountInvalid)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testAddIncome() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.patch("/accounts/income")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(transfer)))
                .andExpect(status().isOk());

        Mockito.verify(accountFacade, times(1)).addIncome(iban, 420.42);
    }

    @Test
    public void testAddIncome_InvalidInput() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.patch("/accounts/income")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(transferInvalid)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSubtractExpense() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.patch("/accounts/expense")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(transfer)))
                .andExpect(status().isOk());

        Mockito.verify(accountFacade, times(1)).subtractExpense(iban, 420.42);
    }

    @Test
    public void testSubtractExpense_InvalidInput() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.patch("/accounts/expense")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(transferInvalid)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testMakeTransfer() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.patch("/accounts/transfer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(transferDto)))
                .andExpect(status().isOk());

        Mockito.verify(accountFacade, times(1)).makeTransfer(iban, iban, 420.42);
    }

    @Test
    public void testMakeTransfer_InvalidInput() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.patch("/accounts/transfer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(transferDtoInvalid)))
                .andExpect(status().isBadRequest());
    }
}
