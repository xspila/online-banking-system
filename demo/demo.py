import json
import matplotlib.pyplot as plt
import pandas as pd
import requests
import time
from http import HTTPStatus
from requests.models import Response

ADS = True

LOG_FILE = 'extra/log.txt'

HOST = 'http://host.docker.internal'

TRANSACTION_MANAGER_PORT = 8080
ACCOUNT_MANAGER_PORT = 8081
REPORTING_MANAGER_PORT = 8082
USER_MANAGER_PORT = 8083


def create_url(port: int):
    return f'{HOST}:{port}'


TRANSACTION_MANAGER_URL = create_url(TRANSACTION_MANAGER_PORT)
ACCOUNT_MANAGER_URL = create_url(ACCOUNT_MANAGER_PORT)
REPORTING_MANAGER_URL = create_url(REPORTING_MANAGER_PORT)
USER_MANAGER_URL = create_url(USER_MANAGER_PORT)


def resolve_service_url(service_name: str):
    if service_name == 'transaction':
        return TRANSACTION_MANAGER_URL
    elif service_name == 'account':
        return ACCOUNT_MANAGER_URL
    elif service_name == 'reporting':
        return REPORTING_MANAGER_URL
    elif service_name == 'user':
        return USER_MANAGER_URL
    else:
        raise Exception('Invalid service name')


TRANSACTION_MANAGER = 'transaction'
ACCOUNT_MANAGER = 'account'
REPORTING_MANAGER = 'reporting'
USER_MANAGER = 'user'

EMPLOYEE_TOKEN = """eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJFTVBMT1lFRUBtdW5pLmN6IiwiaXNzIjoiT25saW5lQmFua2luZ1N5c3RlbSIsInNjb3BlIjoiQ1VTVE9NRVIgRU1QTE9ZRUUgdGVzdF8xIHRlc3RfMiIsImlhdCI6MTcxNjIxNTI5NX0.neQDIrJcfGJM9Swx-UrP5wArj60dtTidSxYVh7jPwB5ryIMFeC11XzC7zkwBzwniHCYQhAE6KXYRJRU3U9xOfV3dHD0m5YONFbKSOPxnib64k8PIQ7Jqp8v5dNVJjvpM0gnOMpRs1QIFB-REtG4EsIvpTmGUN2Tgl6eELG03VOzXHmxbgw8WPmeKT0rkzwHCSj8nl48x8i7uqv7i_1BYZr0wlBWwUPQU089wdekZIxv0Ah3LLbJwNQBm1fI4PMoGjQajfbywt0tBfhSrAeHOHSP0YB_t0NL0ePkzWVZR_xqVG9Vu1EVuCM8EOeNe5qfg4Oq6eyP_u-7YANt-ym_eUA"""

customer1_token = """eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJ0ZXN0MUBtdW5pLmN6IiwiaXNzIjoiT25saW5lQmFua2luZ1N5c3RlbSIsInNjb3BlIjoiQ1VTVE9NRVIgdGVzdF8xIiwiaWF0IjoxNzE2MjE1Mjk1fQ.a-JGzS5GWrXgk7yppoIv9Fy-PUCEO-Jd2Nmmh-eCnfmUc9ZL3jPhtv1Q5oJDyG7rdQSc5wXIQMMw4sDnkrMlXSgw2nmcBRGpwN8WfaIHkjSIZwUeXq_x_zov5xxjvMu-YeLCc2AAyD-iSwx2-a_s4O79Lpq2N5JpfEcbQnt4T-zLVPqmOuwhroKfrPdwUbAZ1NhkaJnK0VyEMpt72dkDP2hjB5tCyP5ceAfPXPZBnwlC8mFWH8dQPEnSMoeakZxmk0k2e9GBWf9NS6occQHHYPHJ2hWlt68r-yb69gFyWcyV-5rNx-65TtTB1vHduxctl2fJ0sdN07joFlR8B3tIaQ"""
customer2_token =  """eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJ0ZXN0MkBtdW5pLmN6IiwiaXNzIjoiT25saW5lQmFua2luZ1N5c3RlbSIsInNjb3BlIjoiQ1VTVE9NRVIgdGVzdF8xIiwiaWF0IjoxNzE2MjE1Mjk1fQ.sdcQXAQO1QbE7jGRR0fiUitlMmJ-gbqc-hzp7l64j-a-gMD0gVfYNNRBG76tNEGLndojTDpV5DVXVu2UybCeGuMDpNcMsRw95ShkUvU5X49yXo6fc2YiV8VEW2VrR1j_h1-qEItw3LLsjnEBsA0lFfkMMpDHdfP-LrePKw3XAzb_wJJQLoe_-acfrhkEayWBooLa4zVB3VUGMl7exYeinH0s1vmCOnLMuVwQa0F2_HH879Z9xcNJYZ0V0dvWdlU01yj8mOI4UY1X2Zl93oJGm8LeTCm5w5p3VheGGI2UP-y7MT67IpQc5Pzx4lyLMImVP2gJpp4MHwcZ5_-qogNe7w"""


def is_json(myjson):
    try:
        json.loads(myjson)
    except ValueError:
        return False
    return True


def log(response: Response):
    response_text = response.text
    if is_json(response_text):
        print(json.dumps(json.loads(response_text), indent=4))
    else:
        print(response_text)

    with open(LOG_FILE, 'a') as log_file:
        log_file.write(f'Response from {response.request.url}:\n')
        log_file.write(f'Status: {response.status_code}\n')
        log_file.write(f'Content: {response_text}\n\n')


def print_divider():
    print('----------------------------------------')


def print_status_code(response: Response):
    code = response.status_code
    print(f"{code} - {HTTPStatus(code).phrase}")


def get(service_name: str, enpoint: str, params: dict = None, json: dict = None, token: str = None):
    url = resolve_service_url(service_name)
    headers = {'content-type': 'application/json', 'authorization': f'Bearer {token or EMPLOYEE_TOKEN}'}
    response = requests.get(f'{url}/{enpoint}', headers=headers, params=params, json=json)
    time.sleep(1)
    log(response)
    return response


def post(service_name: str, enpoint: str, params: dict = None, json: dict = None, token: str = None):
    url = resolve_service_url(service_name)
    headers = {'content-type': 'application/json', 'authorization': f'Bearer {token or EMPLOYEE_TOKEN}'}
    response = requests.post(f'{url}/{enpoint}', headers=headers, params=params, json=json)
    time.sleep(1)
    log(response)
    return response


def patch(service_name: str, enpoint: str, params: dict = None, json: dict = None, token: str = None):
    url = resolve_service_url(service_name)
    headers = {'content-type': 'application/json', 'authorization': f'Bearer {token or EMPLOYEE_TOKEN}'}
    response = requests.patch(f'{url}/{enpoint}', headers=headers, params=params, json=json)
    time.sleep(1)
    log(response)
    return response


def delete(service_name: str, enpoint: str, params: dict = None, json: dict = None, token: str = None):
    url = resolve_service_url(service_name)
    headers = {'content-type': 'application/json', 'authorization': f'Bearer {token or EMPLOYEE_TOKEN}'}
    response = requests.delete(f'{url}/{enpoint}', headers=headers, params=params, json=json)
    time.sleep(1)
    log(response)
    return response


print("State of user DB before creating a user")
get(USER_MANAGER, 'users')

employee_data = {
    "username": "EMPLOYEE@muni.cz",
    "password": "secure_password",
    "firstName": "John",
    "lastName": "Doe",
    "nationality": "USA",
    "phoneNumber": "+420123456789",
    "mobilePhoneNumber": "+420701234567",
    "email": "john.doe@example.com",
    "street": "123 Main St",
    "city": "New York",
    "state": "NY",
    "country": "USA",
    "postalCode": "10001",
    "isEmployee": True
}

jack_customer_data = {
    "username": "test1@muni.cz",
    "password": "secure_password1",
    "firstName": "Jack",
    "lastName": "Customer",
    "nationality": "USA",
    "phoneNumber": "+420213456789",
    "mobilePhoneNumber": "+420701234567",
    "email": "jack.customer@example.com",
    "street": "456 Main St",
    "city": "New York",
    "state": "NY",
    "country": "USA",
    "postalCode": "10002",
    "isEmployee": False
}

jane_customer_data = {
    "username": "test2@muni.cz",
    "password": "secure_password2",
    "firstName": "Jane",
    "lastName": "Customer",
    "nationality": "USA",
    "phoneNumber": "+420987654321",
    "mobilePhoneNumber": "+420709876543",
    "email": "jane.customer@example.com",
    "street": "456 Main St",
    "city": "New York",
    "state": "NY",
    "country": "USA",
    "postalCode": "10002",
    "isEmployee": False
}

leaving_customer_data = {
    "username": "leaving_customer",
    "password": "secure_password3",
    "firstName": "Leaving",
    "lastName": "Customer",
    "nationality": "USA",
    "phoneNumber": "+420987654322",
    "mobilePhoneNumber": "+420709876544",
    "email": "leaving.customer@example.com",
    "street": "789 Main St",
    "city": "New York",
    "state": "NY",
    "country": "USA",
    "postalCode": "10003",
    "isEmployee": False
}

print("Creating users")
post(USER_MANAGER, 'users/create', employee_data)
post(USER_MANAGER, 'users/create', jack_customer_data)
post(USER_MANAGER, 'users/create', jane_customer_data)
post(USER_MANAGER, 'users/create', leaving_customer_data)

print_divider()
print("State of user DB after creating a users")
get(USER_MANAGER, 'users/details')

print_divider()
print("Now that we have a couple users let's create some accounts for them")

print("Let's check if we have any accounts in the system")
get(ACCOUNT_MANAGER, 'accounts')

print("As there are none, let's create some accounts for our new customers")

account1_data = {
    "name": "Crypto_withdrawals",
    "iban": "CZ4907100000000000123457",
    "balance": 100,
    "accountType": "MAIN",
    "currency": "CZK",
    "owner": "test1@muni.cz"
}

account2_data = {
    "name": "OF_deposits",
    "iban": "CZ6508000000192000145399",
    "balance": 1_000_000,
    "accountType": "MAIN",
    "currency": "CZK",
    "owner": "test2@muni.cz"
}

post(ACCOUNT_MANAGER, 'accounts', json=account1_data, token=customer1_token)
post(ACCOUNT_MANAGER, 'accounts', json=account2_data, token=customer2_token)

print("Summary of accounts after creation")
get(ACCOUNT_MANAGER, 'accounts')

print_divider()
print("Let's first deposit some money into test1@muni.cz owned account")
print("Before deposit calling get on the account")
get(ACCOUNT_MANAGER, 'accounts/CZ4907100000000000123457', token=customer1_token)
post(TRANSACTION_MANAGER, 'transactions/deposit', json={"toIban": "CZ4907100000000000123457", "amount": 1000, "currency": "CZK"}, token=customer1_token)
print("After deposit calling get on the account")
get(ACCOUNT_MANAGER, 'accounts/CZ4907100000000000123457', token=customer1_token)
print("Now let's withdraw some money from the account")
post(TRANSACTION_MANAGER, 'transactions/withdrawal', json={"fromIban": "CZ4907100000000000123457", "amount": 500, "currency": "CZK"}, token=customer1_token)
print("After withdrawal calling get on the account")
get(ACCOUNT_MANAGER, 'accounts/CZ4907100000000000123457', token=customer1_token)
print("And now let's transfer some money to the other account")
post(TRANSACTION_MANAGER, 'transactions/immediate', json={"senderIban": "CZ4907100000000000123457", "receiverIban": "CZ6508000000192000145399", "amount": 500, "currency": "CZK"}, token=customer1_token)
print("After transfer calling get on the accounts")
get(ACCOUNT_MANAGER, 'accounts')

print("There are also some security measures in place")
print("Let's try to withdraw more money than we have")
response = patch(ACCOUNT_MANAGER, 'accounts/expense', json={"iban": "CZ4907100000000000123457", "amount": 10000, "currency": "CZK"}, token=customer1_token)
print("after trying to withdraw more money than we have")
get(ACCOUNT_MANAGER, 'accounts/CZ4907100000000000123457', token=customer1_token)
print("Also, only jwt tokens with subject specified as the owner of the account are allowed to manipulate it")
patch(ACCOUNT_MANAGER, 'accounts/income', json={"iban": "CZ4907100000000000123457", "amount": 1000, "currency": "CZK"}, token=customer2_token)
print("Employee scopes are allowed to do any valid operation on any account")
patch(ACCOUNT_MANAGER, 'accounts/income', json={"iban": "CZ4907100000000000123457", "amount": 1000, "currency": "CZK"}, token=EMPLOYEE_TOKEN)
get(ACCOUNT_MANAGER, 'accounts/CZ4907100000000000123457', token=EMPLOYEE_TOKEN)




print_divider()

print("Let's have a look what impact has this had on the rest of the system")

print("Summary of transactions")
get(TRANSACTION_MANAGER, 'transactions')

print("This is of course well and good but imagine you have a lot of transactions.")
print("For these cases we have a reporting manager to give you more insights about the aggregate big picture.")
print("Reporting managers loads the transactions every 30seconds, so we need to wait at least that to be sure the data is loaded.")
time.sleep(30)

print()
post(REPORTING_MANAGER, 'reporting/total', json={"accountNumbers": ["CZ4907100000000000123457"]})
post(REPORTING_MANAGER, 'reporting/cash', json={"accountNumbers": ["CZ4907100000000000123457"]})
post(REPORTING_MANAGER, 'reporting/average', json={"accountNumbers": ["CZ4907100000000000123457"]})

print_divider()
print("Now let's add some transactions directly into reporting manager db to showcase its use case")
get(REPORTING_MANAGER, 'reporting/test')

response_1 = post(REPORTING_MANAGER, 'reporting/cash', json={"accountNumbers": ["CZ2850514863768494113256"]})
response_2 = post(REPORTING_MANAGER, 'reporting/cash', json={"accountNumbers": ["CZ6050516263453833731579"]})
response_3 = post(REPORTING_MANAGER, 'reporting/cash', json={"accountNumbers": ["CZ1750512527682967891453"]})
response_4 = post(REPORTING_MANAGER, 'reporting/cash', json={"accountNumbers": ["CZ2550515548468511346336"]})
response_5 = post(REPORTING_MANAGER, 'reporting/cash', json={"accountNumbers": ["CZ3250517347693611456594"]})
response_6 = post(REPORTING_MANAGER, 'reporting/cash', json={"accountNumbers": ["CZ8450516554635931338193"]})
response_7 = post(REPORTING_MANAGER, 'reporting/cash', json={"accountNumbers": ["CZ1150515754779739431496"]})

accounts = ['CZ2850514863768494113256', 'CZ6050516263453833731579', 'CZ1750512527682967891453', 'CZ2550515548468511346336', 'CZ3250517347693611456594', 'CZ8450516554635931338193', 'CZ1150515754779739431496']
responses = [response_1, response_2, response_3, response_4, response_5, response_6, response_7]
data = []
for i, response in enumerate(responses):
    myJson = json.loads(response.text)
    data.append({
        'debit': abs(myJson['debit']),
        'balance': abs(myJson['balance']),
        'credit': abs(myJson['credit']),
        'account': '_' + accounts[i][-4:]
    })
df = pd.DataFrame(data)

# Plot
colors = ['green', 'red']
df.plot(x='account', y=['debit', 'credit'], kind='bar', color=colors)
plt.title('Debit and Credit values for each account')
plt.xlabel('Account Number')
plt.ylabel('Value in CZK')
plt.xticks(rotation=0)
plt.savefig('extra/report.png')
plt.show()
